IMPACT ANALYSIS TECHNIQUE:
    This paper describes an impact analysis technique that identifies 
    which parts should be retested after a system written in C++ is modified. 
    We are interested in identifying the impacts of changes at the class member level 
    by using dependency relations between class members. 
    We try to find out which member functions need unit-level retesting and 
    which interactions between them need integration-level retesting. 
    To get precise analysis results, we adopt a technique that classifies types of changes and 
    analyze the impact for each type. 
    Primitive changes, changes which are associated with C++ features, are first defined and 
    their ripple effects are computed in order to construct a firewall for each type of change systematically. 
    We have applied our prototype tool to a real system with small size. 
    This case study shows some evidence that our approach gives reasonable efficiency and 
    precision as well as being practical for analyzing change impacts of C++ programs.

    
TESTING:
1- PASS: all scenario requirements are PASS and procedural bugs are accepted 
2- NOT TESTED: the scenario has been validated in another iteration or the scenario has been deleted 
3- FAIL: at least one bug has been found on scenario 
4- NOT FULLY TESTED 
    a) a feature is not implemented or not testable (ex: mechanical problem ..) 
    b) a bug prevents the test 
5- PROC KO: 
    a) at least one procedural bug has been refused in the scenario 
    b) the scenario contains too many bugs Procedures 
    
In cases 3, 4 and 5, the scenario will have to be replayed in a next iteration.

SCENARIO TESTING:
Scenario testing is a software testing activity that uses scenarios: hypothetical stories to help the tester work through a complex problem or test system. 
The ideal scenario test is a credible, complex, compelling or motivating story; the outcome of which is easy to evaluate.[1] 
These tests are usually different from test cases in that test cases are single steps whereas scenarios cover a number of steps.[2][3]

SCENARIO:
A scenario has a goal, which is usually functional. A scenario describes one way that a system is or is envisaged to be used in the context of activity in a defined time-frame. The time-frame for a scenario could be (for example) a single transaction; a business operation; a day or other period; or the whole operational life of a system. Similarly the scope of a scenario could be (for example) a single system or piece of equipment; an equipped team or department; or an entire organization.

FUNCTIONAL REQUIREMENT:
In software engineering and systems engineering, a functional requirement defines a function of a system or its component, 
where a function is described as a specification of behavior between outputs and inputs.[1]
Functional requirements may involve calculations, technical details, data manipulation and processing,
and other specific functionality that define what a system is supposed to accomplish.[2] 
Behavioral requirements describe all the cases where the system uses the functional requirements, these are captured in use cases. 
Functional requirements are supported by non-functional requirements (also known as "quality requirements"), 
which impose constraints on the design or implementation (such as performance requirements, security, or reliability). 
Generally, functional requirements are expressed in the form "system must do <requirement>," 
while non-functional requirements take the form "system shall be <requirement>."[3]
The plan for implementing functional requirements is detailed in the system design, whereas non-functional requirements are detailed in the system architecture.[4][5]

As defined in requirements engineering, functional requirements specify particular results of a system. 
This should be contrasted with non-functional requirements, which specify overall characteristics such as cost and reliability. 
Functional requirements drive the application architecture of a system, while non-functional requirements drive the technical architecture of a system.[4]

In some cases a requirements analyst generates use cases after gathering and validating a set of functional requirements. The hierarchy of functional requirements collection and change, broadly speaking, is: user/stakeholder request → analyze → use case → incorporate. Stakeholders make a request; systems engineers attempt to discuss, observe, and understand the aspects of the requirement; use cases, entity relationship diagrams, and other models are built to validate the requirement; and, if documented and approved, the requirement is implemented/incorporated.[6] Each use case illustrates behavioral scenarios through one or more functional requirements. Often, though, an analyst will begin by eliciting a set of use cases, from which the analyst can derive the functional requirements that must be implemented to allow a user to perform each use case.

HIGHER-ORDER FUNCTION:
In mathematics and computer science, a higher-order function is a function that does at least one of the following:
takes one or more functions as arguments (i.e. procedural parameters),
returns a function as its result.
All other functions are first-order functions. In mathematics higher-order functions are also termed operators or functionals. The differential operator in calculus is a common example, since it maps a function to its derivative, also a function.

RESERVED WORD/KEYWORD:
In a computer language, a reserved word (also known as a reserved identifier) is a word that cannot be used as an identifier, 
such as the name of a variable, function, or label – it is "reserved from use". 
This is a syntactic definition, and a reserved word may have no meaning.
A closely related and often conflated notion is a keyword, which is a word with special meaning in a particular context. 
This is a semantic definition. 
By contrast, names in a standard library but not built into the language are not considered reserved words or keywords. 
The terms "reserved word" and "keyword" are often used interchangeably – one may say that a reserved word is 
"reserved for use as a keyword" – and formal use varies from language to language; for this article we distinguish as above.

TYPENAME:
"typename" is a keyword in the C++ programming language used when writing templates. 
It is used for specifying that a dependent name in a template definition or declaration is a type.[1][2] 
In the original C++ compilers before the first ISO standard was completed, the typename keyword was not part of the C++ language and Bjarne Stroustrup used the class keyword for template arguments instead. While typename is now the preferred keyword, older source code may still use the class keyword instead (for example see the difference in source code examples between The Design and Evolution of C++ by Bjarne Stroustrup published in 1994 and the source code examples in The C++ Programming Language: Fourth Edition by Bjarne Stroustrup published in 2013).

DATA TYPE:
In computer science and computer programming, a data type or simply type is an attribute of data which tells the compiler or interpreter how the programmer intends to use the data. Most programming languages support basic data types of integer numbers (of varying sizes), Floating-point numbers (which approximate real numbers), characters and Booleans. A data type constrains the values that an expression, such as a variable or a function, might take. This data type defines the operations that can be done on the data, the meaning of the data, and the way values of that type can be stored. A data type provides a set of values from which an expression (i.e. variable, function, etc.) may take its values.[
Value space
A type is a set of possible values which a variable can possess. Such definitions make it possible to speak about (disjoint) unions or Cartesian products of types.

VIRTUAL MACHINE:
In computing, a virtual machine (VM) is an emulation of a computer system. Virtual machines are based on computer architectures and provide functionality of a physical computer. Their implementations may involve specialized hardware, software, or a combination.

SONAME:
In Unix and Unix-like operating systems, a soname is a field of data in a shared object file. The soname is a string, which is used as a "logical name" describing the functionality of the object. Typically, that name is equal to the filename of the library, or to a prefix thereof, e.g. libc.so.6.