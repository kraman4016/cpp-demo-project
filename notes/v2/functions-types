#   struct sockaddr
        Generic Socket Address Structures: struct sockaddr
        This structure serves as a template for all of the domain-specific address structures.

#   struct sockaddr_in
        IPv4 socket addresses: struct sockaddr_in
        An IPv4 socket address is stored in a sockaddr_in structure, defined in <netinet/in.h>

#   struct sockaddr_in6
        IPv6 socket addresses: struct sockaddr_in6
        The difference is that an IPv6 address is 128 bits instead of 32 bits. An
        IPv6 socket address is stored in a sockaddr_in6 structure, defined in <netinet/in.h>

#   struct addrinfo
        see below.

#   getaddrinfo
        int getaddrinfo(const char *host, const char *service, const struct addrinfo *hints, struct addrinfo **result);
        Given a hostname and a service name, getaddrinfo() returns a set of
        structures containing the corresponding binary IP address(es) and port number.

#   inet_addr
        in_addr_t inet_addr(const char *cp);
        inet_addr, inet_ntoa - IPv4 address manipulation
        The inet_addr() function shall convert the string pointed to by cp, in the standard IPv4 dotted decimal notation, to an integer value suitable for use as an Internet address.
        The inet_addr() function converts the Internet host address cp from IPv4 numbers-and-dots notation into binary data in NETWORK BYTE ORDER. 
        If the input is invalid, INADDR_NONE (usually -1) is returned. Use of this function is problematic because -1 is a valid address (255.255.255.255). 
        Avoid its use in favor of inet_aton(), inet_pton(3), or getaddrinfo(3) which provide a cleaner way to indicate error return.

#   htons
        The htons(), htonl(), ntohs(), and ntohl() functions are defined (typically as macros) for converting integers in either direction between host and network byte order.
        #include <arpa/inet.h>
        uint16_t htons(uint16_t host_uint16);
        Returns host_uint16 converted to network byte order

#   atoi
        Defined in header <cstdlib>
        int       atoi( const char *str );
        Interprets an integer value in a byte string pointed to by str.
        If str does not point to a valid C-string, or if the converted value would be out of the range of values representable by an int, it causes undefined behavior.

#   setsockopt
     NAME
        setsockopt - set the socket options
     SYNOPSIS
        #include <sys/socket.h>
        int setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len);
     DESCRIPTION
        The setsockopt() function shall set the option specified by the option_name argument, 
        at the protocol level specified by the level argument, 
        to the value pointed to by the option_value argument for the socket associated with the file descriptor specified by the socket argument.

        The level argument specifies the protocol level at which the option resides. 
        To set options at the socket level, specify the level argument as SOL_SOCKET. 
        To set options at other levels, supply the appropriate level identifier for the protocol controlling the option. 
        For example, to indicate that an option is interpreted by the TCP (Transport Control Protocol), set level to IPPROTO_TCP as defined in the <netinet/in.h> header.

        The option_name argument specifies A SINGLE option to set. 
        The option_name argument and any specified options are passed uninterpreted to the appropriate protocol module for interpretations. 
        The <sys/socket.h> header defines the socket-level options. 
        The options are as follows:
        SO_KEEPALIVE
        Keeps connections active by enabling the periodic transmission of messages, if this is supported by the protocol. This option takes an int value.
        If the connected socket fails to respond to these messages, the connection is broken and threads writing to that socket are notified with a SIGPIPE signal. This is a Boolean option.
        
        For Boolean options, 0 indicates that the option is disabled and 1 indicates that the option is enabled.
        Options at other protocol levels vary in format and name.
        the use of the getsockopt() and setsockopt() system calls to retrieve and modify options affecting the operation of a socket.
        level is set to SOL_SOCKET, which indicates an option that applies at the sockets API level.
        The optname argument identifies the option whose value we wish to set or retrieve. 
        The optval argument is a pointer to a buffer used to specify or return the option value; this argument is a pointer to an integer or a structure, depending on the option.
        The optlen argument specifies the size (in bytes) of the buffer pointed to by optval.
        For setsockopt(), this argument is passed by value. For getsockopt(), optlen is a value-result argument.
        the socket file descriptor returned by a call to accept() inherits the values of settable socket options from the listening socket.
        Socket options are associated with an open file description. This means that file descriptors duplicated as a consequence of dup() (or similar) or fork() share the same set of socket options.
         TCP_KEEPIDLE (since Linux 2.4)
              The time (in seconds) the connection needs to remain idle
              before TCP starts sending keepalive probes, if the socket
              option SO_KEEPALIVE has been set on this socket.  This option
              should not be used in code intended to be portable.
         TCP_KEEPINTVL (since Linux 2.4)
              The time (in seconds) between individual keepalive probes.
              This option should not be used in code intended to be
              portable.
         TCP_KEEPCNT (since Linux 2.4)
              The maximum number of keepalive probes TCP should send before
              dropping the connection.  This option should not be used in
              code intended to be portable.

        #define TCP_KEEPIDLE		 4  /* Start keeplives after this period */
        #define TCP_KEEPINTVL		 5  /* Interval between keepalives */
        #define TCP_KEEPCNT		 6  /* Number of keepalives before death */
        
        Both return 0 on success, or –1 on error

        Undoubtedly the most common question regarding this option is whether the timing
        parameters can be modified (usually to reduce the two-hour period of inactivity to some
        shorter value). Appendix E of TCPv1 discusses how to change these timing parameters for
        various kernels, but be aware that most kernels maintain these parameters on a per-kernel
        basis, not on a per-socket basis, so changing the inactivity period from 2 hours to 15 minutes,
        for example, will affect ALL SOCKETS ON THE HOST that enable this option. However, such questions
        usually result from a misunderstanding of the purpose of this option.
        The purpose of this option is to detect if the PEER HOST crashes or becomes unreachable (e.g., dial-up modem connection drops, power fails, etc.).
        If the PEER PROCESS crashes, its TCP will send a FIN across the connection, which we can easily detect with select.

        For Windows: 
            int setsockopt(
              SOCKET     s,
              int        level,
              int        optname,
              const char *optval,
              int        optlen
            );        

        Note: SO_REUSEADDR needs to be set before bind(). However, not all options need to be set before bind(), or even before connect(). 
        It really depends on the specific options being set, so you have to deal with them on an option-by-option basis.

        The following socket options are inherited by a connected TCP socket from the listening socket
        (pp. 462–463 of TCPv2): SO_DEBUG, SO_DONTROUTE, SO_KEEPALIVE, SO_LINGER,
        SO_OOBINLINE, SO_RCVBUF, SO_RCVLOWAT, SO_SNDBUF, SO_SNDLOWAT, TCP_MAXSEG, and
        TCP_NODELAY. This is important with TCP because the connected socket is not returned to a
        server by accept until the three-way handshake is completed by the TCP layer. To ensure that
        one of these socket options is set for the connected socket when the three-way handshake
        completes, we must set that option for the listening socket.    

#   std::cin, std::wcin
        Defined in header <iostream>
        extern std::istream cin;
        extern std::wistream wcin;

        The global objects std::cin and std::wcin control input from a stream buffer of implementation-defined type (derived from std::streambuf), 
        associated with the standard C input stream stdin.

        These objects are guaranteed to be initialized during or before the first time an object of type std::ios_base::Init is constructed 
        and are available for use in the constructors and destructors of static objects with ordered initialization (as long as <iostream> is included before the object is defined).

        Unless sync_with_stdio(false) has been issued, it is safe to concurrently access these objects from multiple threads for both formatted and unformatted input.

        Once std::cin is constructed, std::cin.tie() returns &std::cout, and likewise, std::wcin.tie() returns &std::wcout. 
        This means that any formatted input operation on std::cin forces a call to std::cout.flush() if any characters are pending for output.

        Notes
        The 'c' in the name refers to "character" (stroustrup.com FAQ); cin means "character input" and wcin means "wide character input"


#   RS232
    ::open (port_.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);

    File Status Flags
    File status flags are used to specify attributes of the opening of a file. 
    Unlike the file descriptor flags discussed in Descriptor Flags, the file status flags are shared by duplicated file descriptors resulting from a single opening of the file. 
    The file status flags are specified with the flags argument to open; see Opening and Closing Files.

    File status flags fall into three categories, which are described in the following sections.

    Access Modes, specify what type of access is allowed to the file: reading, writing, or both. 
    They are set by open and are returned by fcntl, but cannot be changed.
    Open-time Flags, control details of what open will do. These flags are not preserved after the open call.
    Operating Modes, affect how operations such as read and write are done. They are set by open, and can be fetched or changed with fcntl.

    File Access Modes
    The file access modes allow a file descriptor to be used for reading, writing, or both. 
    (On GNU/Hurd systems, they can also allow none of these, and allow execution of the file as a program.) 
    The access modes are chosen when the file is opened, and never change.

    Macro: int O_RDWR
    Open the file for both reading and writing.

    On GNU/Hurd systems (and not on other systems), O_RDONLY and O_WRONLY are independent bits that can be bitwise-ORed together, 
    and it is valid for either bit to be set or clear. This means that O_RDWR is the same as O_RDONLY|O_WRONLY. 
    A file access mode of zero is permissible; it allows no operations that do input or output to the file, 
    but does allow other operations such as fchmod. 
    On GNU/Hurd systems, since “read-only” or “write-only” is a misnomer, fcntl.h defines additional names for the file access modes. 
    These names are preferred when writing GNU-specific code. 
    But most programs will want to be portable to other POSIX.1 systems and should use the POSIX.1 names above instead.


    Open-time Flags
    The open-time flags specify options affecting how open will behave. 
    These options are not preserved once the file is open. 
    The exception to this is O_NONBLOCK, which is also an I/O operating mode and so it is saved. 
    See Opening and Closing Files, for how to call open.

    There are two sorts of options specified by open-time flags.

    File name translation flags affect how open looks up the file name to locate the file, and whether the file can be created.
    Open-time action flags specify extra operations that open will perform on the file once it is open.

    Here are the file name translation flags.

    Macro: int O_NOCTTY
    If the named file is a terminal device, don’t make it the controlling terminal for the process. 
    See Job Control, for information about what it means to be the controlling terminal.

    On GNU/Hurd systems and 4.4 BSD, opening a file never makes it the controlling terminal and O_NOCTTY is zero. 
    However, GNU/Linux systems and some other systems use a nonzero value for O_NOCTTY and set the controlling terminal 
    when you open a file that is a terminal device; so to be portable, use O_NOCTTY when it is important to avoid this.

    Macro: int O_NONBLOCK
    This prevents open from blocking for a “long time” to open the file. 
    This is only meaningful for some kinds of files, usually devices such as serial ports; 
    when it is not meaningful, it is harmless and ignored. 
    Often, opening a port to a modem blocks until the modem reports carrier detection; 
    if O_NONBLOCK is specified, open will return immediately without a carrier.

    Note that the O_NONBLOCK flag is overloaded as both an I/O operating mode and a file name translation flag. 
    This means that specifying O_NONBLOCK in open also sets nonblocking I/O mode; see Operating Modes. 
    To open the file without blocking but do normal I/O that blocks, you must call open with O_NONBLOCK set and then call fcntl to turn the bit off.

    I/O Operating Modes
    The operating modes affect how input and output operations using a file descriptor work. 
    These flags are set by open and can be fetched and changed with fcntl.
    Macro: int O_NONBLOCK
    The bit that enables nonblocking mode for the file. 
    If this bit is set, read requests on the file can return immediately with a failure status if there is no input immediately available, instead of blocking. 
    Likewise, write requests can also return immediately with a failure status if the output can’t be written immediately.

    Note that the O_NONBLOCK flag is overloaded as both an I/O operating mode and a file name translation flag; see Open-time Flags.

    File Descriptor Flags
    File descriptor flags are miscellaneous attributes of a file descriptor. 
    These flags are associated with particular file descriptors, 
    so that if you have created duplicate file descriptors from a single opening of a file, each descriptor has its own set of flags.

    Currently there is just one file descriptor flag: FD_CLOEXEC, which causes the descriptor to be closed if you use any of the exec… functions (see Executing a File).

    Name: open
    Prototype: int open (const char *filename, int flags[, mode_t mode])
    Description:
    The open function creates and returns a new file descriptor for the file named by filename. Initially, 
     the file position indicator for the file is at the beginning of the file. The argument mode is used only 
     when a file is created, but it doesn't hurt to supply the argument in any case. 

    The flags argument controls how the file is to be opened. This is a bit mask; you create the value by the 
     bitwise OR of the appropriate parameters (using the | operator in C). , for the parameters available. 

    The normal return value from open is a non-negative integer file descriptor. In the case of an error, a 
     value of -1 is returned instead. In addition to the usual file name errors , the following errno error 
     conditions are defined for this function: 


    EACCES 	 The file exists but is not readable/writable as requested by the flags argument, the file 
     does not exist and the directory is unwritable so it cannot be created. 


	    - EEXIST Both O_CREAT and O_EXCL are set, and the named file already exists. 


	    - EINTR The open operation was interrupted by a signal. . 


	    - EISDIR The flags argument specified write access, and the file is a directory. 


	    - EMFILE The process has too many files open. The maximum number of file descriptors is 
	     controlled by the RLIMIT_NOFILE resource limit; . 


	    - ENFILE The entire system, or perhaps the file system which contains the directory, cannot 
	     support any additional open files at the moment. (This problem cannot happen on the GNU system.) 


	    - ENOENT The named file does not exist, and O_CREAT is not specified. 


	    - ENOSPC The directory or file system that would contain the new file cannot be extended, because 
	     there is no disk space left. 


	    - ENXIO O_NONBLOCK and O_WRONLY are both set in the flags argument, the file named by 
	     filename is a FIFO , and no process has the file open for reading. 


	    - EROFS The file resides on a read-only file system and any of O_WRONLY, O_RDWR, and O_TRUNC 
	     are set in the flags argument, or O_CREAT is set and the file does not already exist. 



    If on a 32 bit machine the sources are translated with _FILE_OFFSET_BITS == 64 the function open 
     returns a file descriptor opened in the large file mode which enables the file handling functions to use 
     files up to 2^63 bytes in size and offset from -2^63 to 2^63. This happens transparently for the user 
     since all of the lowlevel file handling functions are equally replaced. 

    This function is a cancellation point in multi-threaded programs. This is a problem if the thread 
     allocates some resources (like memory, file descriptors, semaphores or whatever) at the time open is 
     called. If the thread gets canceled these resources stay allocated until the program ends. To avoid this 
     calls to open should be protected using cancellation handlers. 

    The open function is the underlying primitive for the fopen and freopen functions, that create 
     streams. 
    Header files:
    fcntl.h

       The <errno.h> header file defines the integer variable errno, which
       is set by system calls and some library functions in the event of an
       error to indicate what went wrong.

       cfsetospeed() sets the output baud rate stored in the termios struc‐
       ture pointed to by termios_p to speed, which must be one of these
       constants:

            B0
            B50
            B75
            B110
            B134
            B150
            B200
            B300
            B600
            B1200
            B1800
            B2400
            B4800
            B9600
            B19200
            B38400
            B57600
            B115200
            B230400

Name: cfsetispeed
Prototype: int cfsetispeed (struct termios *termios-p, speed_t speed)
Description:
This function stores speed in *termios-p as the input speed. The normal return value is 0; a value of -1 
 indicates an error. If speed is not a speed, cfsetospeed returns -1. 
Header files:
termios.h

Name: cfsetospeed
Prototype: int cfsetospeed (struct termios *termios-p, speed_t speed)
Description:
This function stores speed in *termios-p as the output speed. The normal return value is 0; a value of -1 
 indicates an error. If speed is not a speed, cfsetospeed returns -1. 
Header files:
termios.h


The termios_p argument is a pointer to a termios structure, which records terminal
attributes:
struct termios {
tcflag_t c_iflag; /* Input flags */
tcflag_t c_oflag; /* Output flags */
tcflag_t c_cflag; /* Control flags */
tcflag_t c_lflag; /* Local modes */
cc_t c_line; /* Line discipline (nonstandard)*/
cc_t c_cc[NCCS]; /* Terminal special characters */
speed_t c_ispeed; /* Input speed (nonstandard; unused) */
speed_t c_ospeed; /* Output speed (nonstandard; unused) */
};
The first four fields of the termios structure are bit masks (the tcflag_t data type is an
integer type of suitable size) containing flags that control various aspects of the terminal
driver’s operation:
􀁺 c_iflag contains flags controlling terminal input;
􀁺 c_oflag contains flags controlling terminal output;
􀁺 c_cflag contains flags relating to hardware control of the terminal line; and
􀁺 c_lflag contains flags controlling the user interface for terminal input.



















