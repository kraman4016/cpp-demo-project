#Q1. PERFORMANCE TESTING
        Performance testing is the process of determining the speed, responsiveness 
        and stability of a computer, network, software program or device under a workload.
        It is a non-functional testing technique.

#2. CODE SONAR
    GrammaTech is a software-development tools vendor based in Ithaca, New York. 
    The company was founded in 1988 as a technology spin-off of Cornell University. 
    They now develop CodeSonar, a static analysis tool for source code and binaries, 
    and perform cyber-security research.
