# HRU TOOL

AstmAutotestsDMConverters
    static bool FromDM(DeviceMessage& dm, AstmMessage& message, AstmLogger& logger, bool isSendToHostDirection);
    static bool FromMessageToDm(DeviceMessage& dm, AstmMessage& message, AstmLogger& logger, bool isSendToHostDirection);

    static bool FromDM(DeviceMessage& dm, const QVector<AstmAbstractSequenceRecord*>* records, AstmLogger& logger);
    static bool ToDm(DeviceMessage& dm, const IAstmRecordContainer* parent, AstmLogger& logger, char recordType, std::vector<IPatient>::iterator* dmPatient, std::vector<IManualOrder>::iterator* dmOrder, IHematoParam* dmResult );











#
	AstmMessageConverter	-	Conversion between ASTM Message and ASTM Frame
	AstmFrame		-	ASTM packet of data
	AstmLogger		-	ASTM error and warning logger
	AstmFrameConverter	-	Conversion between ASTM Frame and bytes
	AstmAbstractSession	-	ASTM communication business logic class
	AstmMessage		-	ASTM Communication Message
	AstmMessageGenerator	-	Generator of ASTM test messages
	AstmSessionState	-	ASTM communication state
	AstmSessionEvent	-	ASTM communication state machine event
        AstmTcpServer           -       ASTM TCP/IP server class
        AstmRecordFormatter     -       Conversion between ASTM Record and QByteArray
        TextConverter           -       Text conversion methods
        IAstmRecordContainer    -       Record container interface
        AstmTcpClient           -       ASTM TCP/IP Client class       
		
	
# LOG & COMMENT MESSAGES
	Failed to read file %1: %2	-	filename, error string
	!!! No message received, timeout. \n
	"" "Autotest is being started" 
	"" "Delete output files completed" 
	"" "" 
	"" "Autotest is being started" 
	"" "Mode(parameter): lisssm"
	"" "TO HOST started at 2019-04-29T10:45:34 : Read -> ToDm -> LIS_SSM -> Receive -> Write" 
	"" "FROM HOST started at 2019-04-29T10:46:02 : Read -> Send -> Receive from LIS_SSM -> Write" 
	Check and remove BOM
	numerate frames 	(RK set valid frame number)
	initialize re-send counter
	processing enabled
	stay in IDLE
	has frames to send
	re-try send timer is still active
    Wrong parameter passed.
    Mode(parameter): 
    Unsupported data record type: %1
    Send event failed.
    Remark: time zone is not implemented

    COMMENTED OUT by AV as per SRS chapter 5.3.6
    ADDED by AV as per SRS chapter 5.3.6
    BUG 52: SV_FIELD_MANDATORY_ABSENT is not risen
    Value is only ^'s and nothing more
    Fix of bug 141: valid numeric data got >= 0
    if resultNamesToSend null or empty, rise an error (don't send any results)
    // HRU AV: DML# 113
    // COMMENTED OUT by HRU AV 24.02.14
    // TODO
    // HRU AV 22.06.2016 BUG# 152 fix: undesired <CR> before <ETB> in traceability log
    //compare to regex patterns
    //VK - add separate method to improve readability
    /**
     * ASTMProtocol is a realization of the communication protocol between hosts and
     * medical analyzers according to ASTM Spec E1394
     * 
     * @author S.Ivanovskiy (HMR), A.Vilkov (HRU), HRU
     * @version 30.10.2014
     */
    throw std::runtime_error("Failed to mkdir -p latency_logs");
    Invalid header record length
    Invalid header record type: %1[ASTM_CODE_ERROR_HEADER_RECORD_TYPE = ASTME0026]
    // message begin could not be found - keep data and waiting
    // end of data character not found - keep data and waiting
    Incomplete frame bytes skipped (no ETX/ETB).
    // check number of bytes available after the end of data
    // message is incomplete - keep data and waiting
    // check frame completes with CRLF
    Skipped frame with invalid CRLF tail.
    // check frame number is available and valid
    Frame number is missing.
    Tests for ASTM frame-bytes converter class
    // at the moment for field 9.4 only. Check only for double or open list
    // Fix of bug 141: valid numeric data got >= 0
    NONE						  // Reserved for cases when error does not refer to a given field
    LISValidator            // Validator of message 
    // AV 19.03.15: BUG# 58 regression fix (sending Traceability M without POR)
    // TODO: here file not found exception should be thrown
    // Used for store settings of single field

# VARIABLE NAMES
	fileName
	readCount
	ch
	lineBytes
	recordBytes
	correctedRecordBytes
	position
	recordSize
	isEnd
	frameSize
	frameBytes
	frame
	index
	infile, outfile
        auto rv = system("mkdir -p latency_logs");

        


	
# FUNCITON NAMES
	AstmAutoTestsServices::Start
	AstmAutoTestsServices::SendFromHostToClient
	AstmAutoTestsServices::SendContentOfFile
	AstmMessageConverter::AddFramesFromFile
	AstmMessageConverter::AddFrames
	AstmFrameConverter::GetNextFrameNumber [frameNumber >= 7 ? 0 : frameNumber + 1]
	enum StateType , AstmSessionState::SS_IDLE
	enum EventCode, AstmSessionEvent::EC_SEND_ENQ
        
        AstmRecordFormatter        
        static QByteArray Build(const AstmHeaderRecord& record, const AstmProtocolSettings& settings);
        static bool Parse(const QByteArray& bytest, bool isServerSide, AstmHeaderRecord& record, AstmLogger& logger);

        throwInstrumentSettingsErrorException
        prepare_logdir()

        HandleBuiltMessageErrors
        HandleReceivedMessageErrors
        
        

# CODING STYLE
	
#1 funciton signature style
	fun(const QString&, QVector<AstmFrame*>&)	-	input param(const reference), output param(reference) 
        AstmHeaderRecord GetHeader() const { return this->header; }
        AstmHeaderRecord& Header() { return this->header; }  

#2 warning and error codes
	//---------- Errors
	//--- Main Errors
	#define ASTM_CODE_ERROR_OPEN_MESSAGE_FILE "ASTME0009"
	
#3  default values
	#define ASTM_INVALID_FRAME_NUMBER 0xFF
        #define CHAR_WRAPPER_BEGIN '<'
        #define CHAR_WRAPPER_END '>'
        #define HL7_INVALID_INTEGER_VALUE (-1)
        #define ASTM_CONTROL_CHAR_STX '\x02'    // file : AstmProtocolContants.h


#4 logic

    frameNumber >= 7 ? 0 : frameNumber + 1


    if((ch >= '0') && (ch <= '9'))
        return (int)(ch - '0');
    if ((ch >= 'A') && (ch <= 'F'))
        return (int)(ch - 'A') + 10;
    if ((ch >= 'a') && (ch <= 'f'))
        return (int)(ch - 'a') + 10;


    for (QVector<AstmAbstractSequenceRecord*>::ConstIterator it = records->constBegin(); it != records->constEnd(); it++)
    {
        record = *it;
        if (record)
        {
            switch (record->GetRecordType())
            {
            case ASTM_RECORD_TYPE_COMMENT:
                recordBytes = AstmRecordFormatter::Build(*dynamic_cast<const AstmCommentRecord*>(record), header, settings);
                break;


    enum ConversionFlags : int
    {
        NONE = 0,

        HANDLE_CR = 0x01,
        HANDLE_LF = 0x02,
        DUPLICATE_WRAPPER = 0x04,

        ALL = HANDLE_CR | HANDLE_LF | DUPLICATE_WRAPPER
    };

      

        enum ELaunchType : char
        {
            UNDEFINED = 0,                      ##
            DM,
            NODM,
            LISSSM,
            ALL
        };

        enum ETestType : char
        {
            TO_HOST,
            FROM_HOST,
        };

# GESTURE RECOGNITION TOOLKIT

# COMMIT MESSAGES
        fixing a couple of spelling typos

#
    To temporarily disable a section of source code by converting it into a comment.
