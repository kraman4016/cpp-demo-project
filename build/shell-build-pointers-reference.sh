# virtulal concept compilation
g++ -std=c++11 -c -I../src ../src/pointers/reference/MyReference.cpp

g++ -std=c++11 -c -I../src ../src/pointers/reference/main.cpp

# linking object files to create executable
g++ main.o MyReference.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



