# virtulal concept compilation
g++ -std=c++14 -c -I../src ../src/pointers/uniquepointer/MyUniquePointer.cpp

g++ -std=c++14 -c -I../src ../src/pointers/uniquepointer/main.cpp

# linking object files to create executable
g++ main.o MyUniquePointer.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



