# virtulal concept compilation
# -Werror : treat warnings as errors
# -Wsign-conversion : enalble
# -Wno-sign-conversion : disable
g++ -std=c++11 -c -Wall -Weffc++ -Wextra  -Wsign-conversion -Wno-sign-conversion -I../src ../src/warning/MyClass.cpp

g++ -std=c++11 -c -w -I../src ../src/warning/main.cpp

# linking object files to create executable
g++ main.o MyClass.o -w -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



