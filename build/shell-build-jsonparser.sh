# virtulal concept compilation
g++ -std=c++11 -c -I../src -I../third_party/rapidjson/include ../src/jsonparsing/MyJsonParser.cpp

g++ -std=c++11 -c -I../src ../src/jsonparsing/main.cpp

# linking object files to create executable
g++ main.o MyJsonParser.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



