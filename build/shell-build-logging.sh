# logging compilation steps
g++ -std=c++11 -c -I../src -I../third_party/spdlog/include ../src/logging/MyLogger.cpp
g++ -std=c++11 -c -I../src ../src/logging/main.cpp
g++ main.o MyLogger.o -lpthread -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



