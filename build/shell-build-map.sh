# virtulal concept compilation
g++ -std=c++14 -c -I../src ../src/container/associative/map/MyMap.cpp

g++ -std=c++11 -c -I../src ../src/container/associative/map/main.cpp

# linking object files to create executable
g++ main.o MyMap.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



