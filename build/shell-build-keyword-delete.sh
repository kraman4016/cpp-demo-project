g++ -std=c++11 -c -I../src ../src/keyword/delete/MyDelete.cpp
g++ -std=c++11 -c -I../src ../src/keyword/delete/MyClass2.cpp

g++ -std=c++11 -c -I../src ../src/keyword/delete/main.cpp

# linking object files to create executable
g++ main.o MyDelete.o MyClass2.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



