# virtulal concept compilation
# -Werror : treat warnings as errors
# -Wsign-conversion : enalble
# -Wno-sign-conversion : disable
g++ -std=c++11 -c -Wall -Weffc++ -Wextra  -Wsign-conversion -Wno-sign-conversion -I../src ../src/exception/MyException.cpp

g++ -std=c++11 -c -w -I../src ../src/exception/main.cpp

# linking object files to create executable
g++ main.o MyException.o -w -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



