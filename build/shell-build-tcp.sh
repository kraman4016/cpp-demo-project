# virtulal concept compilation
g++ -std=c++11 -c -I../src ../src/utility/tcp/MyTcp.cpp

g++ -std=c++11 -c -I../src ../src/utility/tcp/main.cpp

# linking object dates to create executable
g++ main.o MyTcp.o -o run

# lauch binaries
./run

# remove object dates created during compilation
rm *.o ./run


