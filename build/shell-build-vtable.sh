# virtulal concept compilation
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyBase.cpp
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyBase2.cpp
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyBase3.cpp
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyBase4.cpp
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyBase5.cpp
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyBase6.cpp
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyDerived6.cpp
g++ -fdump-class-hierarchy-details -std=c++11 -c -I../src ../src/virtual/vtable/MyBase7.cpp


g++ -fdump-class-hierarchy -std=c++11 -c -I../src ../src/virtual/vtable/main.cpp

# linking object files to create executable
g++ main.o MyBase.o MyBase2.o MyBase3.o MyBase4.o MyBase5.o MyBase6.o MyDerived6.o MyBase7.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



