# virtulal concept compilation
g++ -std=c++11 -c -I../src ../src/utility/progress-bar/MyProgressBar.cpp

g++ -std=c++11 -c -I../src ../src/utility/progress-bar/main.cpp

# linking object dates to create executable
g++ main.o MyProgressBar.o -o run

# lauch binaries
./run

# remove object dates created during compilation
rm *.o ./run


