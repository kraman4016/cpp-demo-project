# virtulal concept compilation
g++ -std=c++11 -c -I../src ../src/utility/timer/MyTimer.cpp

g++ -std=c++11 -c -I../src ../src/utility/timer/main.cpp

# linking object dates to create executable
g++ main.o MyTimer.o -o run

# lauch binaries
./run

# remove object dates created during compilation
rm *.o ./run


