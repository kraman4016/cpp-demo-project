# virtulal concept compilation
g++ -std=c++11 -c -I../src ../src/std/date/MyDate.cpp

g++ -std=c++11 -c -I../src ../src/std/date/main.cpp

# linking object dates to create executable
g++ main.o MyDate.o -o run

# lauch binaries
./run

# remove object dates created during compilation
rm *.o ./run

arm-xilinx-linux-gnueabi-g++ -std=c++11 -c -I../src ../src/std/date/MyDate.cpp
arm-xilinx-linux-gnueabi-g++ -std=c++11 -c -I../src ../src/std/date/main.cpp
arm-xilinx-linux-gnueabi-g++ main.o MyDate.o -o run
