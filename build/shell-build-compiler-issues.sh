# virtulal concept compilation
g++ -std=c++11 -c -w -I../src ../src/compiler/issues/MyClass.cpp

g++ -std=c++11 -c -w -I../src ../src/compiler/issues/main.cpp

# linking object files to create executable
g++ -w main.o MyClass.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



