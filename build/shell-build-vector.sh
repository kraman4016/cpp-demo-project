# virtulal concept compilation
g++ -std=c++11 -c -I../src ../src/container/sequential/vector/MyVector.cpp

g++ -std=c++11 -c -I../src ../src/container/sequential/vector/main.cpp

# linking object files to create executable
g++ main.o MyVector.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



