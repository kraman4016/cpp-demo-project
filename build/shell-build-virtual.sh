# virtulal concept compilation
g++ -fdump-class-hierarchy -std=c++11 -c -I../src ../src/virtual/MyVirtual.cpp
g++ -fdump-class-hierarchy -std=c++11 -c -I../src ../src/virtual/MyParent.cpp
g++ -fdump-class-hierarchy -std=c++11 -c -I../src ../src/virtual/MyChild.cpp
g++ -fdump-class-hierarchy -std=c++11 -c -I../src ../src/virtual/main.cpp

# linking object files to create executable
g++ main.o MyVirtual.o MyParent.o MyChild.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run


