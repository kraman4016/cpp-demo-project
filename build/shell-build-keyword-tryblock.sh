# virtulal concept compilation
g++ -std=c++11 -c -I../src ../src/keyword/tryblock/MyClass.cpp
g++ -std=c++11 -c -I../src ../src/keyword/tryblock/MyClass2.cpp

g++ -std=c++11 -c -I../src ../src/keyword/tryblock/main.cpp

# linking object files to create executable
g++ main.o MyClass.o MyClass2.o -o run

# lauch binaries
./run

# remove object files created during compilation
rm *.o ./run



