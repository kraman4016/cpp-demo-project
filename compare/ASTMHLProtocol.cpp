/*
 * ASTMHLProtocol.cpp
 *
 *  Created on: Jul 4, 2017
 *      Author: HIN-SB
 */

#include <random>
#include "lis/DeviceMessage.h"
#include "ASTMHLProtocol.h"
#include "decode/ASTMDecode.h"
#include "decode/TerminatorASTMDecode.h"
#include "decode/ASTMFrameValidator.h"
#include "encode/ASTMEncode.h"
#include "encode/MFTraceabilityASTMEncode.h"
#include "../common/Date_Time.h"
#include "decode/MFStatusASTMDecode.h"
#include "decode/MFRackMapASTMDecode.h"
#include "decode/MFRackTranferASTMDecode.h"
#include "decode/MFEventASTMDecode.h"
#include "decode/MFRemoteASTMDecode.h"
#include "encode/MFStatisticASTMEncode.h"
#include "encode/MFStatusASTMEncode.h"
#include "encode/MFRackMapASTMEncode.h"
#include "encode/MFRackTransferASTMEncode.h"
#include "encode/MFEventASTMEncode.h"
#include "encode/MFRemoteASTMEncode.h"

/**
 *  @brief ASTMHLProtocol
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param Log reference,loincMap Setting, HLSettings Setting, DeviceMsgValidator instance
 *  @return
 */
ASTMHLProtocol::ASTMHLProtocol(TraceabilityLog &log,loincMap objLoinc, HLSettings objHLSettings, DeviceMsgValidator objReceiveValidator):m_refLog(log), m_LoincSetting(objLoinc),m_HLSetting(objHLSettings),m_receiveValidator(objReceiveValidator) {
}

/**
 *  @brief encodeMessage
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param Reference DeviceMessage, vector<string>, int mCount(Not used in ASTM)
 *  @section DESCRIPTION
 *  Converts DeviceMessage to vector<string>(High level ASTM Frames)
 *  @return void
 */
void ASTMHLProtocol::encodeMessage(_DeviceMessage &objBO, std::vector<std::string> &objData, int mCount) {
    static_cast<void>(mCount);
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_ENCODE,ELevel::INFO,__FUNCTION__,__LINE__, EDirection::LIS_OUT);
	//Creation of instance of ASTMStructure
	//This ASTMStructure is used to convert BO to ASTM specific structure for further encoding.
    ASTMStructure objASTMMessage;
	//DML#331 HIN-SB setting User Setting in ASTMStructure object
    for (auto strHemato : m_receiveValidator.getInstrumentSettingDV().userSettings.resultNamesToSend) {
        objASTMMessage.m_UserResultNamesToSend.emplace_back(strHemato);
    }
	//Setting HLSetting object in objASTMMessage
    objASTMMessage.setHLSettings(m_HLSetting);
	
	//creation of instance of ASTMEncode class
	//ASTMEncode is used to convert Device Message to ASTM frames 
    ASTMEncode objEncode(m_refLog,&objASTMMessage, m_LoincSetting);

    if (objBO.orders.size()) {
        this->buildOrderList(objBO, objEncode);
        //Converts ASTMstructure for OrderList to vector<string>(High level ASTM data)
        objEncode.createOrderASTMMessage(objData);

    } else if (objBO.queries.size()) {
        this->buildQuery(objBO, objEncode);
        //Converts ASTMstructure for query to vector<string>(High level ASTM data)
        objEncode.createQueryASTMMessage(objData);

    } else if (objBO.result.isEmpty() == false) {
        this->buildResult(objBO, objEncode);
        //Converts ASTMstructure for Results to vector<string>(High level ASTM data)
        objEncode.createResultASTMMessage(objData);

    } else {
        bool isManufacturerToEncode = ((objBO.manufacturerMessages.conveyorRackMapping.empty() == false) || \
                                       (objBO.manufacturerMessages.conveyorRackTransfer.empty() == false) || \
                                       (objBO.manufacturerMessages.statistic.empty() == false) || \
                                       (objBO.manufacturerMessages.status.empty() == false) ||\
                                       (objBO.manufacturerMessages.event.empty() == false) ||\
                                       (objBO.manufacturerMessages.remoteCommand.empty() == false));
        //Creates ASTM Message(Comprising of ASTM frames) out of Manufacturer BO
        if (isManufacturerToEncode == true) {
            encodeManufacturerFrame(objEncode, objBO, objData, objASTMMessage);
        }
    }
}

/**
 *  @brief decodeMessage
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param std::vector<std::string> objProtocolData, _DeviceMessage& objBO
 *  @section DESCRIPTION
 *  Converts vector<string>(High level ASTM data) to DeviceMessage(Business Object)
 *  @return
 */
bool ASTMHLProtocol::decodeMessage(std::vector<std::string> objProtocolData, _DeviceMessage& objBO) {
    bool bRet = true;
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_DECODE,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
	
	//Creation of instance of ASTMStructure
	//This ASTMStructure is used to convert ASTM frames to ASTM specific structure for further filling BO.	
    ASTMStructure objASTMMessage;
	
	//Setting HLSetting object in objASTMMessage
    objASTMMessage.setHLSettings(m_HLSetting);
    m_receiveValidator.cleanHLErrorList();
	
	//Creation of instance of ASTMDecode class
	//ASTMDecode is used to convert ASTM frames to Device Message
    ASTMDecode objDecode(m_refLog,&objASTMMessage, m_LoincSetting);
	
	//Setting ASTM field Validation in objDecode
    objDecode.setFieldValidationSetting(m_receiveValidator.getFieldValidationSetting());

    //Fills DeviceMessage with data acquired from vector<string>(High level ASTM data)
    objBO.clean();
    m_hlErrorList.clear();
    //Creation of instance of ASTMFrameValidator class
    //ASTMFrameValidator is used to find structural error in ASTM frames
    ASTMFrameValidator frameValidator(m_refLog, &objASTMMessage);
    std::vector<std::string> objDataParsing;

    //Parsing received data to generate Structural Errors
    frameValidator.filterStructuralErrorsFromReceivedMessage(objProtocolData, objDataParsing, objBO);

    bool bManufacturerMessage = frameValidator.isManufacturerMessage();

    if (bManufacturerMessage == false) {
        bRet = this->parseMessage(objDataParsing, objDecode, objBO);
    } else {
        bRet = this->parseMFMessage(objDataParsing, objDecode, objBO);
    }
	
    //Setting of list of HLProtocolError in DeviceMessage
    for (auto error : m_hlErrorList) {
        objBO.hlerror.emplace_back(std::move(error));
    }
	
	//Setting managed HLProtocolErrors received from m_receiveValidator in DeviceMessage
    for (auto managedErr : m_receiveValidator.getFieldTruncatedHLerror()) 
        objBO.hlerror.emplace_back(std::move(managedErr));
	

    return bRet;
}

/**
 *  @brief getError
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param
 *  @section DESCRIPTION
 *	Getting High Level Error List
 *  @return List of HLProtocolError
 */
std::list<HLProtocolError>& ASTMHLProtocol::getError() {
    return m_hlErrorList;
}

/**
 *  @brief setError
 *  @author HIN-SB
 *  @date
 *  @version 1.0
 *  @param EHighLevelErrors errorName, ERecordFields fieldName, std::string sFrame
 *  @section DESCRIPTION
 *	Creating HLProtocolError Object and putting in m_hlErrorList
 *  @return void
 */
void ASTMHLProtocol::setError(EHighLevelErrors errorName, ERecordFields fieldName, std::string sFrame) {
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_SET_HLERROR,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
    HLProtocolError objError;
    objError.clean();
    objError.errorName = errorName;
    objError.fieldName = fieldName;
    objError.frame = sFrame;
    //DML#140 HIN-SB Filling Error Context in HLerror
    objError.context.allNegotiationCharacters.emplace_back(sFrame);
    objError.context.dateTime = DateTime::dateToStringWithMilliSec();
    objError.context.direction = EDirection::LIS_IN;
    m_hlErrorList.emplace_back(std::move(objError));
}

/**
 *  @brief handleReceiveMessageError
 *  @author HIN-SB
 *  @date
 *  @version 1.0
 *  @param HLProtocolError object, std::vector<std::string> child frames for generating HL_IGNORED_RECORD_ERROR and HL_BYPASSED_RECORD_ERROR, 
 *	EHighLevelErrors error to create HLProtocolError from HL_TERMINATOR_MISSING_ERROR, HL_UNEXPECTED_RECORD_ERROR, HL_BYPASSED_RECORD_ERROR
 *  @section DESCRIPTION
 *	Manage High Level Error for Received Data
 *  @return
 */
void ASTMHLProtocol::handleReceiveMessageError(HLProtocolError error, std::vector<std::string> &sChildFrames, EHighLevelErrors enumHLError) {
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_HANDLE_HLERROR,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
    HLProtocolError objHandleError;
    objHandleError.clean();
    bool isNoEnumHLError = ((enumHLError == static_cast<EHighLevelErrors>(INTEGER_DEFAULT_VALUE)) && (error.isEmpty() == false));
    if (isNoEnumHLError == true) {
		//Handling IGNORED_RECORD_ERR and HL_BYPASSED_RECORD_ERROR error for child frames  for below errors and FIELD_LENGTH_ERROR if data is mandatory
        bool bGoodErrorName = (error.errorName == EHighLevelErrors::HL_FIELD_REPEAT_DELIMITER_ERROR || \
                               error.errorName == EHighLevelErrors::HL_INVALID_FIELD_ERROR || \
                               error.errorName == EHighLevelErrors::HL_UNEXPECTED_RECORD_ERROR || \
                               error.errorName == EHighLevelErrors::HL_FIELD_LENGTH_ERROR || \
                               error.errorName == EHighLevelErrors::HL_NOT_MANAGED_RECORD_ERROR || \
                               error.errorName == EHighLevelErrors::HL_INVALID_ORDER_RECORD_ERROR);
        if (bGoodErrorName == true) {
            setError(error.errorName, error.fieldName, error.frame);
            setError(EHighLevelErrors::HL_IGNORED_RECORD_ERROR, error.fieldName, error.frame);
            for (auto sFrame : sChildFrames) {
                setError(EHighLevelErrors::HL_BYPASSED_RECORD_ERROR, ERecordFields::NONE, sFrame);
            }
        }
    } else if (enumHLError == EHighLevelErrors::HL_TERMINATOR_MISSING_ERROR) {
		//Creating HLProtocolError for HL_TERMINATOR_MISSING_ERROR
        setError(enumHLError, ERecordFields::NONE, "");
    } else if (enumHLError == EHighLevelErrors::HL_UNEXPECTED_RECORD_ERROR || enumHLError == EHighLevelErrors::HL_NOT_MANAGED_RECORD_ERROR) {
		//Creating HLProtocolError for HL_UNEXPECTED_RECORD_ERROR and HL_NOT_MANAGED_RECORD_ERROR
        if (sChildFrames.size())
            setError(enumHLError, ERecordFields::NONE, sChildFrames[0]);
    } else if (enumHLError == EHighLevelErrors::HL_BYPASSED_RECORD_ERROR) {
		//Creating HLProtocolError for HL_BYPASSED_RECORD_ERROR for Results
        for (unsigned j=0; j<sChildFrames.size(); j++) {
            if (sChildFrames[j][0] == Result_Header_def) {
                setError(EHighLevelErrors::HL_BYPASSED_RECORD_ERROR, ERecordFields::NONE, sChildFrames[j]);
                for (unsigned i=j+1; i<sChildFrames.size(); i++) {
                    if (sChildFrames[i][0] == Comment_Header_def)
                        setError(EHighLevelErrors::HL_BYPASSED_RECORD_ERROR, ERecordFields::NONE, sChildFrames[i]);
                    else
                        break;
                }

            }
        }
    }
}

/**
 *  @brief getChildFrames
 *  @author HIN-SB
 *  @date
 *  @version 1.0
 *  @param std::vector<std::string> &objData, std::vector<std::string> &sChildFrames, unsigned char recordId, unsigned iLevel, unsigned *iIgnoreOrderFrame
 *  @section DESCRIPTION
 *	Getting child frames for Particular RecordId
 *  @return void
 */
void ASTMHLProtocol::getChildFrames(std::vector<std::string> &objData, std::vector<std::string> &sChildFrames, unsigned char recordId, unsigned iLevel, unsigned *iIgnoreOrderFrame) {
    iLevel = iLevel + 1;
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_GET_CHILD_FRAME,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
    switch (recordId) {
    case Patient_Header_def: {
		//Child frames for Patient
        unsigned iSubOrderLevel = iLevel;
        bool bCommentExist = ((iLevel < objData.size()) && (objData[iLevel][0] == Comment_Header_def));
        if (bCommentExist == true) {
            sChildFrames.push_back(objData[iLevel]);
            iSubOrderLevel = iLevel + 1;
        }
        bool bOrderExists = (iSubOrderLevel < objData.size() && objData[iSubOrderLevel][0] == Test_Header_def);
        if (bOrderExists == true) {
            sChildFrames.push_back(objData[iSubOrderLevel]);
            for (unsigned j=iSubOrderLevel+1; (j<objData.size()) && (objData[j][0] == Comment_Header_def); j++)
                sChildFrames.push_back(objData[j]);
            *iIgnoreOrderFrame = iSubOrderLevel;
        }
        break;
    }
    case Result_Header_def:
    case Test_Header_def: {
		//Child frames for Order of Result
        for (unsigned j=iLevel; (j<objData.size() && objData[j][0] == Comment_Header_def); j++)
            sChildFrames.push_back(objData[j]);
        break;
    }
    default:
        break;
    }
}

/**
 *  @brief buildResult
 *  @author HIN-SB
 *  @date    06 Oct, 2017
 *  @version 1.0
 *  @param Reference to DeviceMessage, Reference to ASTMEncode
 *  @section DESCRIPTION
 *  Translates DeviceMessage(Result) to ASTMStructure(Result)
 *  @return
 */
void ASTMHLProtocol::buildResult(const _DeviceMessage& objBO, ASTMEncode& objASTMEncode) {
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_BUILD_RESULT,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_OUT);
    objASTMEncode.fillHeader(objBO.header);
    ASTMStructure *ptrASTMStructure = objASTMEncode.getASTMMessage();
    PATIENTASTMDATA patient{};
    ORDERTESTASTMDATA order{};
    MCURVEMATRIXASTMDATA curveASTM{};
    MRECORDTRACEABILITYASTMDATA traceASTM{};

    //Copying from IDetailledFinalResult(IFinalResult[BO],IPatient[BO], IManualOrder[BO])
    //To ASTMStructure(ORDERTESTASTMDATA,PATIENTASTMDATA, RESULTASTMDATA [ASTM Structures])
    IDetailledFinalResult resultBO = objBO.result;
    if (resultBO.manualOrder.patient.isEmpty() == false) {
        objASTMEncode.fillPatient(resultBO.manualOrder.patient, patient);
        patient.isEmpty = false;
    } else {
        patient.isEmpty = true;
    }
	//Translate IManualOrder BO to ORDERTESTASTMDATA
    if (resultBO.manualOrder.manualOrderBase.isEmpty() == false)
        objASTMEncode.fillTestOrder(resultBO.manualOrder, order);
	//Translate IFinalResult BO to RESULTASTMDATA
    if (resultBO.finalResult.isEmpty() == false)
        objASTMEncode.fillResult(resultBO);
	//Translate ICurveMatrix BO to MCURVEMATRIXASTMDATA
    unsigned short iSeqNum = 1;
    for (auto curveBO : objBO.manufacturerMessages.curveMatrix) {
        if (curveBO.isEmpty() == false) {
            objASTMEncode.fillCurveMatrix(curveBO, curveASTM);
            ptrASTMStructure->m_curveLst.emplace_back(std::move(curveASTM));
            iSeqNum += 1;
        }
    }
    //Translate ITraceability BO to MRECORDTRACEABILITYASTMDATA and create ASTM Frame from MRECORDTRACEABILITYASTMDATA
    MFTraceabilityASTMEncode traceEncode(objASTMEncode.getASTMMessage());
    std::string strTrace;
    for (auto traceBO : objBO.manufacturerMessages.traceability) {
            strTrace.clear();
            traceEncode.encode(traceBO, strTrace, iSeqNum);
            ptrASTMStructure->m_traceLst.emplace_back(std::move(strTrace));
            iSeqNum += 1;
    }

    if (ptrASTMStructure->report.m_resultLst.size() > 0) {
        order.patient = std::move(patient);
        ptrASTMStructure->report.order = std::move(order);
    }

}

/**
 *  @brief buildOrderList
 *  @author HIN-SB
 *  @date    06 Oct, 2017
 *  @version 1.0
 *  @param Reference to DeviceMessage, Reference to ASTMEncode
 *  @section DESCRIPTION
 *  Translates DeviceMessage(Order List) to ASTMStructure(OrderList)
 *  @return
 */
void ASTMHLProtocol::buildOrderList(const _DeviceMessage& objBO, ASTMEncode& objASTMEncode) {
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_BUILD_ORDER_LIST,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_OUT);
    objASTMEncode.fillHeader(objBO.header);
    ASTMStructure *ptrASTMStructure = objASTMEncode.getASTMMessage();

    //Copying orders(IPatient[BO], IManualOrder[BO])
    //To m_orderLst(ORDERTESTASTMDATA,PATIENTASTMDATA[ASTM Structure])
    for (auto orderBO : objBO.orders) {
        PATIENTASTMDATA patient{};
        ORDERTESTASTMDATA order{};
        if (orderBO.isEmpty() == false) {
            objASTMEncode.fillTestOrder(orderBO, order);
            if (orderBO.patient.isEmpty() == false) {
                objASTMEncode.fillPatient(orderBO.patient, patient);
                patient.isEmpty = false;
            } else {
                patient.isEmpty = true;
            }
            order.patient = std::move(patient);
            ptrASTMStructure->m_orderLst.emplace_back(std::move(order));
        }
    }
}

/**
 *  @brief buildQuery
 *  @author HIN-SB
 *  @date    06 Oct, 2017
 *  @version 1.0
 *  @param Reference to DeviceMessage, Reference to ASTMEncode
 *  @section DESCRIPTION
 *  Translates DeviceMessage(Query) to ASTMStructure(Query)
 *  @return
 */
void ASTMHLProtocol::buildQuery(const _DeviceMessage& objBO, ASTMEncode& objASTMEncode) {
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_BUILD_QUERY,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_OUT);
    objASTMEncode.fillHeader(objBO.header);
    ASTMStructure *ptrASTMStructure = objASTMEncode.getASTMMessage();
    REQUESTASTMDATA requestInfo{};

    //Copying orders(IPatient[BO], IManualOrder[BO])
    //To m_orderLst(ORDERTESTASTMDATA,PATIENTASTMDATA[ASTM Structure])
    for (auto queryBO : objBO.queries) {
        if (queryBO.isEmpty() == false) {
            objASTMEncode.fillRequestInfo(queryBO, requestInfo);
            ptrASTMStructure->m_requestInformationLst.emplace_back(std::move(requestInfo));
        }
    }
}

/**
 *  @brief parseMessage
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param  vector<string>, Reference of ASTMDecode, reference of DeviceMessage
 *  @section DESCRIPTION
 *  Decodes vector<string>[High Level frames] to ASTMStructure
 *  @return bool true parsing successful/ false parsing failure
 */
bool ASTMHLProtocol::parseMessage(std::vector<std::string> &objDataParsing, ASTMDecode &objASTMDecode, DeviceMessage &objBO) {
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_PARSE_MESSAGE,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
    //parsing the string to fill ASTM structures
    bool bParseMessage = true;
    bool bValidateBO = true;
    std::map<short, IPatient> patientlst;
    std::map<short, _IManualOrder> orderlst;
    std::vector<std::string> sChildFrames;

    //DML#119 Additional code to Create and Set idMessage in DeviceMessage (idMessage is used in event MESSAGE_RECEIVED)
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<int> dis(1, 60);
    objBO.idMessage =  dis(gen);
    IPatient patientBO;
    IManualOrder order;
    IHematoParam hemato;
    int32_t sCounterOrder = 0;
    unsigned sIgnoreChildOrder = 0;
    int32_t sCounterPatient = 0;
    int32_t sCounterResult = 0;
    std::list<HLProtocolError> m_commentHlErrs;
    TerminatorASTMDecode objTerminatorDecode(objASTMDecode.getASTMMessage()); //TerminatorASTMDecode object is used to decode Terminator frame

    for (unsigned i=0; i<objDataParsing.size(); i++) {
        std::string sFrame = objDataParsing[i];
        bParseMessage = true;
        bValidateBO = true;
        unsigned char recordId = sFrame[0];
        sChildFrames.clear();
        switch (recordId) {
        case Header_Header_def: //Parsing Header Frame and Validating IHeader BO
        {
            m_commentHlErrs.clear();
            bParseMessage = objASTMDecode.parseHeader(sFrame, objBO.header);
            sChildFrames.assign(objDataParsing.begin() + i + 1, objDataParsing.end());
            if (bParseMessage == true) {
            	bValidateBO = validateMessage(&objBO.header, sFrame, sChildFrames, bParseMessage, m_commentHlErrs);
                if (bValidateBO == false){
                    objBO.header.clean();
            		return (false);
                }
            }
            break;
        }
        case Patient_Header_def: //Parsing Patient Frame and Validating IPatient BO
        {
            patientBO.clean();
            m_commentHlErrs.clear();
            sCounterPatient = sCounterPatient + 1;
            getChildFrames(objDataParsing, sChildFrames, Patient_Header_def, i, &sIgnoreChildOrder);
            bParseMessage = objASTMDecode.parsePatient(sFrame, sChildFrames, patientBO, sCounterPatient);
            m_commentHlErrs = objASTMDecode.getCommentHLerror();
            bValidateBO = validateMessage(&patientBO, sFrame, sChildFrames, bParseMessage, m_commentHlErrs);
            if (bValidateBO == true) {
                sIgnoreChildOrder = 0;
                patientlst[sCounterPatient] = std::move(patientBO);
            }
            break;
        }
        case Test_Header_def: //Parsing Order Frame and Validating IManualOrder BO
        {
            if (i > sIgnoreChildOrder) {
                order.clean();
                m_commentHlErrs.clear();
                sCounterOrder = sCounterOrder + 1;
                getChildFrames(objDataParsing, sChildFrames, Test_Header_def, i);
                bParseMessage = objASTMDecode.parseTestOrder(sFrame, sChildFrames, order, sCounterOrder, sCounterPatient);
                m_commentHlErrs = objASTMDecode.getCommentHLerror();
                bValidateBO = validateMessage(&order, sFrame, sChildFrames, bParseMessage, m_commentHlErrs);
                if (bValidateBO == true) {
                    orderlst[sCounterOrder] = std::move(order);
                }
            }
            break;
        }
        case Result_Header_def: //Parsing Result Frame and Validating IFinalResult BO
        {
            hemato.clean();
            m_commentHlErrs.clear();
            sCounterResult = sCounterResult + 1;
            getChildFrames(objDataParsing, sChildFrames, Result_Header_def, i);
            bParseMessage = objASTMDecode.parseResult(sFrame, sChildFrames, hemato, sCounterResult);
            m_commentHlErrs = objASTMDecode.getCommentHLerror();
            bValidateBO = validateMessage(&hemato, sFrame, sChildFrames, bParseMessage, m_commentHlErrs);
			//BUG#380 HIN-SB Adding all received hemato parameter 
            bool bInsertHematoMap = (bValidateBO == true);
            if (bInsertHematoMap)
                    objBO.result.finalResult.Map[hemato.universalTestID.resultName.c_str()] = hemato;
            break;
        }
        default:
            objTerminatorDecode.parseFrame(sFrame, &objBO);//Decoding Terminator frame in order to parsing HLError
            break;
        }
        if (bParseMessage == false) {
            handleReceiveMessageError(objASTMDecode.getHLError(), sChildFrames);
            if (recordId == Header_Header_def)
                return (false);
        }
    }
	//Generating DeviceMessage out of Received ASTM data
    generateDeviceMessage(objBO, patientlst, orderlst, objDataParsing);

    return bParseMessage;
}

/**
 *  @brief parseMFMessage
 *  @author HIN-SB
 *  @date    15 Aug, 2019
 *  @version 1.0
 *  @param  vector<string>, Reference of ASTMDecode, reference of DeviceMessage
 *  @section DESCRIPTION
 *  Decodes vector<string>[High Level frames] to ASTMStructure and ASTMStrucuture to DeviceMessage for Manufacturer Message
 *  @return bool true parsing successful/ false parsing failure
 */
bool ASTMHLProtocol::parseMFMessage(std::vector<std::string> &objDataParsing, ASTMDecode &objASTMDecode, DeviceMessage &objBO) {
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_PARSE_MESSAGE,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
    //parsing the string to fill ASTM structures
    bool bParseMessage = true;
    bool bValidateBO = true;
    unsigned short iSeqNum = 1;
    std::vector<std::string> sChildFrames;

    //DML#119 Additional code to Create and Set idMessage in DeviceMessage (idMessage is used in event MESSAGE_RECEIVED)
    std::random_device randomNum;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(randomNum()); //Standard mersenne_twister_engine seeded with randomNum()
	const int upperLimit = 60;
	const int lowerLimit = 1;
    std::uniform_int_distribution<int> dis(lowerLimit, upperLimit); //Produces random integer values dis, uniformly distributed on the closed interval [1, 60]
    objBO.idMessage =  dis(gen);
    std::list<HLProtocolError> m_commentHlErrs; //List of HL Error for Comment frames
    ASTMFrameValidator frameValidator(m_refLog, objASTMDecode.getASTMMessage());
    TerminatorASTMDecode objTerminatorDecode(objASTMDecode.getASTMMessage());
    for (unsigned i=0; i<objDataParsing.size(); i++) { //iterating List of high Level frames
        std::string sFrame = objDataParsing[i];
        bParseMessage = true;
        bValidateBO = true;
        unsigned char recordId = sFrame[0];
        sChildFrames.clear();
        switch (recordId) {
        case Header_Header_def: //Record Id = Header
        {
            m_commentHlErrs.clear(); //Clearing HLError for comment frame
            bParseMessage = objASTMDecode.parseHeader(sFrame, objBO.header); //Parsing Header Frame
            sChildFrames.assign((objDataParsing.begin() + i + 1), objDataParsing.end()); //Create a list of Child frames associated with Header frame
            if (bParseMessage == true) { //if Header frame parsing true
				//Validate Header BO
                bValidateBO = validateMessage(&objBO.header, sFrame, sChildFrames, bParseMessage, m_commentHlErrs);//Since a common function to validate H, P, O, R but header frame has no comment frame associated with it, So m_commentHlErrs going empty
                if (bValidateBO == false) { //If Header BO validations then return (False) from this function
                    objBO.header.clean();
                    return (false);
				}
            } else { //if Header frame parsing false then return (False) from this function
               handleReceiveMessageError(objASTMDecode.getHLError(), sChildFrames);
               return (false);
            }
            break;
        }
        case Manufacturer_Header_def: //Record Id = Manufacturer
        {
            std::string strMessageType = frameValidator.getMFMessageType(sFrame); //MessageType of Manufacturer frame
            if (strMessageType.compare(MessageTypeMF::STATUS) == 0) { //if STATUS Manufacturer frame
                MFStatusASTMDecode objStatusDecode(objASTMDecode.getASTMMessage(), &m_receiveValidator); //create instance of MFStatusASTMDecode
                objStatusDecode.decode(objBO, sFrame, iSeqNum);//Decode STATUS Manufacturer frame
            } else if (strMessageType.compare(MessageTypeMF::RACKMAP) == 0) { //if RACKMAP Manufacturer frame
                MFRackMapASTMDecode objRackMapDecode(objASTMDecode.getASTMMessage(), &m_receiveValidator); //create instance of MFRackMapASTMDecode
                objRackMapDecode.decode(objBO, sFrame, iSeqNum); //Decode RACKMAP Manufacturer frame
            } else if (strMessageType.compare(MessageTypeMF::RACKTRF) == 0) { //if RACKTRF Manufacturer frame
                MFRackTranferASTMDecode objRackTrfDecode(objASTMDecode.getASTMMessage(), &m_receiveValidator); //create instance of MFRackTranferASTMDecode
                objRackTrfDecode.decode(objBO, sFrame, iSeqNum); //Decode RACKTRF Manufacturer frame
            } else if (strMessageType.compare(MessageTypeMF::EVENT) == 0) { //if EVENT Manufacturer frame
                MFEventASTMDecode objEventDecode(objASTMDecode.getASTMMessage(), &m_receiveValidator); //create instance of MFEventASTMDecode
                objEventDecode.decode(objBO, sFrame, iSeqNum); //Decode EVENT Manufacturer frame
            } else if (strMessageType.compare(MessageTypeMF::EXECUTE) == 0) { //if EXECUTE Remote command Manufacturer frame
                MFRemoteASTMDecode objRemoteDecode(objASTMDecode.getASTMMessage(), &m_receiveValidator); //create instance of MFEventASTMDecode
                objRemoteDecode.decode(objBO, sFrame, iSeqNum); //Decode EVENT Manufacturer frame
            }
        }
        case Final_Header_def: //Record Id = Terminator
        {
            objTerminatorDecode.parseFrame(sFrame, &objBO); //Parse Terminator Manufacturer frame
            break;
        }
        default:
            break;
        }
    }

    return bParseMessage;
}

/**
 *  @brief generateDeviceMessage
 *  @author HIN-SB
 *  @date
 *  @version 1.0
 *  @param reference of DeviceMessage, Map of Patient with Level info as key, Map of Order with Level info as key,objData[High level frames]
 *  @section DESCRIPTION
 *  Generate DeviceMessage[Patient List, Result, Order List] for Received data
 *  @return
 */
void ASTMHLProtocol::generateDeviceMessage(DeviceMessage &objBO,std::map<short, IPatient> &patientlst, std::map<short, IManualOrder> &orderlst, std::vector<std::string> &objData) {
    if (objBO.result.finalResult.Map.size() > 0)
        generateDeviceMessageResult(objBO, patientlst, orderlst, objData);
    else {
        //Setting m_orderLst in DeviceMessage
        if (orderlst.size() > 0) {
            generateDeviceMessageOrder(objBO, patientlst, orderlst);
        } else {
            //Setting m_patientLst in DeviceMessage
            for (auto outer_iter=patientlst.begin(); outer_iter!=patientlst.end(); ++outer_iter) {
                //DML#119 Creation id for creating DeviceMessage of PatientList
                std::random_device rd;  //Will be used to obtain a seed for the random number engine
                std::mt19937 genPatient(rd()); //Standard mersenne_twister_engine seeded with rd()
                std::uniform_int_distribution<int> disPatient(1, 60);
                //DML#119 Setting id for Patient
                outer_iter->second.idPatient = disPatient(genPatient);
                objBO.patientsWithoutOrders.emplace_back(std::move(outer_iter->second));
            }
        }
    }

}

/**
 *  @brief generateDeviceMessageResult
 *  @author HIN-SB
 *  @date
 *  @version 1.0
 *  @param reference of DeviceMessage, Map of Patient with Level info as key, Map of Order with Level info as key,objData[High level frames]
 *  @section DESCRIPTION
 *  Generate DeviceMessage(Result) for Received data
 *  @return void
 */
void ASTMHLProtocol::generateDeviceMessageResult(DeviceMessage &objBO, std::map<short, IPatient> &patientlst, std::map<short, IManualOrder> &orderlst, std::vector<std::string> &objData) {

    HLProtocolError err;
    err.clean();
    //Setting Result details in BO
    //DML#119 Creation id for creating BO of Results
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 genResult(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<int> disResult(1, 60);
    if (patientlst.size() == 1) {
        if (orderlst.size() == 1) {
            IManualOrder ord = orderlst.begin()->second;
            if (ord.isEmpty() == false) {
                ord.patient = std::move(patientlst.begin()->second);
                objBO.result.manualOrder = std::move(ord);
            } else {
                orderlst.begin()->second.patient = std::move(patientlst.begin()->second);
                objBO.result.manualOrder = std::move(orderlst.begin()->second);
                objBO.result.finalResult.Map.clear();
            }
        } else {
			//Multiple orders in DeviceMessage for Results
            IManualOrder ord = orderlst.begin()->second;
            ord.patient = patientlst.begin()->second;
            objBO.result.manualOrder = std::move(ord);
            objBO.result.finalResult.Map.clear();
        }
        //DML#119 Setting id for Patient its Order and its Result
        objBO.result.manualOrder.patient.idPatient = disResult(genResult);
        objBO.result.finalResult.idResult = objBO.result.manualOrder.patient.idPatient;
        objBO.result.manualOrder.manualOrderBase.idOrder = objBO.result.manualOrder.patient.idPatient;

    } else
        objBO.result.finalResult.Map.clear(); //Multiple Patients in DeviceMessage for Results
	//Creating HL_BYPASSED_RECORD_ERROR for frames if DeviceMessage for results have no parameters in Result Map
    if (objBO.result.finalResult.Map.size() == 0)
        handleReceiveMessageError(err, objData, EHighLevelErrors::HL_BYPASSED_RECORD_ERROR);
}

/**
 *  @brief generateDeviceMessageOrder
 *  @author HIN-SB
 *  @date
 *  @version 1.0
 *  @param reference of DeviceMessage, Map of Patient with Level info as key, Map of Order with Level info as key
 *  @section DESCRIPTION
 *  Generate DeviceMessage(std::vector<IManualOrder> orders[this contains Patient inside IManualOrder]) for Received data
 *  @return void
 */
void ASTMHLProtocol::generateDeviceMessageOrder(DeviceMessage &objBO, std::map<short, IPatient> &patientlst, std::map<short, IManualOrder> &orderlst) {
    HLProtocolError err;
    err.clean();
    for (auto outer_iter=orderlst.begin(); outer_iter!=orderlst.end(); ++outer_iter) {
        //DML#119 Creation id for creating BO of OrderList
        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        std::mt19937 genOrder(rd()); //Standard mersenne_twister_engine seeded with rd()
        std::uniform_int_distribution<int> disOrder(1, 60);
        if (patientlst.size() == 1) {
			//Handling One Patient BO with Empty, one Order BO
            IManualOrder order = outer_iter->second;
            if (order.isEmpty() == false) {
                //DML#119 Setting id for Patient and Order(Patient with Multiple Orders)
                order.manualOrderBase.idOrder = disOrder(genOrder);
                order.patient = patientlst.begin()->second;
                order.patient.idPatient = order.manualOrderBase.idOrder;
                objBO.orders.emplace_back(std::move(order));
            }

        } else if (patientlst.size() > 1) {
            //DML#119 Setting id for Patient its respective Order
            short sKey = outer_iter->first;
            IManualOrder order = outer_iter->second;
            if (order.isEmpty() == true) {
				//Handling Patient Without Orders
                IPatient pat = patientlst.find(sKey)->second;
                pat.idPatient = disOrder(genOrder);
                objBO.patientsWithoutOrders.emplace_back(std::move(pat));
            }
            else {
				//Number Orders equal to Number of Patients
                order.patient = std::move(patientlst.find(sKey)->second);
                order.patient.idPatient = disOrder(genOrder);
                order.manualOrderBase.idOrder = order.patient.idPatient;
                objBO.orders.emplace_back(std::move(order));
            }
        } else {
			//Handling Orders with Patients
            //DML#119 Setting id only for Order and Patient Id is set to -1
            IManualOrder order = outer_iter->second;
            order.manualOrderBase.idOrder = disOrder(genOrder);
            order.patient.idPatient = INTEGER_DEFAULT_VALUE;
            if (order.isEmpty() == false)
                objBO.orders.emplace_back(std::move(order));
        }
    }
}

/**
 *  @brief validateMessage
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param objBO[pointer of BO],sCurrent[frame associated with BO],sChildFrame[child frame of parent frame(comments)],bParseMessage[Parsing of BO success[True], Parsing of BO fails[False], m_commentHlErrs[List of HL Error for Comment frames]
 *  @section DESCRIPTION
 *  Validates Different Business Objects(IHeader, IPatient, IManualOrder, IHematoParam)
 *  @return True(BO validation Successfull), False(BO validation Fails)
 */
bool ASTMHLProtocol::validateMessage(void *objBO, std::string sCurrent, std::vector<std::string> &sChildFrame, bool bParseMessage, std::list<HLProtocolError> m_commentHlErrs) {
    unsigned char RecordId = sCurrent[0];
    if (bParseMessage == false) {   
        if (RecordId == Test_Header_def) {
            IManualOrderBase *ptr = static_cast<IManualOrderBase *>(objBO);
            ptr->clean();
        }
        return (false);
    }
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_VALIDATE_MESSAGE,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
    bool bRet = true;
    HLProtocolError error;
    error.clean();
	std::vector<std::string> data;
    for (auto commentParsingHLError : m_commentHlErrs) {
       handleReceiveMessageError(commentParsingHLError, data);
    }

    switch (RecordId) {
    case Header_Header_def: {
        IHeader *ptr = static_cast<IHeader *>(objBO);
        bRet = m_receiveValidator.validateHeader(*ptr, sCurrent);
        break;
    }
    case Patient_Header_def: {
        IPatient *ptr = static_cast<IPatient *>(objBO);
        bRet = m_receiveValidator.validatePatient(*ptr, sCurrent);
        validateCommentMessage(objBO, sCurrent, sChildFrame);
    }
        break;
    case Test_Header_def: {
        IManualOrderBase *ptr = static_cast<IManualOrderBase *>(objBO);
        bRet = m_receiveValidator.validateOrder(*ptr, sCurrent);
        if (bRet == false)
            ptr->clean();
        else
            validateCommentMessage(objBO, sCurrent, sChildFrame);
    }
        break;
    case Result_Header_def: {
        IHematoParam *ptr = static_cast<IHematoParam *>(objBO);
        bRet = m_receiveValidator.validateResult(*ptr, sCurrent);
        validateCommentMessage(objBO, sCurrent, sChildFrame);
    }
        break;
    default:
        break;
    }
	//Managing HLProtocolError errors for Child Frames
    if (bRet == false) {
        error = m_receiveValidator.getHLError();
        handleReceiveMessageError(error, sChildFrame);
        return bRet;
    }
     return bRet;
}

/**
 *  @brief validateCommentMessage
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param void *objBO(pointer to IPatient,
 *						pointer to IManualOrderBase,
 *						pointer to IHematoParam), std::string sCurrentFrame(Patient, Order or Result frame), 
 *						std::vector<std::string> &sChildFrame(Comment frame associated with patient, Order or Result)
 *  @section DESCRIPTION
 *  Validates Different Comment Business Objects associated with Patient Order or Result
 *  If Validation is false then clears comment BO otherwise add Comment BO to to respective Patient Order or Result
 *  @return bool True(Comment validation Successfull), False(Comment validation Fails)
 */
bool ASTMHLProtocol::validateCommentMessage(void *objBO, std::string sCurrentFrame, std::vector<std::string> &sChildFrame) {
    unsigned char RecordId = sCurrentFrame[0];
    m_refLog.createLog(ECategory::ASTM_HLPROTOCOL,TraceabilityMessage::MSG_ASTMHL_VALIDATE_COMMENT_MESSAGE,ELevel::INFO,__FUNCTION__,__LINE__,EDirection::LIS_IN);
    bool bRet = true;
    std::vector<ICommentFreeText> commentList;
    commentList.clear();
    switch (RecordId) {
    case Patient_Header_def: {
        IPatient *ptrPatient = static_cast<IPatient *>(objBO);
        if (ptrPatient->comment.isEmpty() == false)
            bRet = m_receiveValidator.validateCommentFreeTest(ptrPatient->comment, sChildFrame[0]);
        if (bRet == false) {
            //BUG#373 HIN-SB In case of 'Comment BO' validation failure handling its remaining HL Errors
            sChildFrame.clear();
            handleReceiveMessageError(m_receiveValidator.getHLError(), sChildFrame);
            ptrPatient->comment.clean();
        }
        break;
    }
    case Test_Header_def: {
        IManualOrderBase *ptrOrder = static_cast<IManualOrderBase *>(objBO);
        commentList.assign(ptrOrder->comment.commentFreeText.begin(), ptrOrder->comment.commentFreeText.end());
        ptrOrder->comment.commentFreeText.clear();
        unsigned iCounter = 0;
        for (auto comment : commentList) {
            bRet = m_receiveValidator.validateCommentFreeTest(comment, sChildFrame[iCounter]);
            if (bRet == false) {
                //BUG#373 HIN-SB In case of 'Comment BO' validation failure handling its remaining HL Errors
                sChildFrame.clear();
                handleReceiveMessageError(m_receiveValidator.getHLError(), sChildFrame);
            } else {
                ptrOrder->comment.commentFreeText.emplace_back(comment);
            }
            iCounter = iCounter + 1;
        }
        break;
    }
    case Result_Header_def: {
        IHematoParam *ptrResult = static_cast<IHematoParam *>(objBO);
        commentList.assign(ptrResult->comment.commentFreeText.begin(), ptrResult->comment.commentFreeText.end());
        ptrResult->comment.commentFreeText.clear();
        unsigned iCounter = 0;
        for (auto comment : commentList) {
            bRet = m_receiveValidator.validateCommentFreeTest(comment, sChildFrame[iCounter]);
            if (bRet == false) {
                //BUG#373 HIN-SB In case of 'Comment BO' validation failure handling its remaining HL Errors
                sChildFrame.clear();
                handleReceiveMessageError(m_receiveValidator.getHLError(), sChildFrame);
            } else {
                ptrResult->comment.commentFreeText.emplace_back(comment);
            }
            iCounter = iCounter + 1;
        }
        break;
    }
    default:
        break;
    }

    return bRet;
}

/**
 *  @brief ~ASTMHLProtocol
 *  @author HIN-SB
 *  @date    12 July, 2017
 *  @version 1.0
 *  @param
 *  @return
 */
ASTMHLProtocol::~ASTMHLProtocol() {
}


/**
 *  @brief setInstrumentSettings Sets jointly User settings and Open list settings.
 *  @author HIN-RK
 *  @date    
 *  @version 1.0
 *  @param instrumentSettings
 *  @return void
 *	@throws InstrumentSettingsError
 */
void ASTMHLProtocol::setInstrumentSettings(const InstrumentSettings &instrumentSettings) {
    m_receiveValidator.setInstrumentSettingDV(instrumentSettings);
}

/**
 *  @brief encodeManufacturerFrame
 *  @author HIN-SB
 *  @date
 *  @version 1.0
 *  @param ASTMEncode &objASTMEncode, DeviceMessage &objBO, std::vector<std::string> &objData, ASTMStructure &objASTMStruct
 *  @section DESCRIPTION
 *  Create ASTM Message(Comprising of ASTM frames) from ASTMStructure for Manufacturer BO
 *  @return void
 */
void ASTMHLProtocol::encodeManufacturerFrame(ASTMEncode &objASTMEncode, _DeviceMessage &objBO, std::vector<std::string> &objData, ASTMStructure &objASTMStruct) {
    const int MAX_SIZE_RECORD = 1024;
    //Creation Header Frame
    objASTMEncode.fillHeader(objBO.header);
    ASTMFrameCreator frameCreator(m_refLog, &objASTMStruct, m_LoincSetting);
    std::string acHeader = {};
    frameCreator.createHeader(acHeader, &objASTMStruct.m_header); //Create Header Frame
    objData.emplace_back(std::move(acHeader)); //Place Header Frame in ASTMMessage

    //Creation MF Frames
    unsigned short iSeqNum = 1;
    bool bRet = false;

    //Creation Remote Frame
    MFRemoteASTMEncode remoteEncode(&objASTMStruct); //Create object of MFRemoteASTMEncode
    std::string strRemote;
    for (auto remoteBO : objBO.manufacturerMessages.remoteCommand) { //iterate remoteCommand BO List
        strRemote.clear();
        bRet = remoteEncode.encode(remoteBO, strRemote, iSeqNum); //Encode RemoteCommand BO to generate Frame
        if (bRet == true) {
            objData.emplace_back(std::move(strRemote)); //Place RemoteCommand Frame in ASTMMessage
            iSeqNum += 1;
        }
    }

    //Creation statistics Frame
    MFStatisticASTMEncode statsEncode(&objASTMStruct); //Create object of MFStatisticASTMEncode

    std::string strStats;
    for (auto statsBO : objBO.manufacturerMessages.statistic) { //iterate Statistic BO List
        strStats.clear();
        bRet = statsEncode.encode(statsBO, strStats, iSeqNum); //Encode Statistic BO to generate Frame
        if (bRet == true) {
            objData.emplace_back(std::move(strStats)); //Place Statistic Frame in ASTMMessage
            iSeqNum += 1;
        }
    }
    //Creation Status Frame
    MFStatusASTMEncode statusEncode(&objASTMStruct); //Create object of MFStatusASTMEncode
    std::string strStatus;
    for (auto statusBO : objBO.manufacturerMessages.status) { //iterate Status BO List
        strStatus.clear();
        bRet = statusEncode.encode(statusBO, strStatus, iSeqNum); //Encode Status BO to generate Frame
        if (bRet == true) {
            objData.emplace_back(std::move(strStatus)); //Place Status Frame in ASTMMessage
            iSeqNum += 1;
        }
    }
    //Creation RackMap Frame
    MFRackMapASTMEncode rackMapEncode(&objASTMStruct); //Create object of MFRackMapASTMEncode
    std::string strRackMap;
    for (auto rackMapBO : objBO.manufacturerMessages.conveyorRackMapping) { //iterate RackMap BO List
        strRackMap.clear();
        bRet = rackMapEncode.encode(rackMapBO, strRackMap, iSeqNum); //Encode RackMap BO to generate Frame
        if (bRet == true) {
            objData.emplace_back(std::move(strRackMap)); //Place RackMap Frame in ASTMMessage
            iSeqNum += 1;
        }
    }
    //Creation Rack Tranfer Frame
    MFRackTranferASTMEncode rackTrfEncode(&objASTMStruct); //Create object of MFRackTranferASTMEncode
    std::string strRackTrf;
    for (auto rackTrfBO : objBO.manufacturerMessages.conveyorRackTransfer) { //iterate RackTranfer BO List
        strRackTrf.clear();
        bRet = rackTrfEncode.encode(rackTrfBO, strRackTrf, iSeqNum); //Encode RackTranfer BO to generate Frame
        if (bRet == true) {
            objData.emplace_back(std::move(strRackTrf)); //Place RackTranfer Frame in ASTMMessage
            iSeqNum += 1;
        }
    }
    //Creation Event Frame
    MFEventASTMEncode eventEncode(&objASTMStruct); //Create object of MFEventASTMEncode
    std::string strEvent;
    for (auto eventBO : objBO.manufacturerMessages.event) { //iterate Event BO List
        strEvent.clear();
        bRet = eventEncode.encode(eventBO, strEvent, iSeqNum); //Encode Event BO to generate Frame
        if (bRet == true) {
            objData.emplace_back(std::move(strEvent)); //Place Event Frame in ASTMMessage
            iSeqNum += 1;
        }
    }

    //Creation of Terminator Frame
    char acTerminator[MAX_SIZE_RECORD] = {};
    frameCreator.createTerminator(1, acTerminator, 'N'); //Creata Terminator Frame
    objData.emplace_back(std::move(acTerminator)); //Place Terminator Frame in ASTMMessage
}
