/*
 * Date_Time.h
 *
 *  Created on: Jul 17, 2017
 *      Author: horiba
 */

#ifndef COMMON_DATE_TIME_H_
#define COMMON_DATE_TIME_H_
#include <chrono>
class DateTime final {
    public:

        static std::string dateToString();
        static timespec timespec_from_ms (const uint32_t millis);
        static std::string dateToStringWithMilliSec();
        static bool validateDatetime(std::string dateTime, std::string datetimeFormat, tm &time);
        static bool convertDateTimeToDuration(tm &time, int miliSeconds, std::chrono::high_resolution_clock::duration &totalDuration);
        static bool convertDateToTimePoint(const std::string date, const std::string dateFormat, std::chrono::high_resolution_clock::time_point& tp);
        static bool isleapYear(std::string &dateTime);
    };
#endif /* COMMON_DATE_TIME_H_ */
