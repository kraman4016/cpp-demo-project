/*
 * Date_Time.cpp
 *
 *  Created on: Jul 17, 2017
 *      Author: HIN-SB
 */
#include <string>
#include <cstring>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <iomanip>      // std::get_time
#include <ctime>        // struct std::tm
#include <sys/time.h> // gettimeofday
#include "Date_Time.h"

// this Buffer Length is used in dateToStringWithMilliSec()
// Previously set to value 30 which was generating critical warning.
// checked with value 1024, which suppressed the warning.
// here set the value to 100 to re-check for occurance of any critical warning (Normally it should not)
const int MAX_BUFFER_LENGTH = 100;

/**
 *  dateToStringWithMilliSec() will convert system current date and time in to string
 *  @author  HIN-RK
 *  @date    June 1, 2018
 *  @version 1.0
 *  @parameter void
 *  @return string -> which will contain Date/Time with milliseconds in following format YYYY-MM-DD HH:MM:SS.mmm
 *  @section DESCRIPTION
 *	it will help to convert calendar time to printable format (YYYY-MM-DD HH:MM:SS.mmm).
 */
std::string DateTime::dateToStringWithMilliSec() {
    char buffer[MAX_BUFFER_LENGTH];
    char buffer2[MAX_BUFFER_LENGTH];
    struct timeval tv = {};
    time_t curtime;
    memset(buffer,0,sizeof(buffer));
	memset(buffer2,0,sizeof(buffer));
	
    gettimeofday(&tv, NULL);
    curtime = tv.tv_sec;

    // Date/Time with milliseconds in string format “YYYY-MM-DD HH:MM:SS.mmm”
    strftime(buffer, 30, "%Y-%m-%d %H:%M:%S.", localtime(&curtime));
    sprintf(buffer2, "%s%03d", buffer, (int)(tv.tv_usec/1000)); // divide microseconds by 1000 to get milliseconds

    std::string dateTimeStr(buffer2);
    return (dateTimeStr);
}

/**
 *  isleapYear() will check if Year is leap Year or not
 *  @author  HIN-SB
 *  @date    May 18, 2020
 *  @version 1.0
 *  @parameter reference string contains dataTime value
 *  @return True: Leap year, False: Not a leap year
 *  @section DESCRIPTION
 *	check if Year is a leap Year or not.
 */
bool DateTime::isleapYear(std::string &dateTime) {
    int year = atoi(dateTime.substr(0, 4).c_str());
    if (year % 4 == 0) {
        if (year % 100 == 0) {
            // the year is a leap year if it is divisible by 400.
            if (year % 400 == 0)
                return (true);
            else
                return (false);
        } else
            return (true);
    } else {
        return (false);
    }
}

/**
 *  @brief validateDatetime()
 *  @author HIN-SB
 *  @date  30 May, 2019
 *  @version 1.0
 *  @param   :
 *	std::string dateTime(value)
 *	std::string dateTimeFormat(
 *                          "YYYY-MM-DD",
 *                          "YYYY-MM-DD HH:MM:SS.mmm",
 *                          "YYYYMMDD",
 *                          "YYYYMMDDHHmmss")
 *	tm &time(time structure is filled with values in dateTime)
 *  @return bool: True for Validation sucessful, False for Validation Failure
 *  @section DESCRIPTION
 *	Validates dateTime value with respect to format specified in dateTimeFormat and fills time struct with values of dateTime
 */
bool DateTime::validateDatetime(std::string dateTime, std::string dateTimeFormat, tm &time) {
    int year = 0;
    int month = 0;
    int days = 0;
    int hour = 0;
    int minute = 0;
    int sec = 0;
    int tzh = 0;
    bool bRet = true;
    char buffer[80];
    memset(buffer,0,sizeof(buffer));
    const std::string formatDateTraceLogs = "YYYY-MM-DD"; //date format used LIS-SSM interface methods like getTracebilityLog
    const std::string formatDateTime = "YYYYMMDDHHmmss"; //date time format is used in DeviceMessage fields
    const std::string formatDate = "YYYYMMDD";           //date time format is used in DeviceMessage fields
    const std::string formatMiliSec = "YYYY-MM-DD HH:MM:SS.mmm"; //date time format used LIS-SSM interface methods like getErrorLog

	if (dateTime.empty() == true) {
        dateTimeFormat = "";
    }
    if (formatDateTraceLogs == dateTimeFormat) {
       dateTime = dateTime + " 00:00:00.000" ;
       dateTimeFormat = formatMiliSec;
    }

    if (formatDateTime == dateTimeFormat || formatDate == dateTimeFormat) {
        //if date time format is YYYYMMDDHHmmss or YYYYMMDD
        auto pos = std::find_if(dateTime.begin(), dateTime.end(), [](unsigned char c) { return (std::isdigit(c) == false); });
        bool isNumeric = (pos == dateTime.end());	
        if (isNumeric == false)
            //dateTime parameter has characters other than integer
            bRet = false;
        else {
            if (formatDateTime == dateTimeFormat) {
#ifdef __WIN32__
				//This section uses get_time to convert dateTime String to tm Struct as strpTime is not supported by windows
                std::istringstream dateTimeStream(dateTime.c_str());
                dateTimeStream >> std::get_time(&time, "%Y%m%d%H%M%S"); //Convert from datetime string to tm struct
                if (dateTimeStream.fail() == true) {
                    bRet = false;
                }
#else
				//This section strpTime to convert string to tm struct	
				strptime(dateTime.c_str(), "%Y%m%d%H%M%S", &time); //Convert from datetime string to tm struct
				strftime(buffer, sizeof(buffer), "%Y%m%d%H%M%S", &time); //Convert tm struct to datetime string 
                bRet = (strcmp(buffer, dateTime.c_str()) == 0);
#endif				
            } else {
#ifdef __WIN32__
				//Usage of get_time to convert dateTime String to tm Struct as strpTime is not supported by windows
                std::istringstream dateTimeStream(dateTime.c_str());
                dateTimeStream >> std::get_time(&time, "%Y%m%d"); //Convert from datetime string to tm struct
                if (dateTimeStream.fail() == true) {
                    bRet = false;
                }			
#else	
				//Usage of strpTime to convert string to tm struct	
				strptime(dateTime.c_str(), "%Y%m%d", &time); //Convert from datetime string to tm struct
				strftime(buffer, sizeof(buffer), "%Y%m%d", &time); //Convert tm struct to datetime string
				//buffer is getting compared with dateTime string
                bRet = (strcmp(buffer, dateTime.c_str()) == 0);
#endif				
            }
        }
    } else if (formatMiliSec == dateTimeFormat) {
        //if date time format is YYYY-MM-DD HH:MM:SS.mmm
        const int DATETIME_LEN = 23;
        const int MAX_INPUT_DATETIME_FIELDS = 6;
        if (dateTime.length() != DATETIME_LEN) {
            bRet = false;
        } else {
            int retInput = sscanf(dateTime.c_str(), "%d-%d-%d %d:%d:%d.%dZ", &year, &month, &days, &hour, &minute, &sec, &tzh);
            if (retInput < MAX_INPUT_DATETIME_FIELDS) {
                bRet = false;
            } else {
#ifdef __WIN32__
				//This section uses get_time to convert dateTime String to tm Struct as strpTime is not supported by windows
				
                //datetime without miliseconds
                std::string dateTimeNoMs = dateTime.substr(0, 19);
                std::istringstream dateTimeStream(dateTimeNoMs.c_str());
                dateTimeStream >> std::get_time(&time, "%Y-%m-%d %H:%M:%S");//Convert from datetime string to tm struct
                if (dateTimeStream.fail() == true) {
                    bRet = false;
                }
                //Extract miliSeconds from dateTime
                int milliSeconds = atoi(dateTime.substr(20, 3).c_str());
				
				//bRet should be true and milliseconds should be equal to retrieved miliseconds from datTime string in order to get successful validation
                bRet = ((bRet == true) && (milliSeconds == tzh));
#else	
				//This section uses strpTime to convert string to tm struct
			
                //datetime without miliseconds
                std::string dateTimeNoMs = dateTime.substr(0, 19);
				strptime(dateTimeNoMs.c_str(), "%Y-%m-%d %H:%M:%S", &time); //Convert from datetime string to tm struct
                strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &time); //Convert tm struct to datetime string

                //Extract miliSeconds from dateTime
                int milliSeconds = atoi(dateTime.substr(20, 3).c_str());
				
				//buffer is getting compared with dateTime string without milisecond and milliseconds are compared separately
                bRet = ((strcmp(buffer, dateTimeNoMs.c_str()) == 0) && (milliSeconds == tzh)); 
#endif				
            }
        }
    } else {
        bRet = false;
    }
    if (bRet == true) {
        //April, June, Sep, Nov do not have 31 days
        if (((time.tm_mon == 3) || (time.tm_mon == 5) || (time.tm_mon == 8) || (time.tm_mon == 10)) && (time.tm_mday == 31)) {
            bRet = false;
        }
        if (time.tm_mon == 1 ) { //check for February
            bool isleap = isleapYear(dateTime); //isLeap bool to store result leap year
            //Feb do nat have 30, 31 days and Feb do have 29 days if year is not a Leap year
            if (((time.tm_mday == 30) || (time.tm_mday == 31)|| ((time.tm_mday == 29) && (isleap == false)))) {
                bRet = false;
            }
        }
    }
    return bRet;
}

/**
 *  @brief convertDateTimeToDuration is used to convert calendar Datetime with milliseconds to high_resolution_clock duration
 *  @author HIN-SB
 *  @date  30 May, 2019
 *  @version 1.0
 *  @param   :
 *	tm struct(date time value till seconds filled),
 *  milliSeconds,
 *	totalDuration(contains dateTime for comparison)
 *  @return bool True for Successful conversion of tm to high_resolution_clock, False for failure
 *  @section DESCRIPTION
 *	used to convert calendar Datetime with milliseconds to high_resolution_clock duration
 */
bool DateTime::convertDateTimeToDuration(tm &time, int miliSeconds, std::chrono::high_resolution_clock::duration &totalDuration) {
	bool bRet = true;
    //Convert tm struct to time_t
    std::time_t tt = std::mktime(&time);

    if (tt == -1){
        bRet = false;
    } else {
		//Convert time_t to struct high_resolution_clock::duration
		std::chrono::high_resolution_clock::duration dur = std::chrono::high_resolution_clock::from_time_t(tt).time_since_epoch();

		//Convert miliSeconds to struct high_resolution_clock::duration
		std::chrono::high_resolution_clock::duration ms = std::chrono::milliseconds(miliSeconds);

		//Add miliSeconds to time
		totalDuration = dur + ms;
	}

    return bRet;
}

/**
 *  @brief dateToString() will convert system current date and time in to string
 *  @author  HIN-PS
 *  @date    20 September, 2017
 *  @version 1.0
 *  @parameter void
 *  @return string value : -> string which will contain date and time in string format
 *  @section DESCRIPTION
 *	- it will help to convert system current date and time in to string.
 **/

std::string DateTime::dateToString() {

        time_t rawtime;
        struct tm * timeinfo;
        char buffer[80];
        memset(buffer,0,sizeof buffer);

        time(&rawtime);
        timeinfo = localtime(&rawtime);
        strftime(buffer, 80, "%Y%m%d%H%M%S", timeinfo);
        std::string str(buffer);
        return (str);

}

/**
 *  @brief timespec_from_ms() will convert milli second to timespec structure to be used by pselect call in RS232
 *  @author  HIN-PS
 *  @date
 *  @version 1.0
 *  @parameter milliSeconds
 *  @return timespec variable
 *  @section DESCRIPTION
 *	will convert milli second to timespec structure to be used by pselect call in RS232
 **/
timespec DateTime::timespec_from_ms (const uint32_t millis) {
        timespec time;
        time.tv_sec = millis / 1e3;
        time.tv_nsec = (millis - (time.tv_sec * 1e3)) * 1e6;
        return time;
}

/**
 *  @brief convertDateToTimePoint, constructs a new time point from date(discard the time info if supplied).
 *  @author  HIN-RK
 *  @date    June 19, 2019
 *  @version 1.0
 *  @param        date - input date
 *          dateFormat - date format (YYYY-MM-DD HH:MM:SS.mmm, YYYYMMDD, ...)
 *                  tp	- reference to time_point, contains a new time_point representing input date
 *  @return isDateValid
 *  			 true - if it completes date format check successfully
 *  			false - if date format check fails
 *  @section DESCRIPTION
 *              create time_point type from date string(time info will be discarded if supplied)
 *              Conversion: Date(string value) -> struct tm(broken-down time) -> struct time_t(calender time) -> time_point
 */
bool DateTime::convertDateToTimePoint(const std::string date, const std::string dateFormat, std::chrono::high_resolution_clock::time_point& tp) {
    struct std::tm t;
    bool isDateValid = DateTime::validateDatetime(date, dateFormat, t);
    // make time fields empty
    t.tm_sec = 0;
    t.tm_min = 0;
    t.tm_hour = 0;

    if (isDateValid == true) {      // If Date is valid
        std::time_t tt = std::mktime(&t);       // Convert broken-down time(struct tm) to calender time(time_t)
        tp = std::chrono::high_resolution_clock::from_time_t(tt);       // Converts tt(time_t) to a time point type(time_point)
    }
    return isDateValid;
}
