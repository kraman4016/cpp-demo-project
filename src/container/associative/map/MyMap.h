#ifndef MYMAP_H
#define MYMAP_H

#include <map>


//namespace Demo {

enum class ERecordFields : short {
	// ASTM-CI SRS CHAPTER 5.3.9.2 TRACEABILITY MESSAGE
	FN_14_1_TR_RECORD_TYPE_ID = 0,
	FN_14_2_TR_SEQUENCE_NUMBER,
	FN_14_3_TR_MESSAGE_TYPE,
	FN_14_4_TR_TRACEABILITY_NAME,
	FN_14_5_TR_TRACEABILITY_INFORMATION,
	FN_14_5_TR_TRACEABILITY_INFORMATION_REAGENT_TYPE,
	FN_14_5_TR_TRACEABILITY_INFORMATION_LOADED_DATETIME,
	FN_14_5_TR_TRACEABILITY_INFORMATION_EXPIRATION_DATE,
	FN_14_5_TR_TRACEABILITY_INFORMATION_STATUS,
	FN_14_5_TR_TRACEABILITY_INFORMATION_VALUE,                   // added as per DML# 115: Value for SETTINGS Message type
        NONE						  // Reserved for cases when error does not refer to a given field
};

enum class EDataType : char {
	NONE=0,
	STRING,
	FIXED,
	CLOSED_LIST,
	OPEN_LIST,
	INTEGER,
	DATE_TIME,
	DATE
};

typedef struct _MapEDataType {
	static const std::map<std::string, EDataType> strToEnum;

} MapEDataType;

class MyMap {
	public:
		MyMap();
 		virtual ~MyMap();

		void run();
                void example_1();
                void example_2();
	private:
};
/*}
using Demo::MyMap;
using Demo::EDataType;
using Demo::MapEDataType;*/
#endif /* MYMAP_H */


