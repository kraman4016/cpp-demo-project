#include "MyMap.h"

// https://en.cppreference.com/w/cpp/container/map/map
#include <iostream>
#include <string>
#include <iomanip>
#include <map>

// https://en.cppreference.com/w/cpp/container/map/map
template<typename Map>
void print_map(Map& m)
{
   std::cout << '{';
   for(auto& p: m)
        std::cout << p.first << ':' << p.second << ' ';
   std::cout << "}\n";
}
 
struct Point { double x, y; };
struct PointCmp {
    bool operator()(const Point& lhs, const Point& rhs) const { 
        return lhs.x < rhs.x; // NB. intentionally ignores y
    }
};


template<typename T1, typename T2>
void findValue(T2 t2) {
}

template<typename M, typename K, typename V>
void genericFunction(M& map, K key, V& value) {
    std::cout << __func__ << '\n';
    auto it = map.find(key);
    if (it != map.end()) {
        std::cout << static_cast<uint16_t>(it->second) << '\n';
        value = it->second;
    }
    for (auto& em : map) {
        std::cout << em.first << '\n';
    }
}

const std::map<std::string, EDataType> MapEDataType::strToEnum = {
	{"NONE", EDataType::NONE},
	{"STRING", EDataType::STRING},
	{"FIXED", EDataType::FIXED},
	{"STRING", EDataType::STRING},
	{"CLOSED_LIST", EDataType::CLOSED_LIST},
	{"OPEN_LIST", EDataType::OPEN_LIST},
	{"INTEGER", EDataType::INTEGER},
	{"DATE_TIME", EDataType::DATE_TIME},
	{"DATE", EDataType::DATE}
};




MyMap::MyMap() {
	run();
}

MyMap::~MyMap() {
}

void MyMap::run() {
    example_1();
    example_2();
}

void MyMap::example_1() {
    std::cout << __func__ << '\n';
    // (1) Default constructor
    std::map<std::string, int> map1;
    map1["something"] = 69;
    map1["anything"] = 199;
    map1["that thing"] = 50;
    std::cout << "map1 = "; print_map(map1);

    // (2) Range constructor
    std::map<std::string, int> iter(map1.find("anything"), map1.end());
    std::cout << "\niter = "; print_map(iter);
    std::cout << "map1 = "; print_map(map1);

    // (3) Copy constructor
    std::map<std::string, int> copied(map1);
    std::cout << "\ncopied = "; print_map(copied);
    std::cout << "map1 = "; print_map(map1);

    // (4) Move constructor
    std::map<std::string, int> moved(std::move(map1));
    std::cout << "\nmoved = "; print_map(moved);
    std::cout << "map1 = "; print_map(map1);

    // (5) Initializer list constructor
    const std::map<std::string, int> init {
    {"this", 100},
    {"can", 100},
    {"be", 100},
    {"const", 100},
    };
    std::cout << "\ninit = "; print_map(init);
    

    // Custom Key class option 1:
    // Use a comparison struct
    std::map<Point, double, PointCmp> mag = {
    { {5, -12}, 13 },
    { {3, 4},   5 },
    { {-8, -15}, 17 }
    };

    for(auto p : mag)
    std::cout << "The magnitude of (" << p.first.x
            << ", " << p.first.y << ") is "
            << p.second << '\n';

    // Custom Key class option 2:    
    // Use a comparison lambda
    // This lambda sorts points according to their magnitudes, where note that
    //  these magnitudes are taken from the local variable mag
    auto cmpLambda = [&mag](const Point &lhs, const Point &rhs) { return mag[lhs] < mag[rhs]; };
    //You could also use a lambda that is not dependent on local variables, like this:
    //auto cmpLambda = [](const Point &lhs, const Point &rhs) { return lhs.y < rhs.y; };
    std::map<Point, double, decltype(cmpLambda)> magy(cmpLambda);

    //Various ways of inserting elements:
    magy.insert(std::pair<Point, double>({5, -12}, 13));
    magy.insert({ {3, 4}, 5});
    magy.insert({Point{-8.0, -15.0}, 17});

    std::cout << '\n';
    for(auto p : magy)
    std::cout << "The magnitude of (" << p.first.x
            << ", " << p.first.y << ") is "
            << p.second << '\n';    

    // DO-THINGS
    std::map<std::string, int>::iterator it;
    std::map<std::string, int>::const_iterator cit;   
    // compilation-error: error: no match for ‘operator=’ (operand types are ‘std::map<std::__cxx11::basic_string<char>, int>::iterator {aka std::_Rb_tree_iterator<std::pair<const std::__cxx11::basic_string<char>, int> >}’ and ‘std::map<std::__cxx11::basic_string<char>, int>::const_iterator {aka std::_Rb_tree_const_iterator<std::pair<const std::__cxx11::basic_string<char>, int> >}’)
    //it = init.find("this");
    // If the map object is const-qualified, the function returns a const_iterator. Otherwise, it returns an iterator.
    cit = init.find("this");
}

void MyMap::example_2() {
    std::cout << __func__ << '\n';
    ERecordFields er;
    std::cout << "" << static_cast<int16_t>(ERecordFields::NONE) << '\n'; 

    // C++14 COMPILABLE CODE 
    const int x = 0;
    auto x1 = x; // int
    decltype(auto) x2 = x; // const int

    // simple comparison demo
    std::map<int,char> example = {{1,'a'},{2,'b'}};
 
    auto search = example.find(2);
    if (search != example.end()) {
        std::cout << "Found " << search->first << " " << search->second << '\n';
    } else {
        std::cout << "Not found\n";
    }
    std::string key = "DATE_TIME";
    EDataType dataType;
    //findValue<std::string, MapEDataType::strToEnum>(MapEDataType::strToEnum);
    genericFunction(MapEDataType::strToEnum, key, dataType);
    std::cout << static_cast<uint16_t>(dataType) << '\n';
}
