#ifndef MYVECTOR_H
#define MYVECTOR_H
namespace Demo {
class MyVector {
	public:
		MyVector();
 		virtual ~MyVector();

		void run();
        void pushBack();
        void moveList();
	private:
};
}
using Demo::MyVector;
#endif /* MYVECTOR_H */


