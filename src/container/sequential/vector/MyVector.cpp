#include "MyVector.h"

#include <iostream>
#include <vector>


MyVector::MyVector() {
	run();
}

MyVector::~MyVector() {
}

void MyVector::run() {
    pushBack();
    //moveList();
}

void MyVector::pushBack() {
    std::vector<std::string> sequentialList;
    sequentialList.push_back("1");
    sequentialList.push_back("2");
    sequentialList.push_back("3");
    sequentialList.push_back("4");
    sequentialList.push_back("5");

    for (auto &element : sequentialList) {
            std::cout << element << '\n';
    }
    std::cout << "empty: " << sequentialList.empty() << '\n';       
    std::cout << "size: " << sequentialList.size() << '\n';

    if ( (sequentialList.empty() == false) && (sequentialList.size() > 4) ) {         // First check for array: array not empty; Second check for array: array size greater than index number
            std::cout << "access: " << sequentialList[4] << '\n';           
    }        
    std::string str = sequentialList[5];
    std::cout << "access: " << sequentialList[5] << '\n';           
    
}

struct STest {
    std::vector<int> listInt;
} ;

std::vector<int> test() {
    std::vector<int> listInt2;
    return std::move(listInt2);
}

void MyVector::moveList() {
    STest sTest;
    sTest.listInt.clear();
    std::cout << std::boolalpha;
    std::cout << "empty: " << sTest.listInt.empty() << '\n';
    std::cout << "size: " << sTest.listInt.size() << '\n';
    
    // move or copy list from other function return value
    sTest.listInt = test();                     
    std::cout << "empty: " << sTest.listInt.empty() << '\n';
    std::cout << "size: " << sTest.listInt.size() << '\n';

}

