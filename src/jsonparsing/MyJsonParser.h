#ifndef MYJSONPARSER_H
#define MYJSONPARSER_H

#include <string>

namespace Demo {
class MyJsonParser {
        //static constexpr const std::string m_filename = "configuration.json";
        static constexpr const char* m_filename = "configuration.json";

	public:
		MyJsonParser();
		virtual ~MyJsonParser();

		void run();
        void parseJsonFile();
        void parseFieldSettingsFromJson();
	private:
};
}
using Demo::MyJsonParser;
#endif /* MYJSONPARSER_H */


