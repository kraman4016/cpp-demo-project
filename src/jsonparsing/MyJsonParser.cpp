#include "MyJsonParser.h"
#include "rapidjson/document.h"

#include <iostream>
#include <sstream>
#include <fstream>

MyJsonParser::MyJsonParser() {
	run();
}

MyJsonParser::~MyJsonParser() {
}

void MyJsonParser::run() {
    //parseJsonFile();
    parseFieldSettingsFromJson();
}

void MyJsonParser::parseJsonFile() {
    std::stringstream ss;
    std::ifstream ifs(m_filename, std::ios::binary);
    if (ifs) {
        ss << ifs.rdbuf(); 
        ifs.close();
        std::cout << ss.str() << "\n";
    }
    rapidjson::Document document;    
    document.Parse(ss.str().c_str());
    if (document.HasParseError()) {
        std::cout << "parse error\n";
    }
    assert(document.IsObject());
    //assert(document.HasMember("Common"));
    //assert(document.HasMember("Common_")); // void Demo::MyJsonParser::parseJsonFile(): \
                    Assertion `document.HasMember("Common_")' failed. Aborted                 (core dumped)
    std::cout << document.HasMember("Common") << document.HasMember("Common_") << '\n';

    static	const	char*	kTypeNames[]	= {	"Null",	"False",	"True",	"Object",	"Array",	"String",	"Number" };
    for	(rapidjson::Value::ConstMemberIterator	itr	=	document.MemberBegin(); itr	!=	document.MemberEnd();	++itr) {
	    printf("Type	of	member	%s	is	%s\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
    }
}

void MyJsonParser::parseFieldSettingsFromJson() {
    std::stringstream ss;
    std::string filename("field-setting.json");
    std::ifstream ifs(filename, std::ios::binary);
    if (ifs) {
        ss << ifs.rdbuf(); 
        ifs.close();
        std::cout << ss.str() << "\n";
    }
    rapidjson::Document document;    
    document.Parse(ss.str().c_str());
    if (document.HasParseError()) {
        std::cout << "parse error\n";
    }
    assert(document.IsObject());
    
    const rapidjson::Value& jFields = document["Fields"]["Field"];
    for (std::size_t i = 0; i < jFields.Size(); i++) {
        const rapidjson::Value& jField = jFields[i];
        std::string fieldName = jField["name"].GetString();
        
        //const char* direction = "TO_HOST";
        //const std::string direction2 = "FROM_HOST";   // error

        const char* directions[] = {"TO_HOST", "FROM_HOST"};
        for (auto &direction : directions) {
            std::string max_length;

            //if () 
            std::string is_used = jField[direction]["is_used"].GetString();
            if (jField[direction]["max_length"].IsString())            
                max_length = jField[direction]["max_length"].GetString();
            std::string type = jField[direction]["type"].GetString();
            std::string repeat_delimiter = jField[direction]["repeat_delimiter"].GetString();
            std::string mandatory = jField[direction]["mandatory"].GetString();
            std::string components = jField[direction]["components"].GetString();


            // print field settings
            std::cout << "field name: " << fieldName << '\n';
            std::cout << "direction: " << direction << '\n';
            std::cout << "is_used: " << is_used << '\n';
            std::cout << "max_length: " << max_length << '\n';
            std::cout << "type: " << type << '\n';
            std::cout << "repeat_delimiter: " << repeat_delimiter << '\n';
            std::cout << "mandatory: " << mandatory << '\n';
            std::cout << "components: " << components << '\n';

        }
    }

}

