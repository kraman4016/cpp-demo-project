#include <iostream>


struct compositeType {
    int32_t nb;
    char* cptr;
    char ch;
    std::string st;
};

int main() {
	std::cout << "Bonjour le monde ! intialization" << "\n";
    for (int i = 0; i < 10; i++) {
        //compositeType cp;
        //compositeType cp = {};
        compositeType cp{0};
        std::cout << "first member value: " << cp.nb << '\n';
        std::cout << "second member value: " << cp.ch << '\n';
    }
}
