#include <iostream>
#include <iterator>
#include <string>
#include <regex>
 

void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        std::cout << "pos: " << start_pos << '\n';
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}


template<typename T>
T GetInput(std::string displayInfo = ""){
    T choice;
    std::cout << displayInfo;
    std::cin >> choice;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return choice;
}


int main()
{
    std::cout << "conversions(stoi vs atoi) \n";
    {
        std::string str1 = "45";
        std::string str2 = "3.14159";
        std::string str3 = "313a37 with words";
        std::string str4 = "words and 2";
        std::size_t pos2 = 0;    
        std::size_t& pos = pos2;    

        int myint1 = std::stoi(str1);
        int myint2 = std::stoi(str2);
        int myint3 = std::stoi(str3, &pos);
        // error: 'std::invalid_argument'
        // int myint4 = std::stoi(str4);

     
        std::cout << "std::stoi(\"" << str1 << "\") is " << myint1 << '\n';
        std::cout << "std::stoi(\"" << str2 << "\") is " << myint2 << '\n';
        std::cout << "std::stoi(\"" << str3 << "\") is " << myint3 << "\t conversion stopped at index " << pos << '\n';
        //std::cout << "std::stoi(\"" << str4 << "\") is " << myint4 << '\n';
    }
    {
        const char *str1 = "42";
        const char *str2 = "3.14159";
        const char *str3 = "31337 with words";
        const char *str4 = "words and 2";
     
        int num1 = std::atoi(str1);
        int num2 = std::atoi(str2);
        int num3 = std::atoi(str3);
        int num4 = std::atoi(str4);
     
        std::cout << "std::atoi(\"" << str1 << "\") is " << num1 << '\n';
        std::cout << "std::atoi(\"" << str2 << "\") is " << num2 << '\n';
        std::cout << "std::atoi(\"" << str3 << "\") is " << num3 << '\n';
        std::cout << "std::atoi(\"" << str4 << "\") is " << num4 << '\n';    
    }

    {
        std::string str = "";
        std::string str2;
        
        std::cout << "str: " << str.size() << '\n';
        std::cout << "str2: " << str2.size() << '\n'; 
    
        //str = nullptr;        
        std::cout << "str: " << str.size() << '\n';
    }
    {
        std::vector<std::string> list = {"A", "B"};
        if (false == list.empty()) {
            std::cout << "list: " <<  list[0] << '\n';
        }
        
    }
    {
        std::string str("This is a test string!");
        std::cout << str << '\n';
        str.replace(str.begin(), str.end()-1, "replace");
        std::cout << str << '\n';
        str = "AB1AB2AB3AB4BCD...";
        std::string from("A");
        std::string to("AB");
        replaceAll(str, from, to);
        std::cout << "str: " << str << '\n';
        std::cout << "from: "  << from << '\n';
        std::cout << "to: "  << to << '\n';        
    }
    {
  		std::string dateFrom = GetInput<std::string>("<     > dateFrom: ");
   		std::string dateTo = GetInput<std::string>("<     > dateTo: ");     
        
        std::cout << "from: "  << dateFrom << "size: " << dateFrom.size() << '\n';
        std::cout << "to: "  << dateTo << "size: " << dateTo.size() << '\n';            
    }
}
