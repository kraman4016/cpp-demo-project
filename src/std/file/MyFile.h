#ifndef MYFILE_H
#define MYFILE_H
namespace Demo {
class MyFile {
	public:
		MyFile();
 		virtual ~MyFile();

		void run();
                void tmpnamStd();
                void fopenStd();
                void fwriteStd();
                void fwrite2();
                void fileSync();
	private:
};
}
using Demo::MyFile;
#endif /* MYFILE_H */


