#include "MyFile.h"

#include <iostream>
#include <cstdio>
#include <cstring>
#include <array>
#include <vector>
#include <thread>
#include <sys/stat.h>


MyFile::MyFile() {
	run();
}

MyFile::~MyFile() {
}

void MyFile::run() {
    //tmpnamStd();
    //fopenStd();
    //fwriteStd();
    fwrite2();
}

void MyFile::tmpnamStd() {
	std::string name1 = std::tmpnam(nullptr);
	std::cout << "temporary file name: " << name1 << '\n';
	
	char name2[L_tmpnam];
	if (std::tmpnam(name2)) {
		std::cout << "temporary file name: " << name2 << '\n';
	}	
}

void MyFile::fopenStd() {
    FILE* fp = std::fopen("test.txt", "r");
    if(!fp) {
        std::perror("File opening failed");
        std::cout << "Failed opening file: " << errno << std::strerror(errno) << '\n';
        return;
    }
 
    int c; // note: int, not char, required to handle EOF
    while ((c = std::fgetc(fp)) != EOF) { // standard C I/O file reading loop
       std::putchar(c);
    }
 
    if (std::ferror(fp))
        std::puts("I/O error when reading");
    else if (std::feof(fp))
        std::puts("End of file reached successfully");
 
    std::fclose(fp);
}

void MyFile::fwrite2() {
    std::string filename("file2.bin");
    std::FILE* f1 = std::fopen(filename.c_str(), "wb");
    std::array<int, 3> v = {42, -1, 7}; // underlying storage of std::array is an array
    if(f1 != nullptr) {
        //std::array<int, 3> v = {42, -1, 7}; // underlying storage of std::array is an array
        int objectsWritten = std::fwrite(v.data(), sizeof v[0], v.size(), f1);
        //std::fclose(f1);
        std::cout << "fwrite return " << objectsWritten << " \n";
    }
    for(uint16_t index = 0; index < 10; index++) {
        std::this_thread::sleep_for(std::chrono::seconds(10));
        std::cout << "timeout expired. \n";
        int objectsWritten = std::fwrite(v.data(), sizeof v[0], v.size(), f1);
        std::cout << "fwrite return " << objectsWritten << " \n";
        struct stat sbuf;
        fstat(fileno(f1), &sbuf);
        std::cout << "Links to example: " << sbuf.st_nlink << std::endl;

        // common linux/unix all have the stat system call
        struct stat buffer;
        if (stat(filename.c_str(), &buffer) == 0) {
            std::cout << "file exists!" << std::endl;
        } else {
            std::cout << "file not exists!" << std::endl;
        }
    }

}


void MyFile::fwriteStd() {
    // write buffer to file
    if(std::FILE* f1 = std::fopen("file.bin", "wb")) {
        std::array<int, 3> v = {42, -1, 7}; // underlying storage of std::array is an array
        std::fwrite(v.data(), sizeof v[0], v.size(), f1);
        std::fclose(f1);
    }
    // read the same data and print it to the standard output
    if(std::FILE *f2 = std::fopen("file.bin", "rb")) {
        std::vector<int> rbuf(10); // underlying storage of std::vector is also an array
        std::size_t sz = std::fread(rbuf.data(), sizeof rbuf[0], rbuf.size(), f2);
        std::fclose(f2);
        for(std::size_t n = 0; n < sz; ++n) {
            std::printf("%d\n", rbuf[n]);
        }
    }

    FILE* fp;
    if ((fp = std::fopen("test.txt", "r")) == nullptr) {
        std::cout << "null fd: " << errno << std::strerror(errno) << '\n';
        std::array<int, 3> v = {42, -1, 7}; // underlying storage of std::array is an array
        //std::fwrite(v.data(), sizeof v[0], v.size(), fp); // Segmentation fault      (core dumped)
    }

}

void MyFile::fileSync() {
        //std::ofstream of("test-2.txt");
        
}



