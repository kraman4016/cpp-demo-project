#include "MyDate.h"

#include <iostream>
#include <chrono>
#include <cstring>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <sys/stat.h>
#include <regex>
// RK, For convertBrokenDownTimeToCalenderTime
#include <iomanip>
#include <ctime>
#include <stdlib.h>

#define STRING_DEFAULT_VALUE ""
#define INTEGER_DEFAULT_VALUE -1
#define INTEGER_ERROR_VALUE -9

MyDate::MyDate() {
	run();
}

MyDate::~MyDate() {
}

void MyDate::run() {
        testConversions();
        //convertBrokenDownTimeToCalenderTime();
        //createLogs();
        //regularExpressionDate();
        //purgeLogsBetweenDates("3");
        //bool bRet = makeTimePoint();
        //std::cout << "makeTimePoint return value: " << bRet << '\n';
}

void MyDate::testConversions() {
    std::string str = std::to_string(INTEGER_DEFAULT_VALUE);	
    std::cout << "value: " << str << "\tsize: " << str.size() << '\n';

    str = std::to_string(INTEGER_ERROR_VALUE);	
    std::cout << "value: " << str << "\tsize: " << str.size() << '\n';
    std::cout << std::boolalpha;
    std::cout << "std::is_signed<int>(): " << std::is_signed<int>() << '\n';
}
/*
void MyDate::convertBrokenDownTimeToCalenderTime() {
    setenv("TZ", "/usr/share/zoneinfo/America/New_York", 1); // POSIX-specific
 
// RK, Returns the current calendar time encoded as a std::time_t object, and also stores it in the object pointed to by arg, unless arg is a null pointer.
    std::time_t t = std::time(nullptr);         
// RK, Converts given time since epoch as std::time_t value into calendar time, expressed in local time.
// RK, Converting Between time_t and Broken-Down Time
// RK, Structure holding a calendar date and time broken down into its components.
    std::tm tm = *std::localtime(&t);   
        
    std::cout << "Today is           " << std::put_time(&tm, "%c %Z")
              << " and DST is " << (tm.tm_isdst ? "in effect" : "not in effect") << '\n';
    tm.tm_mon -= 100;  // tm_mon is now outside its normal range

// RK, Converts local calendar time to a time since epoch as a time_t object.
// RK, The mktime() function translates a broken-down time, expressed as local time,into a time_t value
    std::mktime(&tm);  // tm_dst is not set to -1; today's DST status is used

    std::cout << "100 months ago was " << std::put_time(&tm, "%c %Z")
              << " and DST was " << (tm.tm_isdst ? "in effect" : "not in effect") << '\n';
        
}

void MyDate::createLogs() {
	int keepErrorNDays =4;
	std::string filename = "Error/Log/LLErrorLog";

	const int SECONDS_PER_HOUR = 3600;
	const int HOURS_PER_DAY = 24;
        
        std::ofstream ofs(filename, std::ios_base::trunc); 	// empty the whole contents of file
        ofs.close();

        for (std::size_t i = 0; i <= keepErrorNDays + 1; i++) {
        	std::chrono::high_resolution_clock::time_point tp = std::chrono::high_resolution_clock::now(); // current point in time
	        std::chrono::duration<int,std::ratio<SECONDS_PER_HOUR * HOURS_PER_DAY>> durationDays(i); // keepErrorNDays + 1
	        tp -= durationDays;
	        time_t simpleTime = std::chrono::high_resolution_clock::to_time_t(tp); // convert Time point to simple time
	        char buffer[30];
	        std::memset(buffer, '\0', 30);
	        std::strftime(buffer, 30, "%Y-%m-%d", localtime(&simpleTime)); // convert simple time to broken down time

	        std::ofstream ofs(filename, std::ios_base::app);
	        ofs << buffer << '\n';
	        ofs.close();
        }
}

void MyDate::regularExpressionDate() {
        struct tm tm;
        time_t t;
        std::string data("On 2015-12-23, the shop will be closed, but on 2016-01-03, it will be open. Invalid date: 2016-13-32. ID: 1022015-12-233333.");
        std::regex pattern("\\b\\d{4}[-]\\d{2}[-]\\d{2}\\b");
        std::smatch result;

        while (std::regex_search(data, result, pattern)) {
            if (strptime(result[0].str().c_str(), "%Y-%m-%d", &tm) != NULL) {
              std::cout << "RK match: " << result[0] << std::endl;
            }
            data = result.suffix().str();
        }
}

bool MyDate::makeTimePoint() {
        bool bRet = false;  

        struct tm tm;
        time_t t;
        std::string data("On 2015-12-23, the shop will be closed, but on 2016-01-03, it will be open. Invalid date: 2016-13-32. ID: 1022015-12-233333.");
        data = "2019-06-10 11:40:51.244| 2019-06-11AAAA-BB-CC OR11_RECEIVE_DATETIME|LIS_IN|HL_IGNORED_RECORD_ERROR|2019-02-28 11:40:51.244<--SPM|1|sampleid_1||WB|||||||P|||DIFFTROL_LOW|||20170909090909|201709090909MM/|SPM|1|sampleid_1||WB|||||||P|||DIFFTROL_LOW|||20170909090909|201709090909MM|";
        data = "BEG 2019-06-10 11:40:51.244 END";
        std::string regex("\\b\\d{4}[-]\\d{2}[-]\\d{2}\\b");
        std::regex pattern(regex);
        std::smatch result;  


        std::cout << "data: " << data << '\n' << "rational expression: " << regex << '\n'; 
        bool isMatchExists = std::regex_search(data, result, pattern);
        if (isMatchExists == true) {
                if (result.empty() == false) {
                        std::cout << result.prefix() << '\t' << result.suffix() << '\t' << result[0].str() << '\n';
                        bRet = true;
                }
        }

        while (std::regex_search(data, result, pattern)) {
                std::cout << "RK: " << result[0].str() << '\n';
                data = result.suffix().str();
                bRet = true;
        }

        return bRet;
}

std::chrono::high_resolution_clock::time_point MyDate::makeTimePoint(std::string date) {
        std::cout << "token: " << date << '\n';
        int year = 0;
        int mon = 0;
        int day = 0;
        std::size_t found = date.find('-');
        if (found != std::string::npos) { // date foramt [YYYY-MM-DD], used in Error Log 
                //if(date.length()<9) { //DEV-BUG -> not take less than 8 digit.
        year = std::stoi(date.substr(0, strlen("YYYY"))); // string to integer conversion
        mon = std::stoi(date.substr(5, strlen("MM")));
        day = std::stoi(date.substr(8, strlen("DD")));
                //}
        } else { // date foramt [YYYYMMDD], used in Traceability log files
                //if(date.length()<7){  //DEV-BUG -> not take less than 8 digit.
        year = std::stoi(date.substr(0, strlen("YYYY"))); // string to integer conversion
        mon = std::stoi(date.substr(4, strlen("MM")));
        day = std::stoi(date.substr(6, strlen("DD")));
                //}
        }
	struct std::tm t;
	t.tm_sec = 0;				// second of minute (0 .. 59 and 60 for leap seconds)
	t.tm_min = 0;				// minute of hour (0 .. 59)
	t.tm_hour = 0;
	t.tm_mday = day;			// day of month (0 .. 31)
	t.tm_mon = mon-1;			// month of year (0 .. 11)
	t.tm_year = year-1900;		// year since 1900
	t.tm_isdst = -1;			// determine whether daylight saving time
	std::time_t tt = std::mktime(&t);
        char buffer[30];
        std::strftime(buffer, 30, "%Y-%m-%d %H:%M:%S.", std::localtime(&tt));
        std::cout << "convert time_t to string: " << buffer << '\n';
        if (tt == -1) {
                std::cout << "std::mktime returns -1 \n";
        }
	return std::chrono::high_resolution_clock::from_time_t(tt);
}

bool MyDate::purgeLogsBetweenDates(std::string keepErrorsNdays) {

	bool bRet = false;
	std::string fileDir = "Error/Log";

	if ("NONE" == keepErrorsNdays) {			// if (keepErrorsNdays == NONE), then no automatic purge must be done at all..[do nothing].
		bRet = true;
	} else if ( (fileDir.empty() == false) && (keepErrorsNdays.empty() ==false) && ("NONE" != keepErrorsNdays) ) {
		std::stringstream ss(keepErrorsNdays);
		int nKeepErrorsNdays;
		ss >> nKeepErrorsNdays;

		if(nKeepErrorsNdays > 0) {   	// check for positive days
			DIR *dirp;
			struct dirent *drtp;

			// get pivot Time point by subtracting durationDays(keepErrorsNdays * seconds per day) from current point in time.
			const int SECONDS_PER_HOUR = 3600;
			const int HOURS_PER_DAY = 24;
			std::chrono::high_resolution_clock::time_point tp1 = std::chrono::high_resolution_clock::now(); // current point in time
			std::chrono::duration<int,std::ratio<SECONDS_PER_HOUR * HOURS_PER_DAY>> durationDays(nKeepErrorsNdays);
			tp1 -= durationDays; 		//  subtract durationDays from current point in time

			dirp = opendir(fileDir.c_str());
			if (dirp != NULL) {
				drtp = readdir(dirp);
				while (drtp != NULL) {
					// skip self and parent
					if ((strcmp(drtp->d_name, ".") != 0) && (strcmp(drtp->d_name, "..") != 0)) {
						// purge all too old errors in files
						std::string filename = fileDir + "/" + drtp->d_name;
						bool isFileTooOld = false;
						int statResult = -1;
						// check file modified time , if it is too old empty the whole contents of file.
#if defined(unix) || defined(__unix__) || defined(__unix)
						struct stat fileAttributes;
						statResult = stat(filename.c_str(), &fileAttributes);
#else
						struct _stat fileAttributes;
						statResult = _stat(filename.c_str(), &fileAttributes);
#endif
						if (0 == statResult) {
							time_t fileModifiedTime = (time_t)(fileAttributes.st_mtime);
							auto tp2 = std::chrono::high_resolution_clock::from_time_t(fileModifiedTime);
							isFileTooOld = (tp1 >= tp2);
							if (isFileTooOld) {
								std::ofstream ofs(filename, std::ios_base::trunc); 	// empty the whole contents of file
								ofs.close();
							}
						}

						if (not isFileTooOld) {
							std::string tempFilename = filename + ".temp";
							std::string line;
							std::string logDate;
							bool isNotOldLogfound = false;
							std::ifstream ifs(filename, std::ios_base::in); 	// input file
							std::ofstream ofs(tempFilename, std::ios_base::trunc); 	// temporary file

							// read single log line from file, and tokenize that log line to get date, and make timepoint(tp2) from date token.
							// compare two time points, if tp1 is not before time point tp2, then do nothing, continue with next log line.
							// else write that log line to temporary file.
							// if the original file is containing too old logs only, empty the whole contents of original file, and delete the temporary file.
							// else delete the original log file, and rename the temporary file to original file name, because temporary file contains NOT too old logs.
							if (ifs.is_open() && ofs.is_open()) {
								while (std::getline(ifs, line, '\n')) { 	// read file line by line
									if (not line.empty()) {
										std::istringstream is(line);
										while (std::getline(is, logDate, '|') && (not isNotOldLogfound)) { 	// get date string token from error log line
											auto tp2 = makeTimePoint(logDate);
											bool isOutofDate = (tp1 >= tp2); // compare Time points
											if(not isOutofDate) {
												isNotOldLogfound = true; 	// error log date is not old, so no need of further comparison
											}
											break; // after processing date token
										}
										if (isNotOldLogfound) { // log is not too old
											ofs << line << '\n'; // write log to temporary file
										}
									}
								}
								ifs.close();
								ofs.close();
								if (isNotOldLogfound) {
									std::remove(filename.c_str()); // delete input log file
									std::rename(tempFilename.c_str(), filename.c_str()); // rename temporary file to input file.
								} else {
									std::ofstream ofs(filename, std::ios_base::trunc); 	// empty the whole contents of file
									ofs.close();
									std::remove(tempFilename.c_str()); // delete temporary log file
								}
							}
						}
					}
					drtp = readdir(dirp); // read file entry
				}
				closedir(dirp);
				bRet = true;
			}
		} else { // if nKeepErrorsNdays <= 0,then delete all error log content
			DIR *dirp;
			struct dirent *drtp;
			dirp = opendir(fileDir.c_str());
			if (dirp != NULL) {
				drtp = readdir(dirp);
				while (drtp != NULL) {
					if ((0 == strcmp(drtp->d_name, ".")) || (0 == strcmp(drtp->d_name, ".."))) {
						// skip self and parent
					} else {
						std::string filename = fileDir + "/" + drtp->d_name;
						std::ofstream ofs(filename, std::ios_base::trunc); 	// empty the whole contents of file
						ofs.close();
					}
					drtp = readdir(dirp); // read file entry
				}
				closedir(dirp);
				bRet = true;
			}
		}
	}

	return bRet;

}
*/
