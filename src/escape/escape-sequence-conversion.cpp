#include <iostream>
#include <string>

int GetHexDigitValue(const char& hexDigit)
{
    char asciiCode = hexDigit;		// TODO check UTF-8 (toLatin1())
    if ((asciiCode >= '0') && (asciiCode <= '9'))
    {
        return (int)asciiCode - (int)'0';
    }
    else
    {
        if ((asciiCode >= 'A') && (asciiCode <= 'F'))
        {
            return (int)asciiCode - (int)'A' + 10;
        }
        else
        {
            return -1;
        }
    }
}

bool convertHexCodeToCharacter(const char& highHexDigit, const char& lowHexDigit, char& hexCharacter)
{
    int highDigitValue = GetHexDigitValue(highHexDigit);
    int lowDigitValue = GetHexDigitValue(lowHexDigit);

    if ((highDigitValue >= 0) && (lowDigitValue >=0))
    {
        hexCharacter = char(highDigitValue * 16 + lowDigitValue);		// TODO check UTF-8
        return true;
    }

    return false;
}

std::string decodeHexCharacters(const std::string& hexCharacters)
{
	std::string result;

    int size = (int)hexCharacters.size();
    if (size % 2)
    {
    	std::cout << "Hex data could not be decoded because number string length is not even" << '\n';
        return std::string();
    }

    result.reserve(size / 2);

    char ch;
    for (int index = 0; index < size; index += 2)
    {
        if (!convertHexCodeToCharacter(hexCharacters[index], hexCharacters[index + 1], ch))
        {
        	std::cout << "Hex data could contains invalid character(s)" << '\n';
            return std::string();
        }

        result += ch;
    }
    return result;
}


std::string decodeEscapeSequence(const std::string& sequence)
{
	std::string result;
    if (!sequence.size())
    {
    	std::cout << "Empty escape sequence has been ignored." << '\n';
    }
    else
    {
    	// TODO check UTF-8
        switch (sequence[0])
        {
        case 'F':
        case 'f':
            result += '|';
            break;
        case 'S':
        case 's':
            result = '^';
            break;
        case 'T':
        case 't':
            result += '~';
            break;
        case 'R':
        case 'r':
            result += '\\';
            break;
        case 'E':
        case 'e':
            result += '&';
            break;
        case 'X':
        case 'x':
            result = decodeHexCharacters(sequence.substr(1, (sequence.size() - 1)));
            break;
        default:
        	std::cout << "Unsupported escape sequence '%1' has been ignored." << '\n';
            break;
        }
    }

    return result;
}


std::string decodeString(const std::string& encodedText) {
	std::string rawText;
	// TODO check for UTF-8
	rawText.reserve(encodedText.size());

	std::string escapeSequence;
	escapeSequence.reserve(4);

	std::string decodedEscapeSequence;
    char escapeCharacter = '/';

    char ch;
    bool isCollectingEscapeSequence = false;
    int length = encodedText.size();
    for (int i = 0; i < length; i++)
    {
        ch = encodedText[i];
        if (ch == escapeCharacter)
        {
            if (isCollectingEscapeSequence)
            {
                isCollectingEscapeSequence = false;

                decodedEscapeSequence = decodeEscapeSequence(escapeSequence);

                if(decodedEscapeSequence.size() > 0)
                {
                    rawText += decodedEscapeSequence;
                }
            }
            else
            {
                isCollectingEscapeSequence = true;
                escapeSequence.clear();
            }
        }
        else
        {
            if (isCollectingEscapeSequence)
            {
                escapeSequence += ch;
            }
            else
            {
                rawText += ch;
            }
        }
    }

    if (isCollectingEscapeSequence)
    {
        std::cout << "Incomplete escape sequence has been ignored." << '\n';

    }

    return rawText;
}

void escapreSequenceTonkenizer() {
	// valid input
	std::string ecodedText("Kuz/X1702/necov");
	std::string decodedText(decodeString(ecodedText));
	std::cout << "input: " << ecodedText << '\n' << "output: " << decodedText << "\n\n";
	// not even length
	ecodedText = "Kuz/X17102/necov";
	decodedText = decodeString(ecodedText);
	std::cout << "input: " << ecodedText << '\n' << "output: " << decodedText << "\n\n";
	// invalid hex digits
	ecodedText = "Kuz/X170G/necov";
	decodedText = decodeString(ecodedText);
	std::cout << "input: " << ecodedText << '\n' << "output: " << decodedText << "\n\n";
	// repetative escape sequences
	ecodedText = "K/Xuz/X170F/ne/X02/co//v";
	decodedText = decodeString(ecodedText);
	std::cout << "input: " << ecodedText << '\n' << "output: " << decodedText << "\n\n";
	// escape sequences of special characters
	ecodedText = "Kuz/F/necov";
	decodedText = decodeString(ecodedText);
	std::cout << "input: " << ecodedText << '\n' << "output: " << decodedText << "\n\n";
	// with UTF8 chars
	ecodedText = "Kuz/Fサ/nサecov";
	decodedText = decodeString(ecodedText);
	std::cout << "input: " << ecodedText << '\n' << "output: " << decodedText << "\n\n";
	//
	ecodedText = "Kuz/X010サ/nサecov";
	decodedText = decodeString(ecodedText);
	std::cout << "input: " << ecodedText << '\n' << "output: " << decodedText << "\n\n";

}

int main() {
	escapreSequenceTonkenizer();
	return 0;
}
