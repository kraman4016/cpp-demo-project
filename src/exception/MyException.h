#ifndef MYEXCEPTION_H
#define MYEXCEPTION_H

#include <string>
#include <iostream>

namespace Demo {
class MyException {
public:
//    MyException();
    MyException(const std::string& str);
    virtual ~MyException();

    void run();
private:
    std::string m_str;
};

///
/// @brief
///
class IOException : public std::exception {
    public:
    explicit IOException(const char * description) : m_errorMessage(description) {
        std::cout << __func__ << __LINE__ << std::endl;
    }

    virtual ~IOException() throw() {
        std::cout << __func__ << __LINE__ << std::endl;
    }

    virtual const char* what () const throw () {
        return m_errorMessage.c_str();
    }
    IOException(const IOException& other) : m_errorMessage(other.m_errorMessage) {}
    
    private:
    //IOException(const IOException& other) = delete;
    // Disable copy constructors
    //IOException& operator=(const IOException&) = delete;
    std::string m_errorMessage;
};
}
using Demo::MyException;
#endif /* MYEXCEPTION_H */


