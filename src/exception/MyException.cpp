#include "MyException.h"

#include <iostream>
#include <string>

/*
MyException::MyException() {
    std::cout << __func__ << __LINE__  << '\t' << std::endl;
}*/

MyException::MyException(const std::string& str) : m_str(str) {
    std::cout << __func__ << __LINE__  << '\t' << str << std::endl; 
}

MyException::~MyException() {
    std::cout << __func__ << __LINE__  << '\t' << m_str << std::endl; 
}

void MyException::run() {
	std::exception ex1();	
	const std::exception ex3();	
    MyException mex = MyException(__func__);
	//std::exception ex2(ex3);	// error: no matching function for call to ‘std::exception::exception(const std::exception (&)())’
	//throw std::exception();
	//throw new std::exception(msg.c_str());
	//throw new std::exception("hey i am thowing my first exception");

	std::invalid_argument ia("message-1");
	try {
		throw ia;
	} catch (std::exception& ex) {	// catch exception using reference, no loss of information
		std::cout << __func__ << __LINE__  << '\t'  << "catch clause: " << ex.what() << '\n';
	}
    try {
        throw IOException("message-2");
    } catch (std::exception &ex) {
        std::cout << __func__ << __LINE__  << '\t'  << "catch clause: " << ex.what() << std::endl;
        throw;
    }

}

