#include "exception/MyException.h"
#include <iostream>

int main() {
	std::cout << "Bonjour le monde ! std::exception " << "\n";

	MyException me("from main-1");

    try {
        MyException me2("from main-2");
	    me.run();
        MyException me3("from main-3");
    } catch (std::exception &ex) {
        std::cout << __func__ << __LINE__  << '\t' << "catch clause: " << ex.what() << std::endl;
        //throw;        // stops stack unwinding
    }
}
