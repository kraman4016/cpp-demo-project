#ifndef MYREFERENCE_H
#define MYREFERENCE_H

#include <string>
#include <memory>
//forward declaration
class MyClass; 

namespace Demo {
static int nb = 2;
class MyReference {
	public:
        MyReference();
		virtual ~MyReference();

		void run();
        void example1(int &b = nb, std::string st = "");
	private:
};
}
using Demo::MyReference;
#endif /* MYREFERENCE_H */


