#include "MyReference.h"

#include <iostream>
#include <string>

MyReference::MyReference() {
    run();
}

MyReference::~MyReference() {
}

void MyReference::run() {
        int nb = 3;
        example1(nb);
}

void MyReference::example1(int &nb, std::string st) {
        std::cout << __func__ << '\n';

}


