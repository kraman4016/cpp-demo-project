#include "pointers/uniquepointer/MyUniquePointer.h"
#include <iostream>
#include <memory>

int main() {
	std::cout << "Bonjour le monde ! unique pointer" << "\n";

	//MyUniquePointer mc;
        //mc.run();
        {
                auto up = std::make_unique<MyUniquePointer>(1);
                up = std::make_unique<MyUniquePointer>(2);
                //std::unique_ptr<MyUniquePointer> up2 = std::make_unique<MyUniquePointer>();
        }
}
