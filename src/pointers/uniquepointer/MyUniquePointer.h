#ifndef MYUNIQUEPOINTER_H
#define MYUNIQUEPOINTER_H

#include <string>
#include <memory>

namespace Demo {
class MyUniquePointer {
	public:
                MyUniquePointer();
		virtual ~MyUniquePointer();

		void run();
	private:
                void test1();
};
}
using Demo::MyUniquePointer;
#endif /* MYUNIQUEPOINTER_H */


