#include "MySharedPointer.h"

#include <iostream>
#include <string>

MySharedPointer::MySharedPointer() {
}

MySharedPointer::~MySharedPointer() {
}

void MySharedPointer::run() {
        example1(1);
        example2(std::make_shared<int>());
}

void MySharedPointer::example1(int nb, std::string st) {
        std::cout << __func__ << '\n';
        int *pnb = nullptr;
        //example2(nb);
}

void MySharedPointer::example2(std::shared_ptr<int> sh) {
        std::cout << __func__ << '\n';
        std::shared_ptr<MySharedPointer> sh2 = std::make_shared<MySharedPointer>();
        MySharedPointer* shp = new MySharedPointer();
        std::shared_ptr<MySharedPointer> sh3(shp);
        std::cout << sh3.get() << '\t' << sh3.use_count();
        std::shared_ptr<MySharedPointer> sh4(shp);
}

