#ifndef MYSHAREDPOINTER_H
#define MYSHAREDPOINTER_H

#include <string>
#include <memory>
//forward declaration
class MyClass; 

namespace Demo {
class MySharedPointer {
	public:
                MySharedPointer();
		virtual ~MySharedPointer();

		void run();
                void example1(int b = 0, std::string st = "");
                //void example1(int b /*= 0*/); // compilaton issue 
                void example2(std::shared_ptr<int> sh);
	private:
                //MyClass& mc;
};
}
using Demo::MySharedPointer;
#endif /* MYSHAREDPOINTER_H */


