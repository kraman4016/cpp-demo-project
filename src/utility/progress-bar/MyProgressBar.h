#ifndef MYPROGRESSBAR_H
#define MYPROGRESSBAR_H
namespace Demo {
class MyProgressBar {
	public:
		MyProgressBar();
 		virtual ~MyProgressBar();

		void run();
        void example_1();
        void example_2();
	private:
};
}
using Demo::MyProgressBar;
#endif /* MYPROGRESSBAR_H */


