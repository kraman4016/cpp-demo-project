#include "MyProgressBar.h"

#include <iostream>
#include <thread>


MyProgressBar::MyProgressBar() {
	run();
}

MyProgressBar::~MyProgressBar() {
}

void MyProgressBar::run() {
    example_2();
    //example_1();
}

void MyProgressBar::example_2() {
    for (uint16_t nb = 0; nb <= 100; nb += 10) {
        for (uint16_t i = 0; i <= 100; i += 10) {
            std::cout << i;
            std::this_thread::sleep_for(std::chrono::seconds(1));    
            std::cout << '\r';
            std::cout.flush();

        }
//        std::cout << '\r';
//        std::cout.flush();
    }
    std::cout << '\n';
}

void MyProgressBar::example_1() {
    float progress = 0.0;
    while (progress < 1.0) {
        int barWidth = 70;

        std::cout << "[";
        int pos = barWidth * progress;
        for (int i = 0; i < barWidth; ++i) {
            if (i < pos) std::cout << "=";
            else if (i == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "] " << int(progress * 100.0) << " %\r";
        std::cout.flush();

        progress += 0.2; // for demonstration only
        std::this_thread::sleep_for(std::chrono::seconds(1));    
    }
    std::cout << std::endl;
}


