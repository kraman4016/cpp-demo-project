#include "MyTcp.h"

#include <iostream>

// ipv6AddressValidations
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>


MyTcp::MyTcp() {
	run();
}

MyTcp::~MyTcp() {
}

void MyTcp::run() {
    ipv6AddressValidation_1();
    ipv6AddressValidation_2();
}

void MyTcp::ipv6AddressValidation_1() {
    const char *argv[] = {"192.168.42.128", "fe80::96cf:17e3:a7f3:89", "fe80::9c54:8b0d:c6f7:b3d9%ens33", "fe80::9c54::8b0d:c6f7:b3d9%ens33"};

    for (auto &add : argv) {
        struct addrinfo hint, *res = NULL;
        int ret;

        memset(&hint, '\0', sizeof hint);

        hint.ai_family = PF_UNSPEC;
        hint.ai_flags = AI_NUMERICHOST;


//        ret = getaddrinfo(add, NULL, &hint, &res);
        ret = getaddrinfo(add, "4145", &hint, &res);
        if (ret) {
            puts("Invalid address");
            puts(gai_strerror(ret));
            return;
        }
        if(res->ai_family == AF_INET) {
            printf("%s is an ipv4 address\n", add);
        } else if (res->ai_family == AF_INET6) {
            printf("%s is an ipv6 address\n", add);
        } else {
            printf("%s is an is unknown address format %d\n", add, res->ai_family);
        }
       freeaddrinfo(res);
    }
}

void MyTcp::ipv6AddressValidation_2() {
    const char *argv[] = {"192.168.42.128", "fe80::96cf:17e3:a7f3:89", "fe80::9c54:8b0d:c6f7:b3d9%ens33", "fe80::9c54::8b0d:c6f7:b3d9%ens33"};

    for (auto &add : argv) {
        struct addrinfo hint, *res = NULL;
        int ret;

        memset(&hint, '\0', sizeof hint);

        hint.ai_family = AF_UNSPEC;
        hint.ai_flags = AI_NUMERICHOST;

        ret = getaddrinfo(add, NULL, &hint, &res);
        if (ret) {
            puts("Invalid address");
            puts(gai_strerror(ret));
        }
        if(res->ai_family == AF_INET) {
            printf("%s is an ipv4 address\n", add);
        } else if (res->ai_family == AF_INET6) {
            printf("%s is an ipv6 address\n", add);
        } else {
            printf("%s is an is unknown address format %d\n", add, res->ai_family);
        }
       freeaddrinfo(res);
    }
}


