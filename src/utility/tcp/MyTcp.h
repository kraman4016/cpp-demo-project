#ifndef MYTCP_H
#define MYTCP_H
namespace Demo {
class MyTcp {
	public:
		MyTcp();
 		virtual ~MyTcp();

		void run();
        void ipv6AddressValidation_1();
        void ipv6AddressValidation_2();
	private:
};
}
using Demo::MyTcp;
#endif /* MYTCP_H */


