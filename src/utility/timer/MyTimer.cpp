#include "MyTimer.h"

#include <iostream>
#include <thread>

#include <chrono> // for std::chrono functions
 
class Timer
{
private:
	// Type aliases to make accessing nested type easier
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;
	
	std::chrono::time_point<clock_t> m_beg;
 
public:
	Timer() : m_beg(clock_t::now())
	{
	}
	
	void reset()
	{
		m_beg = clock_t::now();
	}
	
	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

MyTimer::MyTimer() {
	run();
}

MyTimer::~MyTimer() {
}

void MyTimer::run() {
    //example_2();
    example_1();
}

void MyTimer::example_2() {
}

void MyTimer::example_1() {
    Timer t;
 
    std::this_thread::sleep_for(std::chrono::seconds(1));
    // Code to time goes here
 
    std::cout << "Time elapsed: " << t.elapsed() << " seconds\n";
}


