#ifndef MYTIMER_H
#define MYTIMER_H
namespace Demo {
class MyTimer {
	public:
		MyTimer();
 		virtual ~MyTimer();

		void run();
        void example_1();
        void example_2();
	private:
};
}
using Demo::MyTimer;
#endif /* MYTIMER_H */


