// thirk_party includes
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
// cpp standard includes
#include <iostream>
#include <fstream>
#include <vector>

class FieldSettingsGenerator {
public:
	FieldSettingsGenerator() : m_field(rapidjson::kArrayType) {
		//rapidjson::Document d;
		m_d.SetObject();		
	}
	
	~FieldSettingsGenerator() {
		rapidjson::Document::AllocatorType& allocator = m_d.GetAllocator();
		rapidjson::Value d3(rapidjson::kObjectType);
		d3.AddMember("Field", m_field, allocator);
		rapidjson::Value fields(rapidjson::kObjectType);
		m_d.AddMember("Fields", d3, allocator);
	
		// Convert JSON document to std::string
		rapidjson::StringBuffer strbuf;
		rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(strbuf);
		m_d.Accept(writer);
		
		std::cout << strbuf.GetString() << '\n';
		std::ofstream ofs("astm-field-settings.json");
		ofs << strbuf.GetString();
		ofs.close();
	}
	
	void addFieldEntry(const std::string& fieldName, const std::vector<std::string>& toHost, const std::vector<std::string>& fromHost) {
		rapidjson::Document::AllocatorType& allocator = m_d.GetAllocator();

		size_t sz = allocator.Size();

		rapidjson::Value d2(rapidjson::kObjectType);


		{
			rapidjson::Value val(rapidjson::kObjectType);

			std::string name = fieldName;
			val.SetString(name.c_str(), static_cast<rapidjson::SizeType>(name.length()), allocator);
			
			d2.AddMember("name", val, allocator);
		}
		{
			rapidjson::Value obj(rapidjson::kObjectType);
			rapidjson::Value val(rapidjson::kObjectType);

			std::string is_used = toHost[0];
			val.SetString(is_used.c_str(), static_cast<rapidjson::SizeType>(is_used.length()), allocator);
			obj.AddMember("is_used", val, allocator);

			std::string max_length = toHost[1];
			val.SetString(max_length.c_str(), static_cast<rapidjson::SizeType>(max_length.length()), allocator);
			obj.AddMember("max_length", val, allocator);

			std::string type = toHost[2];
			val.SetString(type.c_str(), static_cast<rapidjson::SizeType>(type.length()), allocator);
			obj.AddMember("type", val, allocator);

			std::string repeat_delimiter = toHost[3];
			val.SetString(repeat_delimiter.c_str(), static_cast<rapidjson::SizeType>(repeat_delimiter.length()), allocator);
			obj.AddMember("repeat_delimiter", val, allocator);

			std::string mandatory = toHost[4];
			val.SetString(mandatory.c_str(), static_cast<rapidjson::SizeType>(mandatory.length()), allocator);	
			obj.AddMember("mandatory", val, allocator);

			std::string components = toHost[5];
			val.SetString(components.c_str(), static_cast<rapidjson::SizeType>(components.length()), allocator);	
			obj.AddMember("components", val, allocator);
			
			d2.AddMember("TO_HOST", obj, allocator);	
		}

		{
			rapidjson::Value obj(rapidjson::kObjectType);
			rapidjson::Value val(rapidjson::kObjectType);

			std::string is_used = fromHost[0];
			val.SetString(is_used.c_str(), static_cast<rapidjson::SizeType>(is_used.length()), allocator);
			obj.AddMember("is_used", val, allocator);

			std::string max_length = fromHost[1];
			val.SetString(max_length.c_str(), static_cast<rapidjson::SizeType>(max_length.length()), allocator);
			obj.AddMember("max_length", val, allocator);

			std::string type = fromHost[2];
			val.SetString(type.c_str(), static_cast<rapidjson::SizeType>(type.length()), allocator);
			obj.AddMember("type", val, allocator);

			std::string repeat_delimiter = fromHost[3];
			val.SetString(repeat_delimiter.c_str(), static_cast<rapidjson::SizeType>(repeat_delimiter.length()), allocator);
			obj.AddMember("repeat_delimiter", val, allocator);

			std::string mandatory = fromHost[4];
			val.SetString(mandatory.c_str(), static_cast<rapidjson::SizeType>(mandatory.length()), allocator);	
			obj.AddMember("mandatory", val, allocator);

			std::string components = fromHost[5];
			val.SetString(components.c_str(), static_cast<rapidjson::SizeType>(components.length()), allocator);	
			obj.AddMember("components", val, allocator);
			
			d2.AddMember("FROM_HOST", obj, allocator);	
		}
		
		{		
			m_field.PushBack(d2, allocator);
		}
	}
	
private:
	rapidjson::Document m_d;
	rapidjson::Value m_field;
};


int main() {
	// "Fieldname", TO_HSOT{"is_used", "max_length", "type", "repeat_delimiter", "mandatory", "components"}
	FieldSettingsGenerator fieldSettingsGenerator;
	fieldSettingsGenerator.addFieldEntry("FN_6_1_RECORD_TYPE_ID", {"Y", "1", "FIXED", "N", "Y", "1"}, {"Y", "1", "FIXED", "N", "Y", "1"});
	fieldSettingsGenerator.addFieldEntry("FN_6_1_RECORD_TYPE_ID2", {"Y", "1", "FIXED", "N", "Y", "1"}, {"Y", "1", "FIXED", "N", "Y", "1"});
	fieldSettingsGenerator.addFieldEntry("FN_6_1_RECORD_TYPE_ID3", {"Y", "1", "FIXED", "N", "Y", "1"}, {"Y", "1", "FIXED", "N", "Y", "1"});	
	return 0;
}
