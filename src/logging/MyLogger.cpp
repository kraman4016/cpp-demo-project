#include "MyLogger.h"
// third party headers
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/daily_file_sink.h"
#include "spdlog/fmt/bin_to_hex.h"
#include "spdlog/async.h"
#include "spdlog/fmt/ostr.h" 
#include "spdlog/sinks/syslog_sink.h"

#include <iostream>


MyLogger::MyLogger() {
	run();
}

MyLogger::~MyLogger() {
}

void lisssmLogger() {
    spdlog::init_thread_pool(8192, 1);
	auto rotating_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt> ("logs/lisssm.log", (1024 * 1024 * 25), 5);
	std::string path = "logs_2";
	system(("mkdir -p " + path).c_str());
	auto rotating_sink2 = std::make_shared<spdlog::sinks::rotating_file_sink_mt> ("logs_2/lisssm.log", (1024 * 1024 * 25), 5);

	//std::shared_ptr<spdlog::logger> logger = std::make_shared<spdlog::async_logger> ("lisssmLogger", rotating_sink, (1024 * 4));
	std::shared_ptr<spdlog::logger> logger = std::make_shared<spdlog::async_logger> ("lisssmLogger", rotating_sink, spdlog::thread_pool(), spdlog::async_overflow_policy::block);
    spdlog::register_logger(logger);
	logger->info(path);
}


// syslog
void syslog_example()
{
    std::string ident = "spdlog-example";
    auto syslog_logger = spdlog::syslog_logger_mt("syslog", ident, LOG_PID);
    syslog_logger->warn("This is warning that will end up in syslog.");
}

// Custom error handler
void err_handler_example()
{
    // can be set globally or per logger(logger->set_error_handler(..))
    spdlog::set_error_handler([](const std::string &msg) { spdlog::get("console")->error("*** LOGGER ERROR ***: {}", msg); });
    spdlog::get("console")->info("some invalid message to trigger an error {}{}{}{}", 3);
}


// User defined types
struct my_type
{
    int i;
    template<typename OStream>
    friend OStream &operator<<(OStream &os, const my_type &c)
    {
        return os << "[my_type i=" << c.i << "]";
    }
};

void user_defined_example()
{
    spdlog::get("console")->info("user defined type: {}", my_type{14});
}

// Asynchronous logger with multi sinks
void multi_sink_example2()
{
    spdlog::init_thread_pool(8192, 1);
    auto stdout_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt >();
    auto rotating_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>("logs/mylog.txt", 1024*1024*10, 3);
    std::vector<spdlog::sink_ptr> sinks {stdout_sink, rotating_sink};
    auto logger = std::make_shared<spdlog::async_logger>("loggername", sinks.begin(), sinks.end(), spdlog::thread_pool(), spdlog::async_overflow_policy::block);
    spdlog::register_logger(logger);
}

// Asynchronous logging
void async_example()
{
    // default thread pool settings can be modified *before* creating the async logger:
    // spdlog::init_thread_pool(8192, 1); // queue with 8k items and 1 backing thread.
    auto async_file = spdlog::basic_logger_mt<spdlog::async_factory>("async_file_logger", "logs/async_log.txt");
	async_file->info("async logger info ");
    // alternatively:
    //auto async_file2 = spdlog::create_async<spdlog::sinks::basic_file_sink_mt>("async_file_logger", "logs/async_log.txt");   
}


// Logger with multi sinks - each with different format and log level
// create logger with 2 targets with different log levels and formats.
// the console will show only warnings or errors, while the file will log all.
void multi_sink_example()
{
    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    console_sink->set_level(spdlog::level::warn);
    console_sink->set_pattern("[multi_sink_example] [%^%l%$] %v");

    auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/multisink.txt", true);
    file_sink->set_level(spdlog::level::trace);

    spdlog::logger logger("multi_sink", {console_sink, file_sink});
    logger.set_level(spdlog::level::debug);
    logger.warn("this should appear in both console and file");
    logger.info("this message should not appear in the console, only in the file");
}

// Binary logging
// log binary data as hex.
// many types of std::container<char> types can be used.
// ranges are supported too.
// format flags:
// {:X} - print in uppercase.
// {:s} - don't separate each byte with space.
// {:p} - don't print the position on each line start.
// {:n} - don't split the output to lines.
void binary_example()
{
    auto console = spdlog::get("console");
    std::array<char, 8> buf = {"rain!"};
    std::array<char, 8> buf2 = {}; // all elements are intialized to zero.
    std::array<char, 8> buf3;
    console->info("Binary example: {}, {:2},{:3}", spdlog::to_hex(buf), spdlog::to_hex(buf2), spdlog::to_hex(buf3));
    console->info("Another binary example:{:n}", spdlog::to_hex(std::begin(buf), std::begin(buf) + 4));
    // more examples:
    console->info("uppercase: {:X}", spdlog::to_hex(buf));
    console->info("uppercase, no delimiters: {:Xs}", spdlog::to_hex(buf));
    console->info("uppercase, no delimiters, no position info: {:Xsp}", spdlog::to_hex(buf));
}

// Cloning loggers
// clone a logger and give it new name.
// Useful for creating subsystem loggers from some "root" logger
void clone_example()
{
    //auto network_logger = spdlog::get("root")->clone("network");
    auto network_logger = spdlog::get("daily_logger")->clone("network");
    network_logger->info("Logging network stuff..");
	
	// Periodic flush
	// periodically flush all *registered* loggers every 3 seconds:
	// warning: only use if all your loggers are thread safe!
	//spdlog::flush_every(std::chrono::seconds(3));
}

// Daily files
void daily_example()
{
	spdlog::set_pattern("*** [%H:%M:%S %z] [thread %t] %v ***");
    // Create a daily logger - a new file is created every day on 2:30am
    auto daily_logger = spdlog::daily_logger_mt("daily_logger", "logs/daily.txt", 2, 30);

	// Create a multiple loggers all sharing the same file (sink) aka categories
	try
    {
        auto daily_sink = std::make_shared<spdlog::sinks::daily_file_sink_mt>("logfile", 23, 59);
        // create synchronous  loggers
        auto net_logger = std::make_shared<spdlog::logger>("net", daily_sink);
        auto hw_logger  = std::make_shared<spdlog::logger>("hw",  daily_sink);
        auto db_logger  = std::make_shared<spdlog::logger>("db",  daily_sink);

        net_logger->set_level(spdlog::level::info); // independent levels
        hw_logger->set_level(spdlog::level::debug);

		net_logger->info("net logger info");
		net_logger->critical("net logger info");

        // globally register the loggers so so the can be accessed using spdlog::get(logger_name)
        spdlog::register_logger(net_logger);
		spdlog::set_pattern("*** [%H:%M:%S %z] [thread %t] %v %@  ***");
		SPDLOG_TRACE("trace");
		net_logger->critical("net logger info");
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
    }
}


// Rotating files
void rotating_example()
{
    // Create a file rotating logger with 5mb size max and 3 rotated files
    //auto rotating_logger = spdlog::rotating_logger_mt("some_logger_name", "logs/rotating.txt", 1048576 * 5, 3);
    auto rotating_logger = spdlog::rotating_logger_mt("some_logger_name", "logs/rotating.txt", 1048, 3);
	for (int i = 0; i < 50; i++ ) {
		rotating_logger->info(i);
	}

}

// Basic file logger
void basic_logfile_example()
{
    try 
    {
        spdlog::set_pattern("[%Y-%m-%d %I:%M:%S:%e] [%l] [thread %t] %v");
        auto my_logger = spdlog::basic_logger_mt("basic_logger", "logs/basic-log.txt");
        my_logger->info("hey buddy!, go to sleep for a while");
        std::this_thread::sleep_for(std::chrono::seconds(1));
        my_logger->info("wakeup wakeup!");
        std::string func = __func__;
//        message += '\t';
        uint32_t lineNb = __LINE__;
        
        std::string message = func + "\t" +std::to_string(lineNb) + "\t " + "start";
        my_logger->info(message);
        
    }
    catch (const spdlog::spdlog_ex &ex)
    {
        std::cout << "Log init failed: " << ex.what() << std::endl;
    }
}

// create stdout/stderr logger object
void stdout_example()
{
    // create color multi threaded logger
    auto console = spdlog::stdout_color_mt("console");    
    auto err_logger = spdlog::stderr_color_mt("stderr");    
    spdlog::get("console")->info("loggers can be retrieved from a global registry using the spdlog::get(logger_name)");
}



void MyLogger::run() {
    //basicUsage();
	//basic_logfile_example();
	//daily_example();
	//lisssmLogger();
/*
	basicUsage();
	stdout_example();
	basic_logfile_example();
	rotating_example();
	daily_example();
	clone_example();
	binary_example();
	multi_sink_example();
	async_example();
	multi_sink_example2();
	user_defined_example();
    err_handler_example();
    syslog_example();
*/
}

// Basic usage
void MyLogger::basicUsage() {
    //Use the default logger (stdout, multi-threaded, colored)
    spdlog::info("Hello, {}!", "World");

	spdlog::info("Welcome to spdlog!");
    spdlog::error("Some error message with arg: {}", 1);

    spdlog::warn("Easy padding in numbers like {:08d}", 12);
    spdlog::critical("Support for int: {0:d};  hex: {0:x};  oct: {0:o}; bin: {0:b}", 42);
    spdlog::info("Support for floats {:03.2f}", 1.23456);
    spdlog::info("Positional args are {1} {0}..", "too", "supported");
    spdlog::info("{:<30}", "left aligned");

    spdlog::set_level(spdlog::level::debug); // Set global log level to debug
    spdlog::debug("This message should be displayed..");

    // change log pattern
    //spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] [thread %t] %v");

    // Compile time log levels
    // define SPDLOG_ACTIVE_LEVEL to desired level
    SPDLOG_TRACE("Some trace message with param {}", {});
    SPDLOG_DEBUG("Some debug message");

    // Set the default logger to file logger
    auto file_logger = spdlog::basic_logger_mt("basic_logger", "logs/basic.txt");
    spdlog::set_default_logger(file_logger);
	spdlog::info("Welcome to spdlog!");
}



