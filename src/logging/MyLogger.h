#ifndef MYLOGGER_H
#define MYLOGGER_H
namespace Demo {
class MyLogger{
	public:
		MyLogger();
		virtual ~MyLogger();
		void run();
		void basicUsage();
	private:
};
}
using Demo::MyLogger;
#endif /* MYLOGGER_H */


