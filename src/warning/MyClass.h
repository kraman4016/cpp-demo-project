#ifndef MYCLASS_H
#define MYCLASS_H

#include <string>
namespace Demo {
class MyClass {
	public:
        MyClass();
		virtual ~MyClass();

		void run();
	private:
		char ch;
		int nb;
		std::string str;
};
}
using Demo::MyClass;
#endif /* MYCLASS_H */


