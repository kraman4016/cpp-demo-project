#include "MyClass.h"

#include <iostream>

MyClass::MyClass() : nb(1), ch('c') {
	run();
}

MyClass::~MyClass() {
}

void MyClass::run() {
	uint16_t nb = 3;
   	int16_t nb2 = -1;
	std::cout << "signed & unsigned comparison" << (nb2 == nb) << '\n';   
	if (nb2 == nb) {
			std::cout << "if block \n";
	} else {
			std::cout << "else block \n";
	}
	nb2 = nb;
	bool bRes = nb > nb2;
}

