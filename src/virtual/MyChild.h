#ifndef MYCHILD_H
#define MYCHILD_H

#include "MyParent.h"

namespace Demo {
class MyChild : public MyParent{
	public:
		MyChild();
		virtual ~MyChild();
		void run();
	private:
};
}
using Demo::MyChild;
#endif /* MYCHILD_H */


