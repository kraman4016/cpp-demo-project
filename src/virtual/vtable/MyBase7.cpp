#ifndef MYBASE7_H
#define MYBASE7_H
namespace Demo {
class MyBase7 {
	public:
		MyBase7();
		virtual ~MyBase7();
		virtual void pvf() = 0;
		virtual void pvf2() = 0;
	private:
};
}
using Demo::MyBase7;
#endif /* MYBASE7_H */


