#ifndef MYBASE2_H
#define MYBASE2_H

namespace Demo {
// 
class MyBase2 {
	public:
		MyBase2() = default;
		~MyBase2() = default;
	private:
};
}
using Demo::MyBase2;
#endif /* MYBASE2_H */


