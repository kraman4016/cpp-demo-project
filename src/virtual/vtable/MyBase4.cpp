#ifndef MYBASE4_H
#define MYBASE4_H
namespace Demo {
class MyBase4 {
	public:
		MyBase4() = default;
		virtual ~MyBase4() = default;
	private:
};
}
using Demo::MyBase4;
#endif /* MYBASE4_H */


