#include "virtual/vtable/MyBase.cpp"
#include "virtual/vtable/MyBase2.cpp"
#include "virtual/vtable/MyBase3.cpp"
#include "virtual/vtable/MyBase4.cpp"
#include "virtual/vtable/MyBase5.cpp"
#include "virtual/vtable/MyBase6.cpp"
#include "virtual/vtable/MyDerived6.cpp"

#include <iostream>

int main() {
	std::cout << "Bonjour le monde" << "\n";
	std::cout << "vtable" << "\n";

	MyBase mbc;
	MyBase2 mbc2;
	//MyBase3 mbc3;
	MyBase4 mbc4;
	MyBase5 mbc5;
	//mbc5.vf(); linkder error : definition not found, but OK : if it is not called
	//MyBase6 mbc6;
	MyDerived6 md6;
}
