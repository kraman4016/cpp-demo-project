#ifndef MYBASE6_H
#define MYBASE6_H
namespace Demo {
class MyBase6 {
	public:
		MyBase6() {};
		virtual ~MyBase6() {};
		//virtual void vf(); // Error: even if it is not called, undefined reference to vtable & vf(if it is not mentioned in calss derived)
		virtual void vf2() {};
	private:
};
}
using Demo::MyBase6;
#endif /* MYBASE6_H */


