#ifndef MYBASE5_H
#define MYBASE5_H
namespace Demo {
class MyBase5 {
	public:
		MyBase5() {};
		virtual ~MyBase5() {};
		void vf(); // OK : as long as no one is calling
	private:
};
}
using Demo::MyBase5;
#endif /* MYBASE5_H */


