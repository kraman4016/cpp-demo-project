#ifndef MYBASE3_H
#define MYBASE3_H
namespace Demo {
class MyBase3 {
	public:
	private:
		MyBase3() = default;
		~MyBase3() = default;
};
}
using Demo::MyBase3;
#endif /* MYBASE3_H */


