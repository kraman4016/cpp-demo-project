#include "MyVirtual.h"
#include "MyParent.h"
#include "MyChild.h"

#include <iostream>


MyVirtual::MyVirtual() {
	run();
}

MyVirtual::~MyVirtual() {
}

void MyVirtual::run() {
	MyParent mp;
	MyChild mc;
}

