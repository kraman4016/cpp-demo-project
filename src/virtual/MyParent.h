#ifndef MYPARENT_H
#define MYPARENT_H
namespace Demo {
class MyParent {
	public:
		MyParent();
		virtual ~MyParent();
		void run();
		virtual void virtualFun1();
		virtual void virtualFun2();
		//virtual void virtualFun1() = 0;
		//virtual void virtualFun2() = 0;
	private:
	private:
};
}
using Demo::MyParent;
#endif /* MYPARENT_H */


