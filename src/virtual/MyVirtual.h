#ifndef MYVIRTUAL_H
#define MYVIRTUAL_H
namespace Demo {
class MyVirtual {
	public:
		MyVirtual();
		virtual ~MyVirtual();
		void run();
	private:
};
}
using Demo::MyVirtual;
#endif /* MYVIRTUAL_H */


