#ifndef MYHELLO_H
#define MYHELLO_H

#include <string>

namespace Demo {
class MyHello {
	public:
                MyHello();
		MyHello(const std::string& arg);
		virtual ~MyHello();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyHello;
#endif /* MYHELLO_H */


