#include "MyHello.h"

#include <iostream>
#include <string>

MyHello::MyHello() {
}

MyHello::MyHello(const std::string& arg) : m_str(arg, 100) {
}

MyHello::~MyHello() {
}

void MyHello::run() {
        std::cout << "static: MyHello::run " << __func__ << '\t' << __LINE__ << '\n';

}

