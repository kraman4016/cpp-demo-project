#ifndef MYDEMO2_H
#define MYDEMO2_H

#include <string>

namespace Demo {
class MyDemo2 {
	public:
                MyDemo2();
		MyDemo2(const std::string& arg);
		virtual ~MyDemo2();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyDemo2;
#endif /* MYDEMO2_H */


