#include "MyHello.h" // hen this statement included : target_include_directories (Hello PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
//#include "../sharedlibrary/MyHello.h"         // when this statement not included : target_include_directories (Hello PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
#include "MyHello2.h"

#include <iostream>

int main() {
	std::cout << "Bonjour le monde ! cmake" << "\n";

	MyHello mh;
        mh.run();
	MyHello2 mh2;
        mh2.run();
	//MyHello3 mh3;
        //mh3.run();

}
