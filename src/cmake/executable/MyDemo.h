#ifndef MYDEMO_H
#define MYDEMO_H

#include <string>

namespace Demo {
class MyDemo {
	public:
                MyDemo();
		MyDemo(const std::string& arg);
		virtual ~MyDemo();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyDemo;
#endif /* MYDEMO_H */


