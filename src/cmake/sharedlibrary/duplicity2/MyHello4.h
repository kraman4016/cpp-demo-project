#ifndef MYHELLO4_H
#define MYHELLO4_H

#include <string>

namespace Demo {
class MyHello4 {
	public:
                MyHello4();
		MyHello4(const std::string& arg);
		virtual ~MyHello4();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyHello4;
#endif /* MYHELLO4_H */


