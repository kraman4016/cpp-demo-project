#include "MyHello4.h"

#include <iostream>
#include <string>

MyHello4::MyHello4() {
}

MyHello4::MyHello4(const std::string& arg) : m_str(arg, 100) {
}

MyHello4::~MyHello4() {
}

void MyHello4::run() {
        std::cout << "shared: MyHello4::run " << __func__ << '\t' << __LINE__ << '\n';

}

