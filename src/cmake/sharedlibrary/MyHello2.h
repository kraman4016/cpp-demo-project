#ifndef MYHELLO2_H
#define MYHELLO2_H

#include <string>

namespace Demo {
class MyHello2 {
	public:
                MyHello2();
		MyHello2(const std::string& arg);
		virtual ~MyHello2();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyHello2;
#endif /* MYHELLO2_H */


