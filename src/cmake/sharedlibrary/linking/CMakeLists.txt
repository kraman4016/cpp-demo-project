# Create a library called "Hello" which includes the source file "hello.cxx".
# The extension is already found. Any number of sources could be listed here.
add_library (Hello5Shared SHARED MyHello3.cpp)

# Make sure the compiler can find include files for our Hello library
# when other libraries or executables link to Hello
target_include_directories (Hello5Shared PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
# works
#target_include_directories (Hello3Shared PUBLIC ${CMAKE_CURRENT_LIST_DIR})

