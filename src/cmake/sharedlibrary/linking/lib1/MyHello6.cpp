#include "MyHello6.h"

#include <iostream>
#include <string>

MyHello6::MyHello6() {
}

MyHello6::MyHello6(const std::string& arg) : m_str(arg, 100) {
}

MyHello6::~MyHello6() {
}

void MyHello6::run() {
        std::cout << "shared: MyHello6::run " << __func__ << '\t' << __LINE__ << '\n';

}

