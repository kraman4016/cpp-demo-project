#ifndef MYHELLO6_H
#define MYHELLO6_H

#include <string>

namespace Demo {
class MyHello6 {
	public:
                MyHello6();
		MyHello6(const std::string& arg);
		virtual ~MyHello6();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyHello6;
#endif /* MYHELLO6_H */


