#include "MyHello5.h"

#include <iostream>
#include <string>

MyHello5::MyHello5() {
}

MyHello5::MyHello5(const std::string& arg) : m_str(arg, 100) {
}

MyHello5::~MyHello5() {
}

void MyHello5::run() {
        std::cout << "shared: MyHello5::run " << __func__ << '\t' << __LINE__ << '\n';

}

