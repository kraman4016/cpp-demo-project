#ifndef MYHELLO5_H
#define MYHELLO5_H

#include <string>

namespace Demo {
class MyHello5 {
	public:
                MyHello5();
		MyHello5(const std::string& arg);
		virtual ~MyHello5();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyHello5;
#endif /* MYHELLO5_H */


