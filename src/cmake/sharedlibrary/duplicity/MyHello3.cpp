#include "MyHello3.h"

#include <iostream>
#include <string>

MyHello3::MyHello3() {
}

MyHello3::MyHello3(const std::string& arg) : m_str(arg, 100) {
}

MyHello3::~MyHello3() {
}

void MyHello3::run() {
        std::cout << "shared: MyHello3::run " << __func__ << '\t' << __LINE__ << '\n';

}

