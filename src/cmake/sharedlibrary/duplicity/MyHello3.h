#ifndef MYHELLO3_H
#define MYHELLO3_H

#include <string>

namespace Demo {
class MyHello3 {
	public:
                MyHello3();
		MyHello3(const std::string& arg);
		virtual ~MyHello3();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyHello3;
#endif /* MYHELLO3_H */


