#include "MyHello2.h"
#include "duplicity/MyHello3.h"
//#include "MyHello3.h"

#include <iostream>
#include <string>

MyHello2::MyHello2() {
}

MyHello2::MyHello2(const std::string& arg) : m_str(arg, 100) {
}

MyHello2::~MyHello2() {
}

void MyHello2::run() {
        std::cout << "shared: MyHello2::run " << __func__ << '\t' << __LINE__ << '\n';
	MyHello3 mh3;
        mh3.run();

}

