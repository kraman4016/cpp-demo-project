# Create a library called "Hello" which includes the source file "hello.cxx".
# The extension is already found. Any number of sources could be listed here.
include(duplicity2/CMakeLists.txt)
list(APPEND Hello2Shared_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/MyHello2.cpp
)
add_library (Hello2Shared SHARED ${Hello2Shared_SOURCES})

# Make sure the compiler can find include files for our Hello library
# when other libraries or executables link to Hello
target_include_directories (Hello2Shared PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
#target_link_libraries (Hello2Shared LINK_PUBLIC Hello3Shared)
