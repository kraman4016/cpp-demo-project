#ifndef MYSOCKET_H
#define MYSOCKET_H
namespace Demo {
class MySocket {
	public:
		MySocket();
		virtual ~MySocket();
		void run();
		void createServer();
        void protocolIndependentHostServiceConversion();
        void enableKeepAliveFeature();
	private:
};
}
using Demo::MySocket;
#endif /* MYSOCKET_H */


