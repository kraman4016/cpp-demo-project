#include "MySocket.h"

#include <iostream>
/*
// createServer
#include <thread>
#include <sys/socket.h> // socket(), bind() system call
#include <netinet/in.h> // struct sockaddr_in() 
*/

/*
// protocolIndependentHostServiceConversion
#include <sys/socket.h>
#include <netdb.h>      // struct addrinfo, ...
#include <cstring>      // memset, ...
*/

// enableKeepAliveFeature
#include <sys/socket.h>         // setsockopt, SOL_SOCKET, SO_KEEPALIVE
#include <netinet/in.h>         // IPPROTO_TCP
#include <netinet/tcp.h>        // TCP_KEEPIDLE, TCP_KEEPINTVL, TCP_KEEPCNT


MySocket::MySocket() {
	run();
}

MySocket::~MySocket() {
}

void MySocket::run() {
	//createServer();
    //protocolIndependentHostServiceConversion();
    enableKeepAliveFeature();
}

// September 04, 2019
void MySocket::enableKeepAliveFeature() {
    int sockFd = 1;
    
    sockFd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(-1 == sockFd) {
        std::cout << "socket fails" << '\n';
    } else {
        std::cout << "socket fd: " << sockFd << '\n';
    }
    
    
    bool bOptValue = true;
    socklen_t bOptLength = sizeof(bool);
    //int32_t iRet = setsockopt(sockFd, SOL_SOCKET, SO_KEEPALIVE, &bOptValue, bOptLength); // run-time error: option value type restriction
    int32_t optionValue = 1;
    socklen_t optionLength = sizeof(int32_t);
    int32_t iRet = setsockopt(sockFd, SOL_SOCKET, SO_KEEPALIVE, &optionValue, optionLength);
    if(-1 == iRet) {
        std::cout << "setsockopt fails" << sockFd << errno << '\n';
        perror("setsockopt-SOL_SOCKET: ");
    }
    
    int idle = 25;
    if(setsockopt(sockFd, IPPROTO_TCP, TCP_KEEPIDLE, &idle, sizeof(int)) == -1) {
        perror("setsockopt-TCP_KEEPIDLE: ");
    }
    int interval = 26;
    if(setsockopt(sockFd, IPPROTO_TCP, TCP_KEEPINTVL, &interval, sizeof(int)) == -1) {
        perror("setsockopt-TCP_KEEPINTVL: ");
    }

    int maxpkt = 27;
    if(setsockopt(sockFd, IPPROTO_TCP, TCP_KEEPCNT, &maxpkt, sizeof(int)) == -1) {
        perror("setsockopt-TCP_KEEPCNT: ");
    }

    int optval = -1;
    socklen_t optlen;
    optlen = sizeof(optval);

    if (getsockopt(sockFd, SOL_SOCKET, SO_KEEPALIVE, &optval, &optlen) == -1) {
        perror("getsockopt-SO_KEEPALIVE: ");
    } else {
        std::cout << "getsockopt-SO_KEEPALIVE-optval: " << optval << '\n';
    }

    if (getsockopt(sockFd, IPPROTO_TCP, TCP_KEEPIDLE, &optval, &optlen) == -1) {
        perror("getsockopt-TCP_KEEPIDLE: ");
    } else {
        std::cout << "getsockopt-TCP_KEEPIDLE-optval: " << optval << '\n';
    }

    if (getsockopt(sockFd, IPPROTO_TCP, TCP_KEEPINTVL, &optval, &optlen) == -1) {
        perror("getsockopt-TCP_KEEPINTVL: ");
    } else {
        std::cout << "getsockopt-TCP_KEEPINTVL-optval: " << optval << '\n';
    }

    if (getsockopt(sockFd, IPPROTO_TCP, TCP_KEEPCNT, &optval, &optlen) == -1) {
        perror("getsockopt-TCP_KEEPCNT: ");
    } else {
        std::cout << "getsockopt-TCP_KEEPCNT-optval: " << optval << '\n';
    }    
    while(1);
/*
    int yes = 1;
    if(setsockopt(sockFd, SOL_SOCKET, SO_KEEPALIVE, &yes, sizeof(int)) == -1) {
    }

    int idle = 1;
    if(setsockopt(sockFd, IPPROTO_TCP, TCP_KEEPIDLE, &idle, sizeof(int)) == -1) {
    }

    int interval = 1;
    if(setsockopt(sockFd, IPPROTO_TCP, TCP_KEEPINTVL, &interval, sizeof(int)) == -1) {
    }

    int maxpkt = 1;
    if(setsockopt(sockFd, IPPROTO_TCP, TCP_KEEPCNT, &maxpkt, sizeof(int)) == -1) {
    }*/

}


// September 03, 2019
void MySocket::protocolIndependentHostServiceConversion() {/*
    std::cout << __func__ << '\n';
    std::string ipAddress("192.168.129.43");

    const char *host = ipAddress.c_str();				// hostname or a numeric address string
    const char *service = NULL;						// a service name or a decimal port number

    struct addrinfo hints;                          // Structure to contain information about address of a service provider.
    memset(&hints, '\0', sizeof hints);

    // Protocol family for socket. The hints.ai_family field selects the domain for the returned socket address structures.
    // If we are interested in getting back all types of socket address structures, we can specify the value AF_UNSPEC for this field.
    hints.ai_family = AF_UNSPEC;                    

    // The hints.ai_flags field is a bit mask that modifies the behavior of getaddrinfo(). 
    // This field is formed by ORing together zero or more of the following values: 
    // Force interpretation of host as a numeric address string. This is used to prevent
    // name resolution in cases where it is unnecessary, since name resolution can be time-consuming.
    //hints.ai_flags = AI_NUMERICHOST;

    // Socket type. The hints.ai_socktype field specifies the type of socket for which the returned address structure is to be used. 
    // If we specify SOCK_STREAM, a lookup for the TCP service is performed. If hints.ai_socktype is specified as 0, any socket type is acceptable.
    hints.ai_socktype = SOCK_STREAM;                
    
    // Protocol for socket. The hints.ai_protocol field selects the socket protocol for the returned address structures.
    // For our purposes, this field is always specified as 0, meaning that the caller will accept any protocol.
    //hints.ai_protocol = IPPROTO_TCP;                
    
    struct addrinfo *result = NULL;

    // Given a hostname and a service name, getaddrinfo() returns a set of
    // structures containing the corresponding binary IP address(es) and port number.
    // getaddrinfo() function converts host and service names to IP addresses and port numbers
    // Returns 0 on success, or nonzero on error
    int ret = getaddrinfo(host, service, &hints, &result);		
    if (0 == ret) {
        std::cout << "getaddrinfo() success!" << '\n';
        freeaddrinfo(result);
    }
    
    // int conv = atoi(service);        // Segmentation fault

*/
}

void MySocket::createServer() {/*
	// creating a socket
	int domain = AF_INET;
	int type = SOCK_STREAM;
	int protocol = 0;
	
	int fd = socket(domain, type, protocol);
	std::cout << "fd : " << fd << "\n";

	// binding a socket to an address (assiginig a name to a socket)
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	int PORT = 8080;
	addr.sin_port = htons(PORT); // todo
	addr.sin_addr.s_addr = INADDR_ANY;
		
	int bRet = bind(fd, (const struct sockaddr *)&addr, sizeof(addr)); 
	std::cout << "bind ret : " << bRet << "\n";
	
	int BACKLOG = 3;
	bRet = listen(fd, BACKLOG);
	std::cout << "listen ret : " << bRet << "\n";

	int cfd = accept(fd, NULL, NULL);
	std::cout << "accept ret or fd : " << cfd << "\n";
*/
}

