#include "clientSock.h"
#include <iostream>
#include <string>

int main() {
	std::cout << "Bonjour le monde ! socket" << "\n";
    clientSock tcpClient("192.168.128.145", 4148);
        std::string hlData;
    
    hlData.clear();
    hlData.append(std::string("\x0B")); // <VT>
    hlData.append(std::string("MSH|^~\\&|H550^112YAXH47741^1.0.0.3|HORIBA_MEDICAL|||20161117152156||OUL^R22^OUL_R22|2017090909090900001|P|2.5||||||UNICODE UTF-8"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("PID|1||PID_212165^^^^PI||Arpit^Saxena||19871112020105|M"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("NTE|1|L|Thhoribamedat19871112020105 Arpit i1987/11/12s 12-Nov-1987 2016/11/17 15:21:56 horibameda frArpitePhysician_9879078x20161117152156t ORArpit aPID_212165sddfPID_212165a20161117152156nt|G"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("SPM|1|LIS4||WB|||||||P||||||20161117152156|"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("OBX|1|NM|35659-2^Age at specimen collection^LN||28|a|||||F"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("SAC||||||||||1000|3"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("OBR|1|||DIF||||||||||||Physician_9879078"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("ORC|UX||||||||||||||||^pediatrie4"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("NTE|1|L|Thhoribamedat19871112020105 Arpit i1987/11/12s 12-Nov-1987 2016/11/17 15:21:56 horibameda frArpitePhysician_9879078x20161117152156t ORArpit aPID_212165sddfPID_212165a20161117152156nt|G"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("OBX|1|NM|6690-2^WBC^LN||8.99|10E3/uL|3.50 - 10.00^REFERENCE_RANGE|N|||F|||||Tech_111"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("OBX|2|NM|789-8^RBC^LN||4.25|10E6/uL|3.80 - 6.00^REFERENCE_RANGE|N|||F|||||Tech_111"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("OBX|3|NM|718-7^HGB^LN||13.8|g/dL|11.5 - 17.0^REFERENCE_RANGE|N|||F|||||Tech_11"));
    hlData.append(std::string("\x0D")); //<CR>
    hlData.append(std::string("\x1C")); //<FS>
    hlData.append(std::string("\x0D")); //<CR>
    
    tcpClient.write(hlData);
    std::this_thread::sleep_for(std::chrono::seconds(10));
    tcpClient.write(hlData);
    std::this_thread::sleep_for(std::chrono::seconds(10));
    std::this_thread::sleep_for(std::chrono::seconds(10));
    std::this_thread::sleep_for(std::chrono::seconds(10));    
    std::this_thread::sleep_for(std::chrono::seconds(200));        
}
