#include "MyClass.h"

#include <iostream>

MyClass::MyClass() {
	run();
}

MyClass::~MyClass() {
}

void MyClass::run() {
	issue1();
}

void MyClass::issue1() {	
	const char c = 'c'; 
	char c2 = 'd'; 
	char* pc; 
		const char* pc2; 
		//pc = &c;	//error: invalid conversion from ‘const char*’ to ‘char*’ [-fpermissive]
		pc = &c2;
		pc2 = &c;
		pc2 = &c2;
	const char** pcc = &pc; //1: not allowed  	error: invalid conversion from ‘char**’ to ‘const char**’ [-fpermissive]
	*pcc = &c; 

	*pc = 'C'; //2: modifies a const object
}
