#ifndef MYCLASS_H
#define MYCLASS_H


namespace Demo {
class MyClass {
	public:
	        MyClass();
		virtual ~MyClass();

		void run();
		void issue1();
	private:

};
}
using Demo::MyClass;
#endif /* MYCLASS_H */


