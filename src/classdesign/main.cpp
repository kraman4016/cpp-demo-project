#include "classdesign/MyClass.h"
#include <iostream>

int main() {
	std::cout << "Bonjour le monde ! classdesign" << "\n";

	MyClass mc;
	mc.validateField(1, "a");
        //MyClass mc2 = MyClass();        // error: use of deleted function \
                                                ‘Demo::MyClass::MyClass(const Demo::MyClass&)’
        //MyClass mc3 = mc;               // error: use of deleted function \
                                                ‘Demo::MyClass::MyClass(const Demo::MyClass&)’
//        MyClass mc4;
        //mc4 = mc;                       // error: use of deleted function \
                                                  ‘Demo::MyClass& Demo::MyClass::operator=(const Demo::MyClass&)’
        //MyClass mc5 = std::move(mc4);   // if move constructor deleted: \
                                                error: use of deleted function ‘Demo::MyClass::MyClass(Demo::MyClass&&)’


}
