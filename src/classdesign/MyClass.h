#ifndef MYCLASS_H
#define MYCLASS_H
namespace Demo {
class MyClass {
	public:
		MyClass();
                MyClass(const MyClass&) = delete;
                MyClass& operator=(const MyClass&) = delete;
                MyClass( MyClass && ) = delete;
                MyClass& operator=(MyClass &&) = delete;

		virtual ~MyClass();

		void run();
//protected:
	template<typename T>
	void validateField(int nb, T& fieldValue){}
	private:
};
}
using Demo::MyClass;
#endif /* MYCLASS_H */


