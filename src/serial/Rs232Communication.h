/*
 * Rs232Communication.h
 *
 *  Created on: Dec 26, 2017
 *      Author: horiba
 */

#ifndef TRANSPORT_RS232_RS232COMMUNICATION_H_
#define TRANSPORT_RS232_RS232COMMUNICATION_H_
#include "../AbstractCommunication.h"       // base class
#include "../common/TraceabilityLog.h"      // TraceabilityLog function parameter in constructor
#include <cstdint>                          // uint32_t

// linux platform specific includes
#if defined(__linux__)
#include <termios.h>                        // POSIX terminal control definitions : struct termios
#endif

// windows platform specific includes
#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif


///
/// @brief
///
class Rs232Communication final: public IAbstractCommunication {
public:
    ///
    /// @brief constructor
    /// @param
    /// @return
    /// @throws
    Rs232Communication();

    ///
    /// @brief deleted copy constructor
    /// @param
    /// @return
    /// @throws
    Rs232Communication(const Rs232Communication &)=delete;

    ///
    /// @brief deleted copy assignment operator
    /// @param
    /// @return
    /// @throws
    Rs232Communication& operator = (const Rs232Communication &)=delete;

    ///
    /// @brief destructor
    /// @param
    /// @return
    /// @throws
    ~Rs232Communication();

    ///
    /// @brief openConnection() will create as socket and will connect with host and if error occurred set the Error accordingly (lastError == 0 means no error)
    /// @param const reference to INetworkSettings
    /// @return bool value : true -> successful, false -> fails
    /// @throws
    virtual bool openConnection(const IConnectionSettings& conSet, bool bUseSocket2 = false) override;

    ///
    /// @brief closeConnection() will close the connection with host and set the Error accordingly
    /// @param
    /// @return bool value : true -> successful, false -> fails
    /// @throws
    virtual bool closeConnection() override;

    ///
    /// @brief sendData() will send the data to host and if any error occurred set the error accordingly
    /// @param const refrence to string to be send
    /// @return bool value : true -> successful, false -> fails
    /// @throws
    virtual bool sendData(const std::string& dataToSend) noexcept override;

    ///
    /// @brief receiveData() will receive data the data from host and if any error occurred, error is set accordingly
    /// @param reference to string object, will be filled with received data
    /// @return bool value : true -> successful, false -> fails
    /// @throws
    virtual bool receiveData(std::string &dataReceived, EConnectionErrorType &connErrorType) override;


    ///
    /// @brief receiveSingleCharacter() will receive single control character from host and if any error occurred, error is set accordingly
    /// @param reference to unsigned char which will be updated with received char, integer max timeOut in seconds, reference to EConnectionErrorType holding the error
    /// @return bool value : true -> successful, false -> fails
    /// @throws
    virtual bool receiveSingleCharacter(unsigned char &data, const int timeOut, EConnectionErrorType &connErrorType) override;
    ///
    /// @brief setLastError() will set the last error occurred
    /// @param int value which is error code
    /// @return
    /// @throws
    virtual void setLastError(std::string err) noexcept override;

    ///
    /// @brief getLastError() will returns the last error occurred, if returned value is 0 means no error occurred
    /// @param
    /// @return  int value which is error code for last error
    /// @throws
    virtual std::string getLastError()const noexcept override;


    ///
    /// @brief clearLastError() will clears the last error
    /// @param
    /// @return
    /// @throws
    virtual void clearLastError() noexcept override;

    ///
    /// @brief getId() will return the unique id (fd for current socket in use ) for TCP unit
    /// @param
    /// @return  int value which is unique id of transport element
    /// @throws
    virtual int getId()const noexcept override;

    ///
    /// @brief isConnected() will check whether connected with host or not
    /// @param
    /// @return  bool value true -> connected; false-> not connected
    /// @throws
    virtual bool isConnected()const override;

    ///
    /// @brief resetConnection() reset data member isConnected
    /// @param
    /// @return  void
    /// @throws
    virtual void resetConnection(bool connectionWithHost) override;

private:
    ///
    /// @brief Enumeration defines the possible bytesizes for the serial port.
    ///
    enum class EByteSize: int8_t {
        FIVE_BITS = 5,
        SIX_BITS = 6,
        SEVEN_BITS = 7,
        EIGHT_BITS = 8
    };

    /*!
     * Enumeration defines the possible stopbit types for the serial port.
     */
    enum class EStopBits: int8_t {
        STOPBITS_ONE = 1,
        STOPBITS_TWO = 2,
        STOPBITS_ONE_POINT_FIVE
    };

    // linux platform specific class members
#if defined(__linux__)
    bool openPortLinux(const ISerialDeviceSettings& serialDeviceSettings);
    bool reconfigurePortLinux(const ISerialDeviceSettings& serialDeviceSettings);
    bool writeDataLinux(const uint8_t *data, const size_t length);
    bool readDataLinux(uint8_t *buf, size_t size, size_t &bytesRead);
    void flushInputLinux();
    void flushOutputLinux();
    bool closePortLinux();
    timespec timespecFromMills(const uint32_t millis);

    int32_t m_fileDescriptor;
#endif
    // windows platform specific class members
#if defined(_WIN32) || defined(_WIN64)
    bool openPortWindows(const ISerialDeviceSettings& serialDeviceSettings);
    bool reconfigurePortWindows(const ISerialDeviceSettings& serialDeviceSettings);
    bool closePortWindows();
    bool writeDataWindows(const uint8_t *data, size_t length, DWORD &bytesWritten);
    bool readDataWindows(uint8_t *buffer, size_t size, DWORD &bytesRead);

    HANDLE m_fileHandle;
#endif

    bool m_isConnectedWithHost;             // bool to inform whether connected with HOST or not
    std::string m_lastErrorStr;             // standard error code for last occurred error
};


#endif /* TRANSPORT_RS232_RS232COMMUNICATION_H_ */
