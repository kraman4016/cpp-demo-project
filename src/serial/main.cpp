#include <glob.h> // glob(), globfree()
#include <string.h> // memset()
#include <vector>
#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>

/*
[21799.497995] usb 2-2.1: new full-speed USB device number 4 using uhci_hcd
[21799.813407] usb 2-2.1: New USB device found, idVendor=0403, idProduct=6001, bcdDevice= 6.00
[21799.813414] usb 2-2.1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[21799.813491] usb 2-2.1: Product: USB Serial Converter
[21799.813496] usb 2-2.1: Manufacturer: FTDI
[21799.813498] usb 2-2.1: SerialNumber: FTGB7JJG
[21802.948199] usbcore: registered new interface driver usbserial_generic
[21802.948231] usbserial: USB Serial support registered for generic
[21803.337374] usbcore: registered new interface driver ftdi_sio
[21803.337411] usbserial: USB Serial support registered for FTDI USB Serial Device
[21803.337505] ftdi_sio 2-2.1:1.0: FTDI USB Serial Device converter detected
[21803.450237] usb 2-2.1: Detected FT232RL
[21803.937034] usb 2-2.1: FTDI USB Serial Device converter now attached to ttyUSB0
*/

std::vector<std::string> glob(const std::string& pattern) {
    using namespace std;

    // glob struct resides on the stack
    glob_t glob_result;
    memset(&glob_result, 0, sizeof(glob_result));

    // do the glob operation
    int return_value = glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
    if(return_value != 0) {
        globfree(&glob_result);
        stringstream ss;
        ss << "glob() failed with return_value " << return_value << endl;
        throw std::runtime_error(ss.str());
    }

    // collect all the filenames into a std::list<std::string>
    vector<string> filenames;
    for(size_t i = 0; i < glob_result.gl_pathc; ++i) {
        filenames.push_back(string(glob_result.gl_pathv[i]));
    }

    // cleanup
    globfree(&glob_result);

    // done
    return filenames;
}

int main() {
/*
    auto filenames = glob("/home/horiba/Desktop/one/work-p/cpp-demo-project/notes/v2/*");
    for (auto filename : filenames) {
        std::cout << filename << '\n';
    }
*/
    uint8_t buffer[10]= {65,66};
    std::cout << "reinterpret_cast()" << std::endl;
    std::string data;
    data.append(static_cast<const char*>(static_cast<void*>(buffer)), 2);
    return 0;
}
