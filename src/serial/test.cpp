//============================================================================
// Name        : test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fcntl.h>		// open, O_RDWR | O_NOCTTY | O_NONBLOCK
#include <errno.h>		// errno
#include <string.h>		// strerror
#include <termios.h>	// tcgetattr
#include <stdint.h>		// uint32_t
#include <stdio.h>		// sscanf
#include <unistd.h>		// write
#include <thread>		// std::this_thread



timespec
timespec_from_ms (const uint32_t millis)
{
	timespec time;
	time.tv_sec = millis / 1e3;
	time.tv_nsec = (millis - (time.tv_sec * 1e3)) * 1e6;
	return time;
}

class SerialImpl {
public:
	explicit SerialImpl(const std::string& port, uint32_t baudrate);
	void open();
	void reconfigurePort();
	void write(std::string sdata);
	void read();
	bool waitReadable(uint32_t timeout);
private:
	std::string port_;               	// Path to the file descriptor
	int fd_;                    		// The current file descriptor
	uint32_t baudrate_;    				// Baudrate
	bool xonxoff_;
	bool rtscts_;
};

SerialImpl::SerialImpl(const std::string& port, uint32_t baudrate) : port_(port), fd_(-1), baudrate_(baudrate), xonxoff_ (false), rtscts_ (false) {
}


void SerialImpl::open() {
	fd_ = ::open (port_.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);	// O_NONBLOCK ?

	if (fd_ == -1) {
		std::cout << "open fails " << strerror(errno) << std::endl;
	}
}



void SerialImpl::reconfigurePort() {
	struct termios options; // The options for the file descriptor
	int ret;

	if (tcgetattr(fd_, &options) == -1) {
		std::cout << "open fails " << strerror(errno) << std::endl;
	}

	// set up raw mode / no echo / binary
	options.c_cflag |= (tcflag_t)  (CLOCAL | CREAD);
	options.c_lflag &= (tcflag_t) ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL |
			ISIG | IEXTEN); //|ECHOPRT

	options.c_oflag &= (tcflag_t) ~(OPOST);
	options.c_iflag &= (tcflag_t) ~(INLCR | IGNCR | ICRNL | IGNBRK);

	speed_t baud;
	switch (baudrate_) {
#ifdef B9600
	case 9600: baud = B9600; break;
#endif
#ifdef B115200
	case 115200: baud = B115200; break;
#endif
	}
	ret = ::cfsetispeed(&options, baud);
	std::cout << "cfsetispeed " << ret << '\n';
	ret = ::cfsetospeed(&options, baud);
	std::cout << "cfsetospeed " << ret << '\n';

	// setup char len
	options.c_cflag &= (tcflag_t) ~CSIZE;
	options.c_cflag |= CS8;								// eightbits
	// setup stopbits
	options.c_cflag &= (tcflag_t) ~(CSTOPB);			// stopbits_one
	// setup parity
	options.c_iflag &= (tcflag_t) ~(INPCK | ISTRIP);
	options.c_cflag &= (tcflag_t) ~(PARENB | PARODD);	// parity_none

	// setup flow control
	// flowcontrol_software
	xonxoff_ = true;
	rtscts_ = false;

	if (xonxoff_)
		options.c_iflag |=  (IXON | IXOFF); //|IXANY)
	else
		options.c_iflag &= (tcflag_t) ~(IXON | IXOFF | IXANY);

	if (rtscts_)
		options.c_cflag |=  (CRTSCTS);
	else
		options.c_cflag &= (unsigned long) ~(CRTSCTS);

	// http://www.unixwiz.net/techtips/termios-vmin-vtime.html
	// this basically sets the read call up to be a polling read,
	// but we are using select to ensure there is data available
	// to read before each call, so we should never needlessly poll
	options.c_cc[VMIN] = 0;
	options.c_cc[VTIME] = 0;


	// activate settings
	::tcsetattr (fd_, TCSANOW, &options);
}


void SerialImpl::write(std::string sdata) {
	std::cout << __func__ << std::endl;
	size_t length = sdata.length();
	const uint8_t *data = reinterpret_cast<const uint8_t*>(sdata.c_str());
	size_t bytes_written = 0;

	while (bytes_written < length) {
		fd_set writefds;
		FD_ZERO (&writefds);
		FD_SET (fd_, &writefds);
		timespec timeout(timespec_from_ms(10));

		// Do the select
		int r = pselect (fd_ + 1, NULL, &writefds, NULL, &timeout, NULL);
		// Figure out what happened by looking at select's response 'r'
		/** Error **/
		if (r < 0) {
			// Select was interrupted, try again
			if (errno == EINTR) {
				//continue;
			}
			// Otherwise there was some error
			// TODO: THROW (IOException, errno);
		}
		/** Timeout **/
		if (r == 0) {
			//break;
		}
		/** Port ready to write **/
		if (r > 0) {
			// Make sure our file descriptor is in the ready to write list
			if (FD_ISSET (fd_, &writefds)) {
				// This will write some
				ssize_t bytes_written_now =
						::write (fd_, data + bytes_written, length - bytes_written);
				// write should always return some data as select reported it was
				// ready to write when we get to this point.
				if (bytes_written_now < 1) {
					// Disconnected devices, at least on Linux, show the
					// behavior that they are always ready to write immediately
					// but writing returns nothing.
					//throw SerialException ("device reports readiness to write but "
					//                     "returned no data (device disconnected?)");
				}
				// Update bytes_written
				bytes_written += static_cast<size_t> (bytes_written_now);
				// If bytes_written == size then we have written everything we need to
				if (bytes_written == length) {
					break;
				}
				// If bytes_written < size then we have more to write
				if (bytes_written < length) {
					continue;
				}
				// If bytes_written > size then we have over written, which shouldn't happen
				if (bytes_written > length) {
					//throw SerialException ("write over wrote, too many bytes where "
					//                     "written, this shouldn't happen, might be "
					//                   "a logical error!");
				}
			}
			// This shouldn't happen, if r > 0 our fd has to be in the list!
			//THROW (IOException, "select reports ready to write, but our fd isn't"
			//                  " in the list, this shouldn't happen!");
		}
	}
}

bool SerialImpl::waitReadable(uint32_t timeout) {
	std::cout << __func__ << std::endl;
	// Setup a select call to block for serial data or a timeout
	fd_set readfds;
	FD_ZERO (&readfds);
	FD_SET (fd_, &readfds);
	timespec timeout_ts (timespec_from_ms (timeout));
	int r = pselect (fd_ + 1, &readfds, NULL, NULL, &timeout_ts, NULL);

	if (r < 0) {
		// Select was interrupted
		if (errno == EINTR) {
			return false;
		}
		// Otherwise there was some error
		//THROW (IOException, errno);
	}
	// Timeout occurred
	if (r == 0) {
		return false;
	}
	// This shouldn't happen, if r > 0 our fd has to be in the list!
	if (!FD_ISSET (fd_, &readfds)) {
		return false;
		//THROW (IOException, "select reports ready to read, but our fd isn't"
		//	" in the list, this shouldn't happen!");
	}
	// Data available to read.
	return true;
}

void SerialImpl::read() {
	std::cout << __func__ << std::endl;
	std::string buffer;
	size_t size = 255;
	size_t bytes_read = 0;
//	uint8_t *buffer_ = new uint8_t[size];
//	uint8_t *buf = buffer_;
//	uint32_t timeout = 10;
	char *cbuffer = new char[255];

	// Pre-fill buffer with available bytes
	{
		ssize_t bytes_read_now = ::read (fd_, cbuffer, size);
		if (bytes_read_now > 0) {
			bytes_read = bytes_read_now;
		}
	}
	//	if (waitReadable(timeout)) {
	//		while (bytes_read < size) {
	//			std::cout << __func__ << ", before read" << std::endl;
	//			// Wait for the device to be readable, and then attempt to read.
	//
	//			// This should be non-blocking returning only what is available now
	//			//  Then returning so that select can block again.
	//			ssize_t bytes_read_now =
	//					::read (fd_, buf + bytes_read, size - bytes_read);
	//			// read should always return some data as select reported it was
	//			// ready to read when we get to this point.
	//			if (bytes_read_now < 1) {
	//				// Disconnected devices, at least on Linux, show the
	//				// behavior that they are always ready to read immediately
	//				// but reading returns nothing.
	//			}
	//			// Update bytes_read
	//			bytes_read += static_cast<size_t> (bytes_read_now);
	//			// If bytes_read == size then we have read everything we need
	//			if (bytes_read == size) {
	//				break;
	//			}
	//			// If bytes_read < size then we have more to read
	//			if (bytes_read < size) {
	//				continue;
	//			}
	//			// If bytes_read > size then we have over read, which shouldn't happen
	//			if (bytes_read > size) {
	//				//	          throw SerialException ("read over read, too many bytes where "
	//				//	                                 "read, this shouldn't happen, might be "
	//				//	                                 "a logical error!");
	//			}
	//		}
	//	}
//	buffer.append (reinterpret_cast<const char*>(buffer_), bytes_read);
//	delete[] buffer_;
	std::cout << "bytes read: " << bytes_read << ", data read: " << cbuffer << std::endl;
	for ( ; *cbuffer != '\0'; ++cbuffer )
	{
	   printf("hex: %02x \t", *cbuffer);
	}
	std::cout << std::endl;
	//delete cbuffer;
}

int main(int argc, char**argv) {
	std::cout << "!!!Hello World from RS232!!!" << std::endl; // prints !!!Hello World!!!

	SerialImpl *pimpl_;
	unsigned long baud = 0;
	sscanf(argv[2], "%lu", &baud);
	pimpl_ = new SerialImpl(argv[1], baud);
	pimpl_->open();
	pimpl_->reconfigurePort();

	for (int i =0; i < 100; i++) {
		std::cout << "Iteration: " << i << std::endl;
		pimpl_->write(argv[3]);
		std::this_thread::sleep_for(std::chrono::seconds(2));
		pimpl_->read();
	}

	delete pimpl_;
	return 0;
}
