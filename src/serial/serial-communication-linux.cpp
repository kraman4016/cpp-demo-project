//============================================================================
// Name        : test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fcntl.h>		// open, O_RDWR | O_NOCTTY | O_NONBLOCK
#include <errno.h>		// errno
#include <string.h>		// strerror
#include <termios.h>	// tcgetattr
#include <stdint.h>		// uint32_t
#include <stdio.h>		// sscanf
#include <unistd.h>		// write
#include <thread>		// std::this_thread



timespec
timespec_from_ms (const uint32_t millis)
{
	timespec time;
	time.tv_sec = millis / 1e3;
	time.tv_nsec = (millis - (time.tv_sec * 1e3)) * 1e6;
	return time;
}



class SerialImpl {
public:
	/*!
	 * Enumeration defines the possible bytesizes for the serial port.
	 */
	typedef enum {
		fivebits = 5,
		sixbits = 6,
		sevenbits = 7,
		eightbits = 8
	} bytesize_t;

	/*!
	 * Enumeration defines the possible stopbit types for the serial port.
	 */
	typedef enum {
		stopbits_one = 1,
		stopbits_two = 2,
		stopbits_one_point_five
	} stopbits_t;

	/*!
	 * Enumeration defines the possible parity types for the serial port.
	 */
	typedef enum {
		parity_none = 0,
		parity_odd = 1,
		parity_even = 2,
		parity_mark = 3,
		parity_space = 4
	} parity_t;

	/*!
	 * Enumeration defines the possible flowcontrol types for the serial port.
	 */
	typedef enum {
		flowcontrol_none = 0,
		flowcontrol_software,
		flowcontrol_hardware
	} flowcontrol_t;

public:
	explicit SerialImpl(const std::string& port, uint32_t baudrate, bytesize_t byteSize, stopbits_t stopbit, parity_t parity, flowcontrol_t flowcontrol);
	void open();
	void reconfigurePort();
	void write(std::string sdata);
	void write(const uint8_t *data, size_t length);
	void read();
	bool waitReadable(uint32_t timeout);
private:
	std::string port_;               	// Path to the file descriptor
	int fd_;                    		// The current file descriptor
	uint32_t baudrate_;    				// Baudrate
	bool xonxoff_;
	bool rtscts_;
	bytesize_t m_byteSize;
	stopbits_t m_stopbits;				// Stop Bits
	parity_t m_parity;
	flowcontrol_t m_flowcontrol;
};

SerialImpl::SerialImpl(const std::string& port, uint32_t baudrate, bytesize_t byteSize, stopbits_t stopbit, parity_t parity, flowcontrol_t flowcontrol) :
																		port_(port), fd_(-1), baudrate_(baudrate), xonxoff_ (false), rtscts_ (false), m_byteSize(byteSize), m_stopbits(stopbit), m_parity(parity), m_flowcontrol(flowcontrol) {
}


void SerialImpl::open() {
	fd_ = ::open (port_.c_str(), O_RDWR | O_NOCTTY /*| O_NONBLOCK | O_NDELAY*/);	// TODO : O_NONBLOCK ?

	if (fd_ == -1) {
		std::cout << "open fails " << strerror(errno) << std::endl;
	}
}



void SerialImpl::reconfigurePort() {
	struct termios options; // The options for the file descriptor
	int ret;

	if (tcgetattr(fd_, &options) == -1) {
		std::cout << "open fails " << strerror(errno) << std::endl;
	}

	//cfmakeraw(&options);										// DONE : non standard or not a POSIX call
	// set up raw mode / no echo / binary
	options.c_cflag |= (tcflag_t)  (CLOCAL | CREAD);			// DONE : your program does not become the 'owner' of the port subject to sporatic job control and hangup signals, and also that the serial interface driver will read incoming data bytes.

	// DONE : non-canonical mode
	options.c_lflag &= (tcflag_t) ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL |
			ISIG | IEXTEN); //|ECHOPRT
	options.c_oflag &= (tcflag_t) ~(OPOST);						// DONE : turn off processed output
	options.c_iflag &= (tcflag_t) ~(INLCR | IGNCR | ICRNL | IGNBRK);		// TODO

	// DONE
	speed_t baud;
	switch (baudrate_) {
#ifdef B9600
	case 9600: baud = B9600; break;
#endif
#ifdef B115200
	case 115200: baud = B115200; break;
#endif
	}
	ret = ::cfsetispeed(&options, baud);
	if (0 != ret) {
		std::cout << "cfsetispeed failure" << std::endl;
	}
	ret = ::cfsetospeed(&options, baud);
	if (0 != ret) {
		std::cout << "cfsetospeed failure" << std::endl;
	}

	// DONE
	// setup char len
	options.c_cflag &= (tcflag_t) ~CSIZE;
	if (eightbits == m_byteSize)
		options.c_cflag |= CS8;								// eightbits
	else if (sevenbits == m_byteSize)
		options.c_cflag |= CS7;
	else if (sixbits == m_byteSize)
		options.c_cflag |= CS6;
	else if (fivebits == m_byteSize)
		options.c_cflag |= CS5;

	// DONE
	// setup stopbits
	if (stopbits_one == m_stopbits)
		options.c_cflag &= (tcflag_t) ~(CSTOPB);			// stopbits_one
	else if (stopbits_two == m_stopbits)
		options.c_cflag |=  (CSTOPB);

	// DONE
	// setup parity
	options.c_iflag &= (tcflag_t) ~(INPCK | ISTRIP);		// TODO
	if (parity_none == m_parity) {
		options.c_cflag &= (tcflag_t) ~(PARENB | PARODD);
	} else if (parity_even == m_parity) {
		options.c_cflag &= (tcflag_t) ~(PARODD);
		options.c_cflag |=  (PARENB);
	} else if (parity_odd == m_parity) {
		options.c_cflag |=  (PARENB | PARODD);
	}


	// DONE
	// setup flow control
	if (flowcontrol_none == m_flowcontrol) {
		xonxoff_ = false;
		rtscts_ = false;
	}
	if (flowcontrol_software == m_flowcontrol) {
		xonxoff_ = true;
		rtscts_ = false;
	}
	if (flowcontrol_hardware == m_flowcontrol) {
		xonxoff_ = false;
		rtscts_ = true;
	}
	// xonxoff
#ifdef IXANY
	if (xonxoff_)
		options.c_iflag |=  (IXON | IXOFF); //|IXANY)			// DONE
	else
		options.c_iflag &= (tcflag_t) ~(IXON | IXOFF | IXANY);
#else
	if (xonxoff_)
		options.c_iflag |=  (IXON | IXOFF);
	else
		options.c_iflag &= (tcflag_t) ~(IXON | IXOFF);
#endif
	// rtscts
#ifdef CRTSCTS
	if (rtscts_)
		options.c_cflag |=  (CRTSCTS);							// DONE
	else
		options.c_cflag &= (unsigned long) ~(CRTSCTS);
#elif defined CNEW_RTSCTS
	if (rtscts_)
		options.c_cflag |=  (CNEW_RTSCTS);
	else
		options.c_cflag &= (unsigned long) ~(CNEW_RTSCTS);
#else
#error "OS Support seems wrong."
#endif

	// DONE
	// http://www.unixwiz.net/techtips/termios-vmin-vtime.html
	// this basically sets the read call up to be a polling read,
	// but we are using select to ensure there is data available
	// to read before each call, so we should never needlessly poll
	options.c_cc[VMIN] = 0;
	options.c_cc[VTIME] = 0;

	//processed output, replace 0x0A with 0x0D 0x0A
	//	options.c_oflag |= OPOST;
	//	options.c_oflag |= ONLCR;
	//	options.c_oflag &= (tcflag_t) ~(OPOST);

	// it can be set at terminal level, so no need to hard code these values, and below are default values
//	options.c_cc[VSTART]   = 17;     /* Ctrl-q XON*/
//	options.c_cc[VSTOP]    = 19;     /* Ctrl-s XOFF*/

	// Flush input buffer and output buffer
	tcflush (fd_, TCIFLUSH);
	tcflush (fd_, TCOFLUSH);

	// activate settings
	ret = ::tcsetattr (fd_, TCSANOW, &options);
	if (0 != ret) {
		std::cout << "tcsetattr failure" << std::endl;
	}
}


void SerialImpl::write(std::string sdata) {
	size_t length = sdata.length();
	const uint8_t *data = reinterpret_cast<const uint8_t*>(sdata.c_str());
	this->write(data, length);
}
void SerialImpl::write(const uint8_t *data, size_t length) {
	std::cout << __func__ << std::endl;
	size_t bytes_written = 0;

	while (bytes_written < length) {
		std::cout << __func__ << __LINE__ << " while in " << std::endl;
		fd_set writefds;
		FD_ZERO (&writefds);
		FD_SET (fd_, &writefds);
		timespec timeout(timespec_from_ms(10));

		// Do the select
		int r = pselect (fd_ + 1, NULL, &writefds, NULL, &timeout, NULL);
		// Figure out what happened by looking at select's response 'r'
		std::cout << __func__ << __LINE__ << " pselect return value " << r << std::endl;
		/** Error **/
		if (r < 0) {
			// Select was interrupted, try again
			if (errno == EINTR) {
				//continue;
			}
			// Otherwise there was some error
			// TODO: THROW (IOException, errno);
		}
		/** Timeout **/
		if (r == 0) {
			//break;
		}
		/** Port ready to write **/
		if (r > 0) {
			// Make sure our file descriptor is in the ready to write list
			if (FD_ISSET (fd_, &writefds)) {
				//				const uint8_t fixedData[11] = {0x61, 0xa, 0x61, 0x0d, 0x62, 0x63, 0x0d, 0x0a, 0x64, 0x65, 0x66};
				//				::write(fd_, fixedData, 11);
				// This will write some
				ssize_t bytes_written_now =
						::write (fd_, data + bytes_written, length - bytes_written);
				std::cout << __func__ << __LINE__ << " bytes_written now " << bytes_written_now << " bytes_written " << bytes_written << std::endl;
				// write should always return some data as select reported it was
				// ready to write when we get to this point.
				if (bytes_written_now < 1) {
					std::cout << __func__ << __LINE__ << " bytes_written < 1 " << std::endl;
					// Disconnected devices, at least on Linux, show the
					// behavior that they are always ready to write immediately
					// but writing returns nothing.
					//throw SerialException ("device reports readiness to write but "
					//                     "returned no data (device disconnected?)");
				}
				// Update bytes_written
				bytes_written += static_cast<size_t> (bytes_written_now);
				// If bytes_written == size then we have written everything we need to
//				if (bytes_written == length) {
//					std::cout << __func__ << __LINE__ << " bytes_written == length " << std::endl;
//					break;
//				}
//				// If bytes_written < size then we have more to write
//				if (bytes_written < length) {
//					std::cout << __func__ << __LINE__ << " bytes_written < length " << std::endl;
//					continue;
//				}
//				// If bytes_written > size then we have over written, which shouldn't happen
//				if (bytes_written > length) {
//					std::cout << __func__ << __LINE__ << " bytes_written > length " << std::endl;
//					//throw SerialException ("write over wrote, too many bytes where "
//					//                     "written, this shouldn't happen, might be "
//					//                   "a logical error!");
//				}
			}
			// This shouldn't happen, if r > 0 our fd has to be in the list!
			//THROW (IOException, "select reports ready to write, but our fd isn't"
			//                  " in the list, this shouldn't happen!");
		}
	}
}

bool SerialImpl::waitReadable(uint32_t timeout) {
	std::cout << __func__ << std::endl;
	// Setup a select call to block for serial data or a timeout
	fd_set readfds;
	FD_ZERO (&readfds);
	FD_SET (fd_, &readfds);
	timespec timeout_ts (timespec_from_ms (timeout));
	int r = pselect (fd_ + 1, &readfds, NULL, NULL, &timeout_ts, NULL);

	if (r < 0) {
		// Select was interrupted
		if (errno == EINTR) {
			return false;
		}
		// Otherwise there was some error
		//THROW (IOException, errno);
	}
	// Timeout occurred
	if (r == 0) {
		return false;
	}
	// This shouldn't happen, if r > 0 our fd has to be in the list!
	if (!FD_ISSET (fd_, &readfds)) {
		return false;
		//THROW (IOException, "select reports ready to read, but our fd isn't"
		//	" in the list, this shouldn't happen!");
	}
	// Data available to read.
	return true;
}

void SerialImpl::read() {
	std::cout << __func__ << std::endl;
	std::string buffer;
	size_t size = 255;
	size_t bytes_read = 0;
	//	uint8_t *buffer_ = new uint8_t[size];
	//	uint8_t *buf = buffer_;
	//	uint32_t timeout = 10;
	char *cbuffer = new char[255];

	// Pre-fill buffer with available bytes
	{
		ssize_t bytes_read_now = ::read (fd_, cbuffer, size);
		if (bytes_read_now > 0) {
			bytes_read = bytes_read_now;
		}
	}
	//	if (waitReadable(timeout)) {
	//		while (bytes_read < size) {
	//			std::cout << __func__ << ", before read" << std::endl;
	//			// Wait for the device to be readable, and then attempt to read.
	//
	//			// This should be non-blocking returning only what is available now
	//			//  Then returning so that select can block again.
	//			ssize_t bytes_read_now =
	//					::read (fd_, buf + bytes_read, size - bytes_read);
	//			// read should always return some data as select reported it was
	//			// ready to read when we get to this point.
	//			if (bytes_read_now < 1) {
	//				// Disconnected devices, at least on Linux, show the
	//				// behavior that they are always ready to read immediately
	//				// but reading returns nothing.
	//			}
	//			// Update bytes_read
	//			bytes_read += static_cast<size_t> (bytes_read_now);
	//			// If bytes_read == size then we have read everything we need
	//			if (bytes_read == size) {
	//				break;
	//			}
	//			// If bytes_read < size then we have more to read
	//			if (bytes_read < size) {
	//				continue;
	//			}
	//			// If bytes_read > size then we have over read, which shouldn't happen
	//			if (bytes_read > size) {
	//				//	          throw SerialException ("read over read, too many bytes where "
	//				//	                                 "read, this shouldn't happen, might be "
	//				//	                                 "a logical error!");
	//			}
	//		}
	//	}
	//	buffer.append (reinterpret_cast<const char*>(buffer_), bytes_read);
	//	delete[] buffer_;
	std::cout << "bytes read: " << bytes_read << ", data read: " << cbuffer << std::endl;
	for ( ; *cbuffer != '\0'; ++cbuffer )
	{
		printf("hex: %02x \t", *cbuffer);
	}
	std::cout << std::endl;
	//delete cbuffer;
}

int main(int argc, char**argv) {
	std::cout << "!!!Hello World from RS232!!!" << std::endl; // prints !!!Hello World!!!
	std::cout << "exe portname baudrate writedata byteSize stopbit parity flowcontrol" << std::endl;

	SerialImpl *pimpl_;
	unsigned long baud = 0;
	sscanf(argv[2], "%lu", &baud);
	unsigned long charLength = 0;
	sscanf(argv[4], "%lu", &charLength);
	unsigned long stopbit = 0;
	sscanf(argv[5], "%lu", &stopbit);
	unsigned long parity = 0;
	sscanf(argv[6], "%lu", &parity);
	unsigned long flowcontrol = 0;
	sscanf(argv[7], "%lu", &flowcontrol);

	pimpl_ = new SerialImpl(argv[1], baud, static_cast<SerialImpl::bytesize_t>(charLength),
			static_cast<SerialImpl::stopbits_t>(stopbit), static_cast<SerialImpl::parity_t>(parity),
			static_cast<SerialImpl::flowcontrol_t>(flowcontrol));
	pimpl_->open();
	pimpl_->reconfigurePort();

	//uint8_t fixedData[11] = {0x61, 0xa, 0x61, 0x0d, 0x62, 0x63, 0x0d, 0x0a, 0x64, 0x65, 0x66};
	//const uint8_t fixedDataWithSTOP[11] = {0x61, 0xa, 0x61, 0x0d, 0x62, 0x63, 0x0d, 0x0a, 0x13, 0x65, 0x66};
	//const uint8_t fixedDataWithSTART[11] = {0x61, 0xa, 0x61, 0x0d, 0x62, 0x63, 0x0d, 0x0a, 0x11, 0x65, 0x66};
	std::string alphabets("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");

	for (int i =0; i < 100; i++) {
		std::cout << __func__ << "Iteration: " << i << std::endl;
		std::cout << __func__ << "data: " << alphabets << "\t length: " << alphabets.length() << std::endl;
		const uint8_t* fixedData = reinterpret_cast<const uint8_t*>(alphabets.c_str());
		pimpl_->write(fixedData, alphabets.length());
		std::this_thread::sleep_for(std::chrono::seconds(3));
		pimpl_->read();
//		pimpl_->write(fixedDataWithSTOP, 11);	// stops receiving data until START received
//		std::this_thread::sleep_for(std::chrono::seconds(20));
//		pimpl_->read();
//		pimpl_->write(fixedDataWithSTART, 11); 	// resume the receive data
//		std::this_thread::sleep_for(std::chrono::seconds(20));
//		pimpl_->read();
	}

	delete pimpl_;
	return 0;
}
