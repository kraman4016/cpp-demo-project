/*
 * Rs232Communication.cpp
 *
 *  Created on: Dec 26, 2017
 *      Author: horiba
 */
#include "Rs232Communication.h"
#include <sstream>      // std::stringstream

// linux platform specific includes
#if defined(__linux__)
#include <fcntl.h>      // open, O_RDWR | O_NOCTTY | O_NONBLOCK
#endif


/**
 *  @brief constructor
 *  @author  HIN-PS
 *  @date    28 Dec, 2017
 *  @version 1.0
 *  @param
 *  @return
 */
Rs232Communication::Rs232Communication() : m_isConnectedWithHost(false), m_lastErrorStr("") {
#if defined(__linux__)
    m_fileDescriptor = -1;
#endif
#if defined(_WIN32) || defined(_WIN64)
    m_fileHandle = INVALID_HANDLE_VALUE;
#endif
}


/**
 *  @brief openConnection() will open a new session (connect to host), if already not connected
 *  @author  HIN-PS
 *  @date    21 May, 2018
 *  @version 1.0
 *  @param const refrence to IConnectionSettings, which contains the HOST details
 *  @return bool value : true -> successful, false -> fails
 *  @section DESCRIPTION
 *  If already connected do nothing, else will create a socket and connect to host, in case of any error lastErrorNo is
 *  set.
 *
 */
bool Rs232Communication::openConnection(const IConnectionSettings& conSet,bool bUseSocket2) {
    bool bRet = false;

    if (true == m_isConnectedWithHost) {
        setLastError("Transport is already connected");
        return bRet;
    }
    // linux platform specific behaviour
#if defined(__linux__)
    bRet = this->openPortLinux(conSet.serialDeviceSettings);
    if (false == bRet) {
        return bRet;
    }
#endif
    // windows platform specific behaviour
#if defined(_WIN32) || defined(_WIN64)
    bRet = this->openPortWindows(conSet.serialDeviceSettings);
    if (false == bRet) {
        return bRet;
    }
#endif

    m_isConnectedWithHost = true;
    bRet = true;
    return bRet;
}



/**
 *  @brief closeConnection() will close an open session.
 *  @author  HIN-PS
 *  @date    22 May, 2018
 *  @version 1.0
 *  @param
 *  @return bool value : true -> successful, false -> fails
 *  @section DESCRIPTION
 *  Closes the serial port.
 *      For Linux:
 *          close() API or library call is used, The normal return value from close is 0;
 *          a value of -1 is returned in case of failure.
 *      For Windows:
 *          CloseHandle API is used, If the function succeeds, the return value is nonzero.
 *          If the function fails, the return value is zero.
 *
 */
bool Rs232Communication::closeConnection() {
    bool bRet = false;

    // linux platform specific behaviour
#if defined(__linux__)
    bRet = this->closePortLinux();
#endif

#if defined(_WIN32) || defined(_WIN64)
    bRet = this->closePortWindows();
#endif

    return bRet;
}
/**
 *  @brief receiveData() will recv data from host
 *  @author  HIN-PS
 *  @date    24 May, 2018
 *  @version 1.0
 *  @param reference to string object, will be updated with received data, connErrorType is a reference to EConnectionErrorType(holds connection error while receiving data
 *  @return bool
 *  @section DESCRIPTION
 *  receive data from host, in case of any error lastErrorNo is set.
 *
 */
bool Rs232Communication::receiveData(std::string &dataToReceive, EConnectionErrorType &connErrorType) {
    bool bRet = false;
    const uint32_t MAX_BYTES_REQUESTED = 300;
    uint8_t buffer[MAX_BYTES_REQUESTED];
    memset(&buffer, '\0', sizeof(buffer));

#if defined(__linux__)
    size_t bytesRead = 0;

    bRet = this->readDataLinux(buffer, MAX_BYTES_REQUESTED, bytesRead);
    if (false == bRet) {
        return bRet;
    }
    if (0 == bytesRead) {		// A value of zero indicates end-of-file
        setLastError("peer closed");
        m_isConnectedWithHost = false;
        connErrorType = EConnectionErrorType::CE_CONNECTION_ABSENT;
        return bRet;
    }
    dataToReceive.append(reinterpret_cast<const char*>(buffer), bytesRead);
#endif

#if defined(_WIN32) || defined(_WIN64)
    DWORD bytesRead = 0;
    bRet = this->readDataWindows(buffer, MAX_BYTES_REQUESTED, bytesRead);
    if (false == bRet) {
        connErrorType = EConnectionErrorType::CE_CONNECTION_ABSENT;
        return bRet;
    }
    dataToReceive.append(reinterpret_cast<const char*>(buffer), bytesRead);
#endif

    bRet = true;
    return bRet;
}


/**
 *  @brief receiveSingleCharacter() will recv single control char from host
 *  @author  HIN-PS
 *  @date    24 May, 2018
 *  @version 1.0
 *  @param reference to string object, will be updated with received data, const integer for timeout , refrence to EConnectionErrorType holds any error occured
 *  @return bool
 *  @section DESCRIPTION
 *  receive data from host, in case of any error lastErrorNo is set.
 *
 */


bool Rs232Communication::receiveSingleCharacter(unsigned char &charReceive, const int timeOut, EConnectionErrorType &connErrorType) {
    bool bRet = false;

#if defined(__linux__)
    int32_t nbRet = 0;
    // Setup a select call to block for serial data or a timeout
    fd_set readFds;
    FD_ZERO (&readFds);
    FD_SET (m_fileDescriptor, &readFds);
    timespec timeout(this->timespecFromMills(timeOut*1000));	// convert seconds to milli-seconds
    nbRet = pselect((m_fileDescriptor + 1), &readFds, NULL, NULL, &timeout, NULL);
    // there was some error
    if (-1 == nbRet) {
        connErrorType = EConnectionErrorType::CE_READ_ERROR;
        return bRet;
    }
    // Timeout occurred
    if (0 == nbRet) {
        connErrorType = EConnectionErrorType::CE_HOST_NO_REPLY;
        return bRet;
    }

    size_t bytesRead = 0;
    bRet = this->readDataLinux(&charReceive, 1, bytesRead);

    if (false == bRet) {
        connErrorType = EConnectionErrorType::CE_READ_ERROR;
        return bRet;
    }
    if (0 == bytesRead) {		// A value of zero indicates end-of-file
        setLastError("peer closed");
        m_isConnectedWithHost = false;
        connErrorType = EConnectionErrorType::CE_CONNECTION_ABSENT;
        return bRet;
    }
#endif

#if defined(_WIN32) || defined(_WIN64)
    DWORD bytesRead = 0;
    const uint32_t MAX_BYTES_REQUESTED = 1;
    uint8_t buffer[MAX_BYTES_REQUESTED];
    memset(&buffer, '\0', sizeof(buffer));

    bRet = this->readDataWindows(buffer, MAX_BYTES_REQUESTED, bytesRead);
    if (false == bRet) {
        std::this_thread::sleep_for(std::chrono::seconds(timeOut));
        connErrorType = EConnectionErrorType::CE_HOST_NO_REPLY;
        return bRet;
    }
    charReceive = buffer[0];
#endif

    bRet = true;
    return bRet;
}


/**
 *  @brief sendData() will send data to host
 *  @author  HIN-PS
 *  @date    25 May, 2018
 *  @version 1.0
 *  @param const refrence to string containg the data to send
 *  @return bool value : true -> successful, false -> fails
 *  @section DESCRIPTION
 *  send data to host, in case of any error lastErrorNo is set.
 *
 */
bool Rs232Communication::sendData(const std::string& dataToSend) noexcept {
    bool bRet = false;

#if defined(__linux__)
    bRet = this->writeDataLinux(reinterpret_cast<const uint8_t*>(dataToSend.c_str()), dataToSend.length());
    if (false == bRet) {
        return bRet;
    }
#endif

#if defined(_WIN32) || defined(_WIN64)
    DWORD bytesWritten = 0;
    bRet = this->writeDataWindows(reinterpret_cast<const uint8_t*>(dataToSend.c_str()), dataToSend.length(), bytesWritten);
    if (false == bRet) {
        return bRet;
    }
#endif

    bRet = true;
    return bRet;
}


/**
 *  @brief setLastError() will set the lastError
 *  @author  HIN-PS
 *  @date    28 Dec, 2017
 *  @version 1.0
 *  @param integer value which error code for the error
 *  @return
 *  @section DESCRIPTION
 *  sets the last error encountered
 *
 */
void Rs232Communication::setLastError(std::string err) noexcept {
    m_lastErrorStr = err;
}

/**
 *  @brief clearLastError() will clear the lastError
 *  @author  HIN-PS
 *  @date    28 Dec, 2017
 *  @version 1.0
 *  @param
 *  @return
 *  @section DESCRIPTION
 *  clears the last error
 *
 */
void Rs232Communication::clearLastError() noexcept {
    m_lastErrorStr = "";
}

/**
 *  @brief getLastError() will return the lastError
 *  @author  HIN-PS
 *  @date    28 Dec, 2017
 *  @version 1.0
 *  @param
 *  @return integer value which is error code of last encounter error
 *  @section DESCRIPTION
 *  returns the error code of last error encountered, if 0 means no error occurred
 *
 */
std::string Rs232Communication::getLastError() const noexcept {
    return (m_lastErrorStr);
}

/**
 *  @brief getId() will return the unique id of TCP unit
 *  @author  HIN-PS
 *  @date    28 Dec, 2017
 *  @version 1.0
 *  @param
 *  @return integer value which is current socket id
 *  @section DESCRIPTION
 *  Each TcpCommunication object can be distinguished based on the fd of socket they are using. Useful when multiple HOSTS
 *  are connected (Eg HL7)
 *
 */
int Rs232Communication::getId() const noexcept {

#if defined(__linux__)
    return (m_fileDescriptor);
#endif

#if defined(_WIN32) || defined(_WIN64)
    return reinterpret_cast<int32_t>(m_fileHandle);
#endif

}

/**
 *  @brief isConnected() will tell if connected with HOST (session with host is currently open or not)
 *  @author  HIN-PS
 *  @date    28 Dec, 2017
 *  @version 1.0
 *  @param
 *  @return bool value
 *  @section DESCRIPTION
 *  If session is open returns true else returns false
 *
 */
bool Rs232Communication::isConnected() const {
    return (m_isConnectedWithHost);
}


/**
 *  @brief resetConnection() resets data member m_isConnectedWithHost with connectionWithHost
 *  @author
 *  @date
 *  @version 1.0
 *  @param reset data member m_isConnectedWithHost with connectionWithHost
 *  @return void
 *  @section DESCRIPTION
 *
 *
 */
void Rs232Communication::resetConnection(bool connectionWithHost) {
    m_isConnectedWithHost = connectionWithHost;
}


/**
 *  @brief destructor
 *  @author  HIN-PS
 *  @date    28 Dec, 2017
 *  @version 1.0
 *  @param
 *  @return
 *  @section DESCRIPTION
 *  If session is still open try to close the session
 *
 */
Rs232Communication::~Rs232Communication() {
    if (true == m_isConnectedWithHost) {
        this->closeConnection();
    }
}


// START - linux platform specific class member function definitions
#if defined(__linux__)


/**
 *  @brief Open serial communication port on linux.
 *  @author HIN-RK
 *  @date October 21, 2019
 *  @param serialDeviceSettings - constant reference to ISerialDeviceSettings
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, openPortLinux() takes the arguments serialDeviceSettings.
 *  The serialDeviceSettings argument specifies attributes that are required while opening a port.
 *  As output, openPortLinux() returns true if communication port opened successfully, otherwise false.
 */
bool Rs232Communication::openPortLinux(const ISerialDeviceSettings& serialDeviceSettings) {
    bool bRet = false;
    std::string deviceName = serialDeviceSettings.name.c_str();

    // open() API: The open function creates and returns a new file descriptor for the file named by filename.
    m_fileDescriptor = ::open(deviceName.c_str(), O_RDWR | O_NOCTTY);
    if (-1 == m_fileDescriptor) {
        return bRet;
    }

    bRet = this->reconfigurePortLinux(serialDeviceSettings);
    if (false == bRet) {
        return bRet;
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief Reconfigure serial communication port attributes on linux.
 *  @author HIN-RK
 *  @date October 21, 2019
 *  @param serialDeviceSettings - constant reference to ISerialDeviceSettings
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, reconfigurePortLinux() takes the arguments serialDeviceSettings.
 *  The serialDeviceSettings argument specifies attributes that are required while reconfiguring a port.
 *  As output, reconfigurePortLinux() returns true if communication port reconfigured successfully, otherwise false.
 */
bool Rs232Communication::reconfigurePortLinux(const ISerialDeviceSettings& serialDeviceSettings) {
    bool bRet = false;
    int32_t iRet = 0;
    struct termios options;                 // The options for the file descriptor
    memset (&options, 0, sizeof(options));

    // tcgetattr() API: This function is used to examine the attributes of the terminal device with file descriptor filedes.
    iRet = ::tcgetattr(m_fileDescriptor, &options);
    if (-1 == iRet) {
        std::stringstream ss;
        ss << "tcgetattr() fails " << strerror(errno) << '\n';
        setLastError(ss.str());
        return bRet;
    }

    // set up raw mode
    // cfmakeraw() API: This function provides an easy way to set up *termios-p for what has traditionally been called ``raw mode'' in BSD. This uses noncanonical input, and turns off most processing to give an unmodified channel to the terminal.
    ::cfmakeraw(&options);                                        // non standard or not a POSIX call !
    options.c_cflag |= (tcflag_t)  (CLOCAL | CREAD);            // your program does not become the 'owner' of the port subject to sporatic job control and hangup signals, and also that the serial interface driver will read incoming data bytes.
    options.c_oflag &= (tcflag_t) ~(OPOST);                     // choose raw output

    // setup baud rate {1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200}
    speed_t baud;
    switch (serialDeviceSettings.speed) {
#ifdef B0
    case 0: baud = B0; break;
#endif
#ifdef B50
    case 50: baud = B50; break;
#endif
#ifdef B75
    case 75: baud = B75; break;
#endif
#ifdef B110
    case 110: baud = B110; break;
#endif
#ifdef B134
    case 134: baud = B134; break;
#endif
#ifdef B150
    case 150: baud = B150; break;
#endif
#ifdef B200
    case 200: baud = B200; break;
#endif
#ifdef B300
    case 300: baud = B300; break;
#endif
#ifdef B600
    case 600: baud = B600; break;
#endif
#ifdef B1200
    case 1200: baud = B1200; break;
#endif
#ifdef B1800
    case 1800: baud = B1800; break;
#endif
#ifdef B2400
    case 2400: baud = B2400; break;
#endif
#ifdef B4800
    case 4800: baud = B4800; break;
#endif
#ifdef B7200
    case 7200: baud = B7200; break;
#endif
#ifdef B9600
    case 9600: baud = B9600; break;
#endif
#ifdef B14400
    case 14400: baud = B14400; break;
#endif
#ifdef B19200
    case 19200: baud = B19200; break;
#endif
#ifdef B28800
    case 28800: baud = B28800; break;
#endif
#ifdef B57600
    case 57600: baud = B57600; break;
#endif
#ifdef B76800
    case 76800: baud = B76800; break;
#endif
#ifdef B38400
    case 38400: baud = B38400; break;
#endif
#ifdef B115200
    case 115200: baud = B115200; break;
#endif
#ifdef B128000
    case 128000: baud = B128000; break;
#endif
#ifdef B153600
    case 153600: baud = B153600; break;
#endif
#ifdef B230400
    case 230400: baud = B230400; break;
#endif
#ifdef B256000
    case 256000: baud = B256000; break;
#endif
#ifdef B460800
    case 460800: baud = B460800; break;
#endif
#ifdef B500000
    case 500000: baud = B500000; break;
#endif
#ifdef B576000
    case 576000: baud = B576000; break;
#endif
#ifdef B921600
    case 921600: baud = B921600; break;
#endif
#ifdef B1000000
    case 1000000: baud = B1000000; break;
#endif
#ifdef B1152000
    case 1152000: baud = B1152000; break;
#endif
#ifdef B1500000
    case 1500000: baud = B1500000; break;
#endif
#ifdef B2000000
    case 2000000: baud = B2000000; break;
#endif
#ifdef B2500000
    case 2500000: baud = B2500000; break;
#endif
#ifdef B3000000
    case 3000000: baud = B3000000; break;
#endif
#ifdef B3500000
    case 3500000: baud = B3500000; break;
#endif
#ifdef B4000000
    case 4000000: baud = B4000000; break;
#endif
    }
    // cfsetispeed() API: This function stores speed in *termios-p as the input speed.
    iRet = ::cfsetispeed(&options, baud);
    if (-1 == iRet) {
        std::stringstream ss;
        ss << "cfsetispeed() fails" << '\n';
        setLastError(ss.str());
        return bRet;
    }
    // cfsetospeed() API: This function stores speed in *termios-p as the output speed.
    iRet = ::cfsetospeed(&options, baud);
    if (-1 == iRet) {
        std::stringstream ss;
        ss << "cfsetospeed() fails" << '\n';
        setLastError(ss.str());
        return bRet;
    }

    // setup char length
    options.c_cflag &= (tcflag_t) ~CSIZE;
    EByteSize byteSize = static_cast<EByteSize>(serialDeviceSettings.dataBitNumber);
    if (EByteSize::EIGHT_BITS == byteSize) {
        options.c_cflag |= CS8;
    } else if (EByteSize::SEVEN_BITS == byteSize) {
        options.c_cflag |= CS7;
    } else if (EByteSize::SIX_BITS == byteSize) {
        options.c_cflag |= CS6;
    } else if (EByteSize::FIVE_BITS == byteSize) {
        options.c_cflag |= CS5;
    }

    // setup stopbits
    EStopBits stopbits = static_cast<EStopBits>(serialDeviceSettings.stopBit);
    if (EStopBits::STOPBITS_ONE == stopbits) {
        options.c_cflag &= (tcflag_t) ~(CSTOPB);            // stopbits_one
    } else if (EStopBits::STOPBITS_TWO == stopbits) {
        options.c_cflag |=  (CSTOPB);
    }

    // setup parity
    ERS232Parity parity = serialDeviceSettings.parity;
    if (ERS232Parity::NONE == parity) {
        options.c_cflag &= (tcflag_t) ~(PARENB | PARODD);
    } else if (ERS232Parity::EVEN == parity) {
        options.c_cflag &= (tcflag_t) ~(PARODD);
        options.c_cflag |=  (PARENB);
    } else if (ERS232Parity::ODD == parity) {
        options.c_cflag |=  (PARENB | PARODD);
    }

    // setup flow control
    ERS232Protocol flowControl = serialDeviceSettings.protocol;
    bool xonxoff;
    bool rtscts;
    if (ERS232Protocol::NONE == flowControl) {
        xonxoff = false;
        rtscts = false;
    }
    if (ERS232Protocol::XON_XOFF == flowControl) {
        xonxoff = true;
        rtscts = false;
    }
    if (ERS232Protocol::HARDWARE == flowControl) {
        xonxoff = false;
        rtscts = true;
    }
    // xonxoff
#ifdef IXANY
    if (true == xonxoff) {
        options.c_iflag |=  (IXON | IXOFF);
    } else {
        options.c_iflag &= (tcflag_t) ~(IXON | IXOFF | IXANY);
    }
#else
    if (true == xonxoff) {
        options.c_iflag |=  (IXON | IXOFF);
    } else {
        options.c_iflag &= (tcflag_t) ~(IXON | IXOFF);
    }
#endif
    // rtscts
#ifdef CRTSCTS
    if (true == rtscts) {
        options.c_cflag |=  (CRTSCTS);
    } else {
        options.c_cflag &= (unsigned long) ~(CRTSCTS);
    }
#elif defined CNEW_RTSCTS
    if (true == rtscts) {
        options.c_cflag |=  (CNEW_RTSCTS);
    } else {
        options.c_cflag &= (unsigned long) ~(CNEW_RTSCTS);
    }
#endif

    // this basically sets the read call up to be a polling read,
    // but we are using select to ensure there is data available
    // to read before each call, so we should never needlessly poll
    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 0;

    options.c_cc[VSTART]   = 17;     /* Ctrl-q XON*/
    options.c_cc[VSTOP]    = 19;     /* Ctrl-s XOFF*/

    this->flushInputLinux();
    this->flushOutputLinux();

    // activate settings
    // tcsetattr() API: This function sets the attributes of the terminal device with file descriptor filedes.
    iRet = ::tcsetattr(m_fileDescriptor, TCSANOW, &options);
    if (-1 == iRet) {
        std::stringstream ss;
        ss << "tcsetattr() fails" << '\n';
        setLastError(ss.str());
        return bRet;
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief  Write a string to the serial port on linux.
 *  @author HIN-RK
 *  @date October 21, 2019
 *  @param data - constant pointer to uint8_t
 *  @param length - constant value of size_t
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, writeDataLinux() takes the arguments data and length.
 *  The data argument points to buffer, it contains data to be written to the serial port.
 *  The length argument indicates how many bytes should be written from the given data buffer.
 *  As output, writeDataLinux() returns true if all the data written to serial port successfully, otherwise false.
 */
bool Rs232Communication::writeDataLinux(const uint8_t *data, const size_t length) {
    bool bRet = false;
    size_t bytesWritten = 0;

    while (bytesWritten < length) {
        // This will write some or complete data
        // write() API: The write function writes up to size bytes from buffer to the file with descriptor filedes.
        ssize_t bytesWrittenNow = ::write(m_fileDescriptor, (data + bytesWritten), (length - bytesWritten));

        if (bytesWrittenNow < 1) {
            std::stringstream ss;
            ss << "write() fails " << strerror(errno) << '\n';
            setLastError(ss.str());
            return bRet;
        }
        // Update bytes_written
        bytesWritten += static_cast<size_t>(bytesWrittenNow);
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief  Read a given amount of bytes from the serial port into a given buffer on linux.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param buffer - An uint8_t array of at least the requested size.
 *  @param size - A size_t defining how many bytes to be read.
 *  @param bytesRead - A reference to size_t.
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, readDataLinux() takes the arguments buffer, size and bytesRead.
 *  The buffer argument points to uint8_t array of at least the requested size, it contains data read from the serial port.
 *  The size argument indicates how many bytes to be read from the serial port.
 *  The bytesRead argument gets the number of bytes read as a result of the call to read.
 *  As output, readDataLinux() returns true if there is no error while reading data from serial port, otherwise false.
 */
bool Rs232Communication::readDataLinux(uint8_t *buffer, size_t size, size_t &bytesRead) {
    bool bRet = false;
    ssize_t bytesReadNow = 0;

    // This should be non-blocking returning only what is available now
    // read() API: The read function reads up to size bytes from the file with descriptor filedes, storing the results in the buffer.
    bytesReadNow = ::read(m_fileDescriptor, (buffer + bytesRead), (size - bytesRead));
    if (-1 == bytesReadNow) {
        std::stringstream ss;
        ss << "read() fails " << strerror(errno) << '\n';
        setLastError(ss.str());
        return bRet;
    }
    // Update bytes_read
    bytesRead += static_cast<size_t>(bytesReadNow);

    bRet = true;
    return bRet;
}


/**
 *  @brief  Flush only the input buffer on linux.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param
 *  @return
 *  @section DESCRIPTION
 *
 */
void Rs232Communication::flushInputLinux() {
    // tcflush() API: The tcflush function is used to clear the input and/or output queues associated with the terminal file filedes.
    // TCIFLUSH : Clear any input data received, but not yet read.
    tcflush(m_fileDescriptor, TCIFLUSH);
}


/**
 *  @brief  Flush only the output buffer on linux.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param
 *  @return
 *  @section DESCRIPTION
 *
 */
void Rs232Communication::flushOutputLinux() {
    // tcflush() API: The tcflush function is used to clear the input and/or output queues associated with the terminal file filedes.
    // TCOFLUSH : Clear any output data written, but not yet transmitted.
    tcflush(m_fileDescriptor, TCOFLUSH);
}


/**
 *  @brief  Closes the serial port on linux.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As output, closePortLinux() returns true if serial port closed successfully, otherwise false.
 */
bool Rs232Communication::closePortLinux() {
    bool bRet = false;          // return value object to indicate the result of close serial port.

    if (-1 != m_fileDescriptor) {
        int ret = 0;
        // close() API: The function close closes the file descriptor filedes.
        ret = ::close(m_fileDescriptor);                // close: The normal return value from close is 0

        if (0 == ret) {
            m_fileDescriptor = -1;
            m_isConnectedWithHost = false;
        } else {
            std::stringstream ss;
            ss << "close() fails " << strerror(errno) << '\n';
            setLastError(ss.str());
            return bRet;
        }
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief  Convert milliseconds to timspec structure.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param millis - const value of uint32_t
 *  @return time - timespec structure
 *  @section DESCRIPTION
 *
 */
timespec Rs232Communication::timespecFromMills(const uint32_t millis) {
    // timespec Structure: holding an interval broken down into seconds and nanoseconds.
    timespec time;
    time.tv_sec = millis / 1e3;
    time.tv_nsec = (millis - (time.tv_sec * 1e3)) * 1e6;
    return time;
}

#endif
// END - linux platform specific class member function definitions


// START - windows platform specific class member function definitions
#if defined(_WIN32) || defined(_WIN64)

/**
 *  @brief Open serial communication port on windows.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param serialDeviceSettings - constant reference to ISerialDeviceSettings
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, openPortWindows() takes the arguments serialDeviceSettings.
 *  The serialDeviceSettings argument specifies attributes that are required while opening a port.
 *  As output, openPortWindows() returns true if communication port opened successfully, otherwise false.
 */
bool Rs232Communication::openPortWindows(const ISerialDeviceSettings& serialDeviceSettings) {
    bool bRet = false;

    // convert std::string to LPCWSTR type
    std::string port = serialDeviceSettings.name;
    int32_t len;
    int32_t slength = static_cast<int32_t>(port.length() + 1);
    // MultiByteToWideChar() API: Maps a character string to a UTF-16 (wide character) string.
    len = MultiByteToWideChar(CP_ACP, 0, port.c_str(), slength, 0, 0);
    wchar_t buf[len];
    MultiByteToWideChar(CP_ACP, 0, port.c_str(), slength, buf, len);
    std::wstring wport(buf);
    LPCWSTR lp_port = wport.c_str();

    // CreateFile() API: Creates or opens a file or I/O device.
    m_fileHandle = CreateFile(lp_port,
                              GENERIC_READ | GENERIC_WRITE,
                              0,
                              0,
                              OPEN_EXISTING,
                              FILE_ATTRIBUTE_NORMAL,
                              0);

    if (INVALID_HANDLE_VALUE == m_fileHandle) {
        // GetLastError() API: Retrieves the calling thread's last-error code value.
        DWORD create_file_err = GetLastError();
        std::stringstream ss;
        switch (create_file_err) {
        case ERROR_FILE_NOT_FOUND:
            ss << "Specified port, " << serialDeviceSettings.name << ", does not exist.";
            setLastError(ss.str());
            return bRet;
        default:
            ss << "Unknown error opening the serial port: " << create_file_err;
            setLastError(ss.str());
            return bRet;
        }
    }

    bRet = this->reconfigurePortWindows(serialDeviceSettings);
    if (false == bRet) {
        return bRet;
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief Reconfigure serial communication port attributes on windows.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param serialDeviceSettings - constant reference to ISerialDeviceSettings
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, reconfigurePortWindows() takes the arguments serialDeviceSettings.
 *  The serialDeviceSettings argument specifies attributes that are required while reconfiguring a port.
 *  As output, reconfigurePortWindows() returns true if communication port reconfigured successfully, otherwise false.
 */
bool Rs232Communication::reconfigurePortWindows(const ISerialDeviceSettings& serialDeviceSettings) {
    bool bRet = false;
    std::stringstream ss;

    if (INVALID_HANDLE_VALUE == m_fileHandle) {
        // Can only operate on a valid file descriptor
        ss << "Invalid file descriptor, is the serial port open?";
        setLastError(ss.str());
        return bRet;
    }

    // size of broadcast buffers and reception
    int32_t iInQueue = 32000;       // The recommended size of the device's internal input buffer, in bytes.
    int32_t iOutQueue = 32000;      // The recommended size of the device's internal output buffer, in bytes.
    // SetupComm() API: Initializes the communications parameters for a specified communications device.
    bRet = SetupComm(m_fileHandle, iInQueue, iOutQueue);
    if (false == bRet) {
        ss << "Error setting serial port communication parameters";
        setLastError(ss.str());
        return bRet;
    }

    // DCB structure: Defines the control setting for a serial communications device.
    DCB dcbSerialParams;
    // FillMemory() API: Fills a block of memory with a specified value.
    FillMemory(&dcbSerialParams, sizeof(dcbSerialParams), 0);

    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

    // GetCommState() API: Retrieves the current control settings for a specified communications device.
    bRet = GetCommState(m_fileHandle, &dcbSerialParams);
    if (false == bRet) {
        //error getting state
        ss << "Error getting the serial port state.";
        setLastError(ss.str());
        return bRet;
    }

    // setup baud rate
    switch (serialDeviceSettings.speed) {
#ifdef CBR_0
    case 0: dcbSerialParams.BaudRate = CBR_0; break;
#endif
#ifdef CBR_50
    case 50: dcbSerialParams.BaudRate = CBR_50; break;
#endif
#ifdef CBR_75
    case 75: dcbSerialParams.BaudRate = CBR_75; break;
#endif
#ifdef CBR_110
    case 110: dcbSerialParams.BaudRate = CBR_110; break;
#endif
#ifdef CBR_134
    case 134: dcbSerialParams.BaudRate = CBR_134; break;
#endif
#ifdef CBR_150
    case 150: dcbSerialParams.BaudRate = CBR_150; break;
#endif
#ifdef CBR_200
    case 200: dcbSerialParams.BaudRate = CBR_200; break;
#endif
#ifdef CBR_300
    case 300: dcbSerialParams.BaudRate = CBR_300; break;
#endif
#ifdef CBR_600
    case 600: dcbSerialParams.BaudRate = CBR_600; break;
#endif
#ifdef CBR_1200
    case 1200: dcbSerialParams.BaudRate = CBR_1200; break;
#endif
#ifdef CBR_1800
    case 1800: dcbSerialParams.BaudRate = CBR_1800; break;
#endif
#ifdef CBR_2400
    case 2400: dcbSerialParams.BaudRate = CBR_2400; break;
#endif
#ifdef CBR_4800
    case 4800: dcbSerialParams.BaudRate = CBR_4800; break;
#endif
#ifdef CBR_7200
    case 7200: dcbSerialParams.BaudRate = CBR_7200; break;
#endif
#ifdef CBR_9600
    case 9600: dcbSerialParams.BaudRate = CBR_9600; break;
#endif
#ifdef CBR_14400
    case 14400: dcbSerialParams.BaudRate = CBR_14400; break;
#endif
#ifdef CBR_19200
    case 19200: dcbSerialParams.BaudRate = CBR_19200; break;
#endif
#ifdef CBR_28800
    case 28800: dcbSerialParams.BaudRate = CBR_28800; break;
#endif
#ifdef CBR_38400
    case 38400: dcbSerialParams.BaudRate = CBR_38400; break;
#endif
#ifdef CBR_57600
    case 57600: dcbSerialParams.BaudRate = CBR_57600; break;
#endif
#ifdef CBR_76800
    case 76800: dcbSerialParams.BaudRate = CBR_76800; break;
#endif
#ifdef CBR_115200
    case 115200: dcbSerialParams.BaudRate = CBR_115200; break;
#endif
#ifdef CBR_128000
    case 128000: dcbSerialParams.BaudRate = CBR_128000; break;
#endif
#ifdef CBR_153600
    case 153600: dcbSerialParams.BaudRate = CBR_153600; break;
#endif
#ifdef CBR_230400
    case 230400: dcbSerialParams.BaudRate = CBR_230400; break;
#endif
#ifdef CBR_256000
    case 256000: dcbSerialParams.BaudRate = CBR_256000; break;
#endif
#ifdef CBR_460800
    case 460800: dcbSerialParams.BaudRate = CBR_460800; break;
#endif
#ifdef CBR_921600
    case 921600: dcbSerialParams.BaudRate = CBR_921600; break;
#endif
    }

    // setup char len
    EByteSize byteSize = static_cast<EByteSize>(serialDeviceSettings.dataBitNumber);
    if (EByteSize::EIGHT_BITS == byteSize) {
        dcbSerialParams.ByteSize = 8;
    } else if (EByteSize::SEVEN_BITS == byteSize) {
        dcbSerialParams.ByteSize = 7;
    } else if (EByteSize::SIX_BITS == byteSize) {
        dcbSerialParams.ByteSize = 6;
    } else if (EByteSize::FIVE_BITS == byteSize) {
        dcbSerialParams.ByteSize = 5;
    }


    // setup stopbits
    EStopBits stopbits = static_cast<EStopBits>(serialDeviceSettings.stopBit);
    if (EStopBits::STOPBITS_ONE == stopbits) {
        dcbSerialParams.StopBits = ONESTOPBIT;
    } else if (EStopBits::STOPBITS_ONE_POINT_FIVE == stopbits) {
        dcbSerialParams.StopBits = ONE5STOPBITS;
    } else if (EStopBits::STOPBITS_TWO == stopbits) {
        dcbSerialParams.StopBits = TWOSTOPBITS;
    }

    // setup parity
    ERS232Parity parity = serialDeviceSettings.parity;
    if (ERS232Parity::NONE == parity) {
        dcbSerialParams.Parity = NOPARITY;
    } else if (ERS232Parity::EVEN == parity) {
        dcbSerialParams.Parity = EVENPARITY;
    } else if (ERS232Parity::ODD == parity) {
        dcbSerialParams.Parity = ODDPARITY;
    }


    // setup flowcontrol
    ERS232Protocol flowControl = serialDeviceSettings.protocol;
    if (ERS232Protocol::NONE == flowControl) {
        dcbSerialParams.fOutxCtsFlow = false;
        dcbSerialParams.fRtsControl = RTS_CONTROL_DISABLE;
        dcbSerialParams.fOutX = false;
        dcbSerialParams.fInX = false;
    }
    if (ERS232Protocol::XON_XOFF == flowControl) {
        dcbSerialParams.fOutxCtsFlow = false;
        dcbSerialParams.fRtsControl = RTS_CONTROL_DISABLE;
        dcbSerialParams.fOutX = true;
        dcbSerialParams.fInX = true;
    }
    if (ERS232Protocol::HARDWARE == flowControl) {
        dcbSerialParams.fOutxCtsFlow = true;
        dcbSerialParams.fRtsControl = RTS_CONTROL_HANDSHAKE;
        dcbSerialParams.fOutX = false;
        dcbSerialParams.fInX = false;
    }

    dcbSerialParams.XonChar = 0x11;   // ASCII_XON ;    // The value of the XON character for both transmission and reception.
    dcbSerialParams.XoffChar = 0x13;  // ASCII_XOFF ;   // The value of the XOFF character for both transmission and reception.
    // XonLim: The minimum number of bytes in use allowed in the input buffer before flow control is activated to allow transmission by the sender. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in the fInX, fRtsControl, or fDtrControl members.
    dcbSerialParams.XonLim = (WORD)((iInQueue * 3 )/10) ;      // 30 % d'occupation, 30% occupancy
    // XoffLim: The minimum number of free bytes allowed in the input buffer before flow control is activated to inhibit the sender. Note that the sender may transmit characters after the flow control signal has been activated, so this value should never be zero. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in the fInX, fRtsControl, or fDtrControl members. The maximum number of bytes in use allowed is calculated by subtracting this value from the size, in bytes, of the input buffer.
    dcbSerialParams.XoffLim = (WORD)((iOutQueue * 7 )/10) ;    // 70 % d'occupation

    dcbSerialParams.fBinary = true;
    dcbSerialParams.fParity = true ;
    // The value of the character used to signal an event.
    dcbSerialParams.EvtChar = (BYTE)0;    // we use the serial port in RX_CHAR only

    // activate settings
    // SetCommState() API: Configures a communications device according to the specifications in a device-control block (a DCB structure).
    bRet = SetCommState(m_fileHandle, &dcbSerialParams);
    if (false == bRet) {
        // CloseHandle() API: Closes an open object handle.
        CloseHandle(m_fileHandle);
        ss << "Error setting serial port settings.";
        setLastError(ss.str());
        return bRet;
    }

    // PurgeComm() API: Discards all characters from the output or input buffer of a specified communications resource. It can also terminate pending read or write operations on the resource.
    // PURGE_RXABORT: Terminates all outstanding overlapped read operations and returns immediately, even if the read operations have not been completed.
    // PURGE_TXABORT: Terminates all outstanding overlapped write operations and returns immediately, even if the write operations have not been completed.
    bRet = PurgeComm( m_fileHandle, PURGE_RXABORT | PURGE_TXABORT );
    if (false == bRet) {
        CloseHandle(m_fileHandle);
        ss << "Error while flushing buffers.";
        setLastError(ss.str());
        return bRet;
    }
    // PURGE_RXCLEAR: Clears the input buffer (if the device driver has one).
    // PURGE_TXCLEAR: Clears the output buffer (if the device driver has one).
    bRet = PurgeComm( m_fileHandle, PURGE_RXCLEAR | PURGE_TXCLEAR );
    if (false == bRet) {
        CloseHandle(m_fileHandle);
        ss << "Error while flushing buffers.";
        setLastError(ss.str());
        return bRet;
    }

    // Setup timeouts
    // COMMTIMEOUTS structure: Contains the time-out parameters for a communications device.
    COMMTIMEOUTS timeouts;
    FillMemory(&dcbSerialParams, sizeof(dcbSerialParams), 0);

    // Timeout for read operations are used temporarily until Bug#200 fixed (waitReadable is not implemented on Windows)
    timeouts.ReadIntervalTimeout = 0;
    timeouts.ReadTotalTimeoutConstant = 1000;
    timeouts.ReadTotalTimeoutMultiplier = 1;
    timeouts.WriteTotalTimeoutConstant = 1;
    timeouts.WriteTotalTimeoutMultiplier = 100;

    // SetCommTimeouts() API: Sets the time-out parameters for all read and write operations on a specified communications device.
    bRet = SetCommTimeouts(m_fileHandle, &timeouts);
    if (false == bRet) {
        ss << "Error setting timeouts.";
        setLastError(ss.str());
        return bRet;
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief  Closes the serial port on windows.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As output, closePortWindows() returns true if serial port closed successfully, otherwise false.
 */
bool Rs232Communication::closePortWindows(){
    bool bRet = false;

    if (true == m_isConnectedWithHost) {
        if (INVALID_HANDLE_VALUE != m_fileHandle) {
            // CloseHandle() API: Closes an open object handle.
            bRet = CloseHandle(m_fileHandle);
            if (false == bRet) {
                std::stringstream ss;
                ss << "Error while closing serial port: " << GetLastError();
                setLastError(ss.str());
                return bRet;
            } else {
                m_fileHandle = INVALID_HANDLE_VALUE;
            }
        }
        m_isConnectedWithHost = false;
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief  Write a string to the serial port on windows.
 *  @author HIN-RK
 *  @date October 21, 2019
 *  @param data - constant pointer to uint8_t
 *  @param length - constant value of size_t
 *  @param bytesWritten - A reference to DWORD.
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, writeDataWindows() takes the arguments data, length and bytesWritten.
 *  The data argument points to buffer, it contains data to be written to the serial port.
 *  The length argument indicates how many bytes should be written from the given data buffer.
 *  The bytesWritten argument gets the number of bytes written as a result of the call to write.
 *  As output, writeDataWindows() returns true if all the data written to serial port successfully, otherwise false.
 */
bool Rs232Communication::writeDataWindows(const uint8_t *data, size_t length, DWORD &bytesWritten) {
    bool bRet = false;
    std::stringstream ss;

    if (false == m_isConnectedWithHost) {
        ss << "port not opened.";
        setLastError(ss.str());
        return bRet;
    }

    // WriteFile() API: Writes data to the specified file or input/output (I/O) device.
    bRet = WriteFile(m_fileHandle, data, static_cast<DWORD>(length), &bytesWritten, NULL);
    if (false == bRet) {
        ss << "Error while writing to the serial port: " << GetLastError();
        setLastError(ss.str());
        return bRet;
    }

    bRet = true;
    return bRet;
}


/**
 *  @brief  Read a given amount of bytes from the serial port into a given buffer on windows.
 *  @author HIN-RK
 *  @date November 08, 2019
 *  @param buffer - An uint8_t array of at least the requested size.
 *  @param size - A size_t defining how many bytes to be read.
 *  @param bytesRead - A reference to DWORD.
 *  @return bRet - boolean value
 *  @section DESCRIPTION
 *  As input, readDataWindows() takes the arguments buffer, size and bytesRead.
 *  The buffer argument points to uint8_t array of at least the requested size, it contains data read from the serial port.
 *  The size argument indicates how many bytes to be read from the serial port.
 *  The bytesRead argument gets the number of bytes read as a result of the call to read.
 *  As output, readDataWindows() returns true if there is no error while reading data from serial port, otherwise false.
 */
bool Rs232Communication::readDataWindows(uint8_t *buffer, size_t size, DWORD &bytesRead) {
    bool bRet = false;
    std::stringstream ss;

    if (false == m_isConnectedWithHost) {
        ss << "port not opened.";
        setLastError(ss.str());
        return bRet;
    }
    bRet = ReadFile(m_fileHandle, buffer, static_cast<DWORD>(size), &bytesRead, NULL);
    if (false == bRet) {
        ss << "Error while reading from the serial port: " << GetLastError();
        setLastError(ss.str());
        return bRet;
    }

    bRet = true;
    return bRet;
}


#endif
// END - windows platform specific class member function definitions
