#include <iostream>
#include <sstream>
#include <thread>           // std::this_thread
//#include <windows.h>      // windows header files are case insensitive as compared to linux, windows.h is recommended for cross compilation code over Windows.h
#include <handleapi.h>      // for HANDLE
#include <stringapiset.h>   // for MultiByteToWideChar
#include <fileapi.h>        // for CreateFileW
#include <errhandlingapi.h> // for GetLastError
#include <WinError.h>       // for System Error Codes: ERROR_FILE_NOT_FOUND,...
#include <WinDef.h>         // for HWND type, must be before winbase.h
#include <winbase.h>        // for DCB structure type
#include <Winbase.h>        // control setting for a serial communications device e.g. baudrate,....
#include <cassert>          // assert



class SerialImpl {
public:
    /*!
     * Enumeration defines the possible bytesizes for the serial port.
     */
    typedef enum {
        fivebits = 5,
        sixbits = 6,
        sevenbits = 7,
        eightbits = 8
    } bytesize_t;

    /*!
     * Enumeration defines the possible stopbit types for the serial port.
     */
    typedef enum {
        stopbits_one = 1,
        stopbits_two = 2,
        stopbits_one_point_five
    } stopbits_t;

    /*!
     * Enumeration defines the possible parity types for the serial port.
     */
    typedef enum {
        parity_none = 0,
        parity_odd = 1,
        parity_even = 2,
        parity_mark = 3,
        parity_space = 4
    } parity_t;

    /*!
     * Enumeration defines the possible flowcontrol types for the serial port.
     */
    typedef enum {
        flowcontrol_none = 0,
        flowcontrol_software,
        flowcontrol_hardware
    } flowcontrol_t;

public:
    explicit SerialImpl(const std::string& port, uint32_t baudrate, bytesize_t byteSize, stopbits_t stopbit, parity_t parity, flowcontrol_t flowcontrol, uint32_t readIntervalTimeout, uint32_t readTotalTimeoutConstant, uint32_t readTotalTimeoutMultiplier, uint32_t writeTotalTimeoutConstant, uint32_t writeTotalTimeoutMultiplier);
    bool open();
    void reconfigurePort();
    size_t write (const uint8_t *data, size_t length);
    size_t read (uint8_t *buf, size_t size);
    void close ();
private:
    std::string port_;               	// Path to the file descriptor
    //    int fd_;                    		// The current file descriptor
    HANDLE fd_;
    bool is_open_;

    uint32_t baudrate_;    				// Baudrate
    bool xonxoff_;
    bool rtscts_;
    bytesize_t m_byteSize;
    stopbits_t m_stopbits;				// Stop Bits
    parity_t m_parity;
    flowcontrol_t m_flowcontrol;
    // // Timeout for read operations
    uint32_t m_readIntervalTimeout;
    uint32_t m_readTotalTimeoutConstant;
    uint32_t m_readTotalTimeoutMultiplier;
    uint32_t m_writeTotalTimeoutConstant;
    uint32_t m_writeTotalTimeoutMultiplier;
};


SerialImpl::SerialImpl(const std::string& port, uint32_t baudrate, bytesize_t byteSize, stopbits_t stopbit, parity_t parity, flowcontrol_t flowcontrol, uint32_t readIntervalTimeout, uint32_t readTotalTimeoutConstant, uint32_t readTotalTimeoutMultiplier, uint32_t writeTotalTimeoutConstant, uint32_t writeTotalTimeoutMultiplier) :
    port_(port), fd_(INVALID_HANDLE_VALUE), baudrate_(baudrate), xonxoff_ (false), rtscts_ (false), m_byteSize(byteSize), m_stopbits(stopbit), m_parity(parity), m_flowcontrol(flowcontrol), m_readIntervalTimeout(readIntervalTimeout), m_readTotalTimeoutConstant(readTotalTimeoutConstant), m_readTotalTimeoutMultiplier(readTotalTimeoutMultiplier), m_writeTotalTimeoutConstant(writeTotalTimeoutConstant), m_writeTotalTimeoutMultiplier(writeTotalTimeoutMultiplier) {
}


bool SerialImpl::open() {
    bool bRet = false;

    // TODO : RS232#P09 - WIP: convert std::string/const char* to LPCWSTR
    std::string port = port_;
    int32_t len;
    int32_t slength = static_cast<int32_t>(port.length() + 1);
    len = MultiByteToWideChar(CP_ACP, 0, port.c_str(), slength, 0, 0);
    wchar_t buf[len];
    MultiByteToWideChar(CP_ACP, 0, port.c_str(), slength, buf, len);
    std::wstring wport(buf);
    LPCWSTR lp_port = wport.c_str();
    //    HANDLE CreateFileW(
    //      LPCWSTR               lpFileName,
    //      DWORD                 dwDesiredAccess,
    //      DWORD                 dwShareMode,
    //      LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    //      DWORD                 dwCreationDisposition,
    //      DWORD                 dwFlagsAndAttributes,
    //      HANDLE                hTemplateFile
    //    );
    //    HANDLE CreateFileA(
    //      LPCSTR                lpFileName,
    //      DWORD                 dwDesiredAccess,
    //      DWORD                 dwShareMode,
    //      LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    //      DWORD                 dwCreationDisposition,
    //      DWORD                 dwFlagsAndAttributes,
    //      HANDLE                hTemplateFile
    //    );
    fd_ = CreateFile(lp_port,
                     GENERIC_READ | GENERIC_WRITE, // read access and write access
                     0,//FILE_SHARE_READ,//0,
                     0, // If this parameter is NULL, the handle returned by CreateFile cannot be inherited by any child processes
                     OPEN_EXISTING, // An action to take on a file or device that exists or does not exist.
                     FILE_ATTRIBUTE_NORMAL, // TODO : FILE_FLAG_OVERLAPPED: Asynchronous I/O is needed for this application purpose? ; In situations where an I/O request is expected to take a large amount of time, such as a refresh or backup of a large database or a slow communications link, asynchronous I/O is generally a good way to optimize processing efficiency. However, for relatively fast I/O operations, the overhead of processing kernel I/O requests and kernel signals may make asynchronous I/O less beneficial,
                     0);

    if (INVALID_HANDLE_VALUE == fd_) {
        DWORD create_file_err = GetLastError();
        std::stringstream ss;
        switch (create_file_err) {
        case ERROR_FILE_NOT_FOUND:
            std::cout << "Specified port, " << port_ << ", does not exist." << std::endl;
            //setLastError(ss.str());
            return bRet;
        default:
            std::cout << "Unknown error opening the serial port: " << create_file_err << std::endl;
            //setLastError(ss.str());
            return bRet;
        }
    }

    reconfigurePort();
    this->is_open_ = true;
    bRet = true;
    return bRet;

}


void SerialImpl::reconfigurePort() {

    if (fd_ == INVALID_HANDLE_VALUE) {
        // Can only operate on a valid file descriptor
        std::cout << "Invalid file descriptor, is the serial port open?" << std::endl;
        //THROW (std::exception, "Invalid file descriptor, is the serial port open?");
    }

    // Defines the control setting for a serial communications device.
    DCB dcbSerialParams = {0};
    dcbSerialParams.DCBlength=sizeof(dcbSerialParams);

    // FROM WinASTM : size of broadcast buffers and reception
    int32_t iInQueue = 32000;       // The recommended size of the device's internal input buffer, in bytes.
    int32_t iOutQueue = 32000;      // The recommended size of the device's internal output buffer, in bytes.
    // init struc DCB
    if (!SetupComm(fd_, iInQueue, iOutQueue)) {   // Initializes the communications parameters for a specified communications device.
    }
    if (!GetCommState(fd_, &dcbSerialParams)) {
        //error getting state
        std::cout << "Error getting the serial port state." << std::endl;
        //THROW (std::exception, "Error getting the serial port state.");
    }

    // setup baud rate
    switch (baudrate_) {
#ifdef CBR_0
    case 0: dcbSerialParams.BaudRate = CBR_0; break;
#endif
#ifdef CBR_50
    case 50: dcbSerialParams.BaudRate = CBR_50; break;
#endif
#ifdef CBR_75
    case 75: dcbSerialParams.BaudRate = CBR_75; break;
#endif
#ifdef CBR_110
    case 110: dcbSerialParams.BaudRate = CBR_110; break;
#endif
#ifdef CBR_134
    case 134: dcbSerialParams.BaudRate = CBR_134; break;
#endif
#ifdef CBR_150
    case 150: dcbSerialParams.BaudRate = CBR_150; break;
#endif
#ifdef CBR_200
    case 200: dcbSerialParams.BaudRate = CBR_200; break;
#endif
#ifdef CBR_300
    case 300: dcbSerialParams.BaudRate = CBR_300; break;
#endif
#ifdef CBR_600
    case 600: dcbSerialParams.BaudRate = CBR_600; break;
#endif
#ifdef CBR_1200
    case 1200: dcbSerialParams.BaudRate = CBR_1200; break;
#endif
#ifdef CBR_1800
    case 1800: dcbSerialParams.BaudRate = CBR_1800; break;
#endif
#ifdef CBR_2400
    case 2400: dcbSerialParams.BaudRate = CBR_2400; break;
#endif
#ifdef CBR_4800
    case 4800: dcbSerialParams.BaudRate = CBR_4800; break;
#endif
#ifdef CBR_7200
    case 7200: dcbSerialParams.BaudRate = CBR_7200; break;
#endif
#ifdef CBR_9600
    case 9600: dcbSerialParams.BaudRate = CBR_9600; break;
#endif
#ifdef CBR_14400
    case 14400: dcbSerialParams.BaudRate = CBR_14400; break;
#endif
#ifdef CBR_19200
    case 19200: dcbSerialParams.BaudRate = CBR_19200; break;
#endif
#ifdef CBR_28800
    case 28800: dcbSerialParams.BaudRate = CBR_28800; break;
#endif
#ifdef CBR_57600
    case 57600: dcbSerialParams.BaudRate = CBR_57600; break;
#endif
#ifdef CBR_76800
    case 76800: dcbSerialParams.BaudRate = CBR_76800; break;
#endif
#ifdef CBR_38400
    case 38400: dcbSerialParams.BaudRate = CBR_38400; break;
#endif
#ifdef CBR_115200
    case 115200: dcbSerialParams.BaudRate = CBR_115200; break;
#endif
#ifdef CBR_128000
    case 128000: dcbSerialParams.BaudRate = CBR_128000; break;
#endif
#ifdef CBR_153600
    case 153600: dcbSerialParams.BaudRate = CBR_153600; break;
#endif
#ifdef CBR_230400
    case 230400: dcbSerialParams.BaudRate = CBR_230400; break;
#endif
#ifdef CBR_256000
    case 256000: dcbSerialParams.BaudRate = CBR_256000; break;
#endif
#ifdef CBR_460800
    case 460800: dcbSerialParams.BaudRate = CBR_460800; break;
#endif
#ifdef CBR_921600
    case 921600: dcbSerialParams.BaudRate = CBR_921600; break;
#endif
    default:
        // Try to blindly assign it
        dcbSerialParams.BaudRate = baudrate_;
    }

    // setup char len
    if (m_byteSize == eightbits)
        dcbSerialParams.ByteSize = 8;
    else if (m_byteSize == sevenbits)
        dcbSerialParams.ByteSize = 7;
    else if (m_byteSize == sixbits)
        dcbSerialParams.ByteSize = 6;
    else if (m_byteSize == fivebits)
        dcbSerialParams.ByteSize = 5;
    else
        throw std::invalid_argument ("invalid char len");

    // setup stopbits
    if (m_stopbits == stopbits_one)
        dcbSerialParams.StopBits = ONESTOPBIT;
    else if (m_stopbits == stopbits_one_point_five)
        dcbSerialParams.StopBits = ONE5STOPBITS;
    else if (m_stopbits == stopbits_two)
        dcbSerialParams.StopBits = TWOSTOPBITS;
    else
        throw std::invalid_argument ("invalid stop bit");

    // setup parity
    if (m_parity == parity_none) {
        dcbSerialParams.Parity = NOPARITY;
    } else if (m_parity == parity_even) {
        dcbSerialParams.Parity = EVENPARITY;
    } else if (m_parity == parity_odd) {
        dcbSerialParams.Parity = ODDPARITY;
    } else if (m_parity == parity_mark) {
        dcbSerialParams.Parity = MARKPARITY;
    } else if (m_parity == parity_space) {
        dcbSerialParams.Parity = SPACEPARITY;
    } else {
        throw std::invalid_argument ("invalid parity");
    }

    // setup flowcontrol
    if (m_flowcontrol == flowcontrol_none) {
        dcbSerialParams.fOutxCtsFlow = false;   // If this member is TRUE, the CTS (clear-to-send) signal is monitored for output flow control. If this member is TRUE and CTS is turned off, output is suspended until CTS is sent again.
        dcbSerialParams.fRtsControl = RTS_CONTROL_ENABLE;// orginal value: RTS_CONTROL_DISABLE; changed as per WinAstm behaviour
        dcbSerialParams.fOutX = false;  // Indicates whether XON/XOFF flow control is used during transmission. If this member is TRUE, transmission stops when the XoffChar character is received and starts again when the XonChar character is received.
        dcbSerialParams.fInX = false;   // Indicates whether XON/XOFF flow control is used during reception. If this member is TRUE, the XoffChar character is sent when the input buffer comes within XoffLim bytes of being full, and the XonChar character is sent when the input buffer comes within XonLim bytes of being empty.
    }
    if (m_flowcontrol == flowcontrol_software) {
        dcbSerialParams.fOutxCtsFlow = false;
        dcbSerialParams.fRtsControl = RTS_CONTROL_DISABLE;  // Disables the RTS line when the device is opened and leaves it disabled.
        dcbSerialParams.fOutX = true;
        dcbSerialParams.fInX = true;
    }
    if (m_flowcontrol == flowcontrol_hardware) {
        // CTS handshaking: ON
        dcbSerialParams.fOutxCtsFlow = true;    // fOutxCtsFlow: If this member is TRUE, the CTS (clear-to-send) signal is monitored for output flow control. If this member is TRUE and CTS is turned off, output is suspended until CTS is sent again.
        // DSR handshaking: ON
        dcbSerialParams.fOutxDsrFlow = true;    // WIP
        // DTR circuit: HANDSHAKE
        dcbSerialParams.fDtrControl = DTR_CONTROL_HANDSHAKE; // WIP
        // DSR sensitivity: ON
        dcbSerialParams.fDsrSensitivity = true;
        dcbSerialParams.fRtsControl = RTS_CONTROL_HANDSHAKE;    // Enables RTS handshaking. The driver raises the RTS line when the "type-ahead" (input) buffer is less than one-half full and lowers the RTS line when the buffer is more than three-quarters full. If handshaking is enabled, it is an error for the application to adjust the line by using the EscapeCommFunction function.
        dcbSerialParams.fOutX = false;
        dcbSerialParams.fInX = false;
    }
    // FROM WinASTM :
    dcbSerialParams.XonChar = 0x11;   // ASCII_XON ;    // The value of the XON character for both transmission and reception.
    dcbSerialParams.XoffChar = 0x13;  // ASCII_XOFF ;   // The value of the XOFF character for both transmission and reception.
    // XonLim: The minimum number of bytes in use allowed in the input buffer before flow control is activated to allow transmission by the sender. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in the fInX, fRtsControl, or fDtrControl members.
    dcbSerialParams.XonLim = (WORD)((iInQueue * 3 )/10) ;      // 30 % d'occupation, 30% occupancy
    // XoffLim: The minimum number of free bytes allowed in the input buffer before flow control is activated to inhibit the sender. Note that the sender may transmit characters after the flow control signal has been activated, so this value should never be zero. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in the fInX, fRtsControl, or fDtrControl members. The maximum number of bytes in use allowed is calculated by subtracting this value from the size, in bytes, of the input buffer.
    dcbSerialParams.XoffLim = (WORD)((iOutQueue * 7 )/10) ;    // 70 % d'occupation

    dcbSerialParams.fBinary = true;
    dcbSerialParams.fParity = true ;
    // The value of the character used to signal an event.
    dcbSerialParams.EvtChar = (BYTE)0;    // we use the serial port in RX_CHAR only


    // activate settings
    if (!SetCommState(fd_, &dcbSerialParams)){
        CloseHandle(fd_);
        std::cout << "Error setting serial port settings." << std::endl;
        //THROW (std::exception, "Error setting serial port settings.");
    }
    // Discards all characters from the output or input buffer of a specified communications resource. It can also terminate pending read or write operations on the resource.
    // PURGE_RXABORT: Terminates all outstanding overlapped read operations and returns immediately, even if the read operations have not been completed.
    // PURGE_TXABORT: Terminates all outstanding overlapped write operations and returns immediately, even if the write operations have not been completed.
    PurgeComm( fd_, PURGE_RXABORT | PURGE_TXABORT );
    // PURGE_RXCLEAR: Clears the input buffer (if the device driver has one).
    // PURGE_TXCLEAR: Clears the output buffer (if the device driver has one).
    PurgeComm( fd_, PURGE_RXCLEAR | PURGE_TXCLEAR );

    // Setup timeouts
    COMMTIMEOUTS timeouts = {0};
    // Timeout: ON, when (m_readTotalTimeoutConstant * m_readTotalTimeoutMultiplier) > 0 || (m_writeTotalTimeoutConstant * m_writeTotalTimeoutMultiplier) > 0
    // Default Timeout: OFF, WinASTM doesn't effect Timeout value,
    timeouts.ReadIntervalTimeout = m_readIntervalTimeout;   // A value of zero indicates that interval time-outs are not used.
    timeouts.ReadTotalTimeoutConstant = m_readTotalTimeoutConstant; // A value of zero for both the ReadTotalTimeoutMultiplier and ReadTotalTimeoutConstant members indicates that total time-outs are not used for read operations.
    timeouts.ReadTotalTimeoutMultiplier = m_readTotalTimeoutMultiplier;
    timeouts.WriteTotalTimeoutConstant = m_writeTotalTimeoutConstant;     // A value of zero for both the WriteTotalTimeoutMultiplier and WriteTotalTimeoutConstant members indicates that total time-outs are not used for write operations.
    timeouts.WriteTotalTimeoutMultiplier = m_writeTotalTimeoutMultiplier;
    if (!SetCommTimeouts(fd_, &timeouts)) {
        std::cout << "Error setting timeouts." << std::endl;
        //THROW (std::exception, "Error setting timeouts.");
    }

}


size_t SerialImpl::write (const uint8_t *data, size_t length) {
    if (is_open_ == false) {
        std::cout << "PortNotOpenedException Serial::write" << std::endl;
        //throw PortNotOpenedException ("Serial::write");
    }
    DWORD bytes_written;
    if (!::WriteFile(fd_, data, static_cast<DWORD>(length), &bytes_written, NULL)) {
        //std::stringstream ss;
        std::cout << "Error while writing to the serial port: " << GetLastError() << std::endl;
        //THROW (IOException, ss.str().c_str());
    }
    return (size_t) (bytes_written);
}


size_t SerialImpl::read (uint8_t *buf, size_t size) {
    std::cout << __func__ << __LINE__ << std::endl;
    if (!is_open_) {
        std::cout << "PortNotOpenedException Serial::read" << std::endl;
        //throw PortNotOpenedException ("Serial::read");
    }
    DWORD bytes_read;
    std::cout << __func__ << __LINE__ << std::endl;
    if (!ReadFile(fd_, buf, static_cast<DWORD>(size), &bytes_read, NULL)) {
        std::cout << __func__ << __LINE__ << std::endl;
        //stringstream ss;
        std::cout << "Error while reading from the serial port: " << GetLastError();
        //THROW (IOException, ss.str().c_str());
    }
    std::cout << __func__ << __LINE__ << std::endl;
    return (size_t) (bytes_read);
}


void SerialImpl::close () {
    if (is_open_ == true) {
        if (fd_ != INVALID_HANDLE_VALUE) {
            int ret;
            ret = CloseHandle(fd_);
            if (ret == 0) {
                //        stringstream ss;
                std::cout << "Error while closing serial port: " << GetLastError() << std::endl;
                //THROW (IOException, ss.str().c_str());
            } else {
                fd_ = INVALID_HANDLE_VALUE;
            }
        }
        is_open_ = false;
    }
}


int main(int argc, char *argv[])
{
    std::cout << "Hello World!" << std::endl;
    std::cout << "!!!Hello World from RS232!!!" << std::endl; // prints !!!Hello World!!!
    std::cout << "exe portname baudrate writedata byteSize stopbit parity flowcontrol ReadIntervalTimeout ReadTotalTimeoutConstant ReadTotalTimeoutMultiplier WriteTotalTimeoutConstant WriteTotalTimeoutMultiplier" << std::endl;

    SerialImpl *pimpl_;
    unsigned long baud = 0;
    sscanf(argv[2], "%lu", &baud);
    unsigned long charLength = 0;
    sscanf(argv[4], "%lu", &charLength);
    unsigned long stopbit = 0;
    sscanf(argv[5], "%lu", &stopbit);
    unsigned long parity = 0;
    sscanf(argv[6], "%lu", &parity);
    unsigned long flowcontrol = 0;
    sscanf(argv[7], "%lu", &flowcontrol);

    unsigned long readIntervalTimeout = 0;
    sscanf(argv[8], "%lu", &readIntervalTimeout);
    unsigned long readTotalTimeoutConstant = 0;
    sscanf(argv[9], "%lu", &readTotalTimeoutConstant);
    unsigned long readTotalTimeoutMultiplier = 0;
    sscanf(argv[10], "%lu", &readTotalTimeoutMultiplier);
    unsigned long writeTotalTimeoutConstant = 0;
    sscanf(argv[11], "%lu", &writeTotalTimeoutConstant);
    unsigned long writeTotalTimeoutMultiplier = 0;
    sscanf(argv[12], "%lu", &writeTotalTimeoutMultiplier);

    pimpl_ = new SerialImpl(argv[1], baud, static_cast<SerialImpl::bytesize_t>(charLength),
            static_cast<SerialImpl::stopbits_t>(stopbit), static_cast<SerialImpl::parity_t>(parity),
            static_cast<SerialImpl::flowcontrol_t>(flowcontrol), readIntervalTimeout, readTotalTimeoutConstant, readTotalTimeoutMultiplier, writeTotalTimeoutConstant, writeTotalTimeoutMultiplier);
    pimpl_->open();

    for (int i =0; i < 20; i++) {
        std::cout << __func__ << "Iteration: " << i << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(3));
        pimpl_->write(reinterpret_cast<uint8_t*>(argv[3]), strlen(argv[3]));
        std::this_thread::sleep_for(std::chrono::seconds(3));
        uint8_t ibuffer[255];
        size_t bytes_read = 0;
        std::cout << __func__ << __LINE__ << std::endl;
        bytes_read = pimpl_->read(ibuffer, 1);
        std::cout << __func__ << __LINE__ << std::endl;
        std::cout << "cbuffer:" << std::endl;
        for (int i = 0; i < bytes_read; ++i )
        {
            printf(": %02x :", ibuffer[i]);
        }
        std::cout << "bytes read: " << bytes_read << ", data read: " << ibuffer << std::endl;

    }
    pimpl_->close();
    return 0;
}
