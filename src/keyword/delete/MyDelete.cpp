#include "MyDelete.h"
#include "MyClass2.h"

#include <iostream>

MyDelete::MyDelete() {
}

MyDelete::~MyDelete() {
}

void MyDelete::run() {
// check delete expression
        int* iptr = nullptr;
        delete iptr;  
        


        int* iptr2 = new int;
        int* iptr3 = new int;
        delete iptr2;  
        delete iptr;
        delete iptr3;    
        delete iptr2;                   // NO runtime-error 
        delete iptr3;  
        //delete iptr3;                 // runtime-error: double free or corruption (fasttop)
  
        //delete nullptr;               // error: expected pointer

// delete [] expression	
        //int** iptr4 = new int[3][1];

// will delete call destructor? answer is yes
        MyClass2* cptr = new MyClass2();
        delete cptr;                            // destructor called 
        std::cout << __func__ << '\n';
}              

