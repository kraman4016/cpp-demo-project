#ifndef MYDELETE_H
#define MYDELETE_H


namespace Demo {
class MyDelete {
	public:
                MyDelete();
		virtual ~MyDelete();

                void run();

};
}
using Demo::MyDelete;
#endif /* MYDELETE_H */


