#ifndef MYCLASS_H
#define MYCLASS_H

#include <string>

namespace Demo {
class MyClass {
	public:
                MyClass();
		MyClass(const std::string& arg);
		virtual ~MyClass();

		void run();
	private:
        std::string m_str;
};
}
using Demo::MyClass;
#endif /* MYCLASS_H */


