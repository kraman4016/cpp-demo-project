#ifndef MYCLASS2_H
#define MYCLASS2_H
namespace Demo {
class MyClass2 {
	public:
		//constexpr MyClass2() {run();}; //error: constexpr constructor does not have empty body
		constexpr MyClass2() {}; 
		virtual ~MyClass2();
		void run();
	private:
};
}
using Demo::MyClass2;
#endif /* MYCLASS2_H */


