#include "MyClass.h"

#include <iostream>

MyClass2 MyClass::mc2 = MyClass2();
const MyClass2 MyClass::mc3;

MyClass::MyClass() {
	run();
}

MyClass::~MyClass() {
}

void MyClass::run() {
    std::cout << INT_MYCLASS << '\n';
    std::cout << INT2_MYCLASS << '\n';
    std::cout << CHAR_MYCLASS << '\n';
    std::cout << CHAR2_MYCLASS << '\n';
    std::cout << CHARPTR_MYCLASS << '\n';
    std::cout << *CHARPTR_MYCLASS << '\n';
    std::cout << STR_MYCLASS << '\n';
   // std::cout << STR2_MYCLASS << '\n';
    static_assert(true, "true");
    //error: static assertion failed: false
    //static_assert(false, "false");

}

