#ifndef MYCLASS_H
#define MYCLASS_H

#include <iostream>
#include "MyClass2.h"

namespace Demo {
class MyClass {
        const int INT_MYCLASS = 9;   
        static const int INT2_MYCLASS = 10;      
        const char CHAR_MYCLASS = 'A';
        static const char CHAR2_MYCLASS = 'a';
        const char *CHARPTR_MYCLASS = "B";
        const std::string STR_MYCLASS = std::string("C");
        //error: in-class initialization of static data member ‘const string Demo::MyClass::STR2_MYCLASS’ of non-literal type
        //static const std::string STR2_MYCLASS = "c";    
        //error: the type ‘const string {aka const std::__cxx11::basic_string<char>}’ of constexpr variable ‘Demo::MyClass::STR3_MYCLASS’ is not literal
        //constexpr static const std::string STR3_MYCLASS = "c";    

        const int c[2] = { 1, 2 };
        //error: ISO C++ forbids in-class initialization of non-const static member ‘Demo::MyClass::d’
        //static int d = 11;        
        // error: ‘constexpr’ needed for in-class initialization of static data member ‘const int Demo::MyClass::c2 [2]’ of non-integral type [-fpermissive]
        //static const int c2[2] = { 1, 2 }; 
        static constexpr const int c3[2] = { 1, 2 }; 
        
        static MyClass2 mc2;
        static const MyClass2 mc3;
        //static const MyClass2 mc4 = MyClass2();
    
	public:
		MyClass();
		virtual ~MyClass();

		void run();
	private:
};
}
using Demo::MyClass;
#endif /* MYCLASS_H */


