#include "MyClass.h"

#include <iostream>
#include <string>

MyClass::MyClass() {
}

MyClass::~MyClass() {
}


template<class F, class A>
void Fwd(F f, A a)
{
    f(a);
}
 
void g(int* i)
{
    std::cout << "Function g called\n";
}

void MyClass::run() {
        fun(123);
        fun("abc");     //warning: ISO C++ forbids converting a string constant to ‘char*’ [-Wwrite-strings]
        fun(nullptr);         //run-time: execution stopped if try to print on console
        //std::cout << &nullptr;
        //fun(&nullptr);  //error: lvalue required as unary ‘&’ operand

        //fun(NULL);      //error: call of overloaded ‘fun(NULL)’ is ambiguous
        fun((std::nullptr_t)NULL);
        fun2(NULL);

        g(NULL);           // Fine
        g(0);              // Fine

        Fwd(g, nullptr);   // Fine
        //Fwd(g, NULL);  // ERROR: No function g(int)
        
        std::nullptr_t pn;
        std::nullptr_t pn2;
        fun(pn);
        if (pn2 == nullptr)
                fun(pn);
        //fun((int)pn);   //error -fpermissive
        //fun(reinterpret_cast<int>(pn));        //error: cast from ‘std::nullptr_t’ to ‘int’ loses precision [-fpermissive]
        //fun(reinterpret_cast<int*>(pn));        //error: invalid cast from type ‘std::nullptr_t’ to type ‘int*’
        int *pi;
        //fun((std::nullptr_t)pi);        //error: invalid cast from type ‘int*’ to type ‘std::nullptr_t’
        //fun(reinterpret_cast<std::nullptr_t>(pi));      //error: invalid cast from type ‘int*’ to type ‘std::nullptr_t’

        std::cout << "nullptr_t size: " << sizeof(std::nullptr_t) << sizeof(void*) << '\n';
        fun3("");
        char arr[] = "";
        fun3(arr);
        std::string str("");
        fun3(str);        
      
        /*
        terminate called after throwing an instance of 'std::logic_error'
          what():  basic_string::_M_construct null not valid
        ./shell-build-keyword-nullptr.sh: line 10:  4532 Aborted                 (core dumped) ./run
        */  
        //std::string str2(nullptr);
        //fun3(str2);         
        //fun3(nullptr);        
}

int MyClass::fun(int nb) {
        std::cout << __func__ << "(int)" << '\n';
}

int MyClass::fun2(int nb) {
        std::cout << __func__ << "(int)" << '\n';
}

int MyClass::fun(char* pc) {
        std::cout << __func__ << "(char*)" << '\n';
}

int MyClass::fun3(std::string str) {
        std::cout << __func__ << "(string)" << '\n';
}
