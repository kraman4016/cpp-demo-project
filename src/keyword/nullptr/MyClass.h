#ifndef MYCLASS_H
#define MYCLASS_H

#include <string>

namespace Demo {
class MyClass {
	public:
                MyClass();
		virtual ~MyClass();

		void run();
                int fun(int nb);
                int fun(char* pc);
                int fun2(int nb);
                int fun3(std::string str);
	private:
};
}
using Demo::MyClass;
#endif /* MYCLASS_H */


